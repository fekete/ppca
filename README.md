# PPCA

Progressive Principal Component Analysis

## Installation

* download and install miniconda (see https://docs.conda.io/en/latest/miniconda.html)
* do what is needed to include the conda executable in your **PATH** environment variable. Logout/login should work. Usually, the default path to conda executable is $HOME/miniconda3/bin/conda.
* Change directory to ppca
* Decide where you will compile the code. It is usually in the directory "build"  ``<build-dir>>`` will be "build" below.
* Execute ``./install_ppca.sh <build-dir>`` to install all the required anaconda packages.
* Switch to the newly create ``ppca`` environment by running ``conda activate ppca``
* Execute ``./build_ppca.sh <build-dir>`` to compile the benchmark program
* Executables are generate in <build-dir> their names are : ppca_benchmark and ppca_momentum

NB: You dont have to create ``<build-dir>``! Indeed, build_ppca.sh will create it foy you as a subdirectory of the ppca dir.

For some reason, if that fails, try a second time. Anaconda has some issues with updating paths.

## Running the Benchmark

After the two programs ppca_benchmark and ppca_momentum are built, you can run the whole benchmark reported in our article by running:

``./run_benchmark.sh <build-dir>``

It will take time, typically 20-60 minutes depending on your setup.
The results are in ``<build-dir>/ppca``

## Installing the GUI Demo

TODO: installing opengl libraries is not straightforward with Anaconda...

### Keyboard interactions
- Z: Zoom out
- X: Zoom in
- P: More points
- O: Less points
- R: Reset all parameters
- T: Test for certain combinations, takes a long time
- Number 1,2,3,4

### Generating Sinusoidal Data

For the equation sin(t) =  sin(2 * PI * (10t / freq)  + off * (2 * PI / maxOffset) )
1. Perturb the frequency, the range of perturb becomes larger.
2. Perturb the frequency, the range of perturb becomes smaller.
3. Perturb the offset, the range of perturb becomes larger.
4. Perturb the offset, the range of perturb becomes smaller.
