# DEMO:
#https://pythonprogramming.net/3d-graphing-pandas-matplotlib/

# Pandas.dataframe DOC
#https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html
import pandas as pd
from pandas import DataFrame
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


#df = pd.read_csv('./data/20190417_155344_Data.csv', sep = ';')
df = pd.read_csv('./data/purecv_float.csv', sep = ';')
df_d = pd.read_csv('./data/purecv_double.csv', sep = ';')
#df2 = pd.read_csv('./data/20190417_155454_Data.csv', sep = ';')
df2 = pd.read_csv('./data/cveigen_float.csv', sep = ';')
#df3 = pd.read_csv('./data/20190417_155917_Data.csv', sep = ';')
df_power = pd.read_csv('./data/power_10iter_float.csv', sep = ';')
#df_power5 = pd.read_csv('./data/20190419_134447_Data.csv', sep = ';')
df_power5 = pd.read_csv('./data/power_epsilon_double.csv', sep = ';')
df_power5s = pd.read_csv('./data/power_epsilon_sin_double.csv', sep = ';')

df_power10 = pd.read_csv('./data/power_100iter_float.csv', sep = ';')
df_power10_d = pd.read_csv('./data/power_100iter_double.csv', sep = ';')

df_spectra = pd.read_csv('./data/spectra_ncv6.csv', sep = ';')
df_spectra4 = pd.read_csv('./data/spectra_ncv4.csv', sep = ';')


t = df['Load Time(sec)']
m = df['Nb points(cluster*perturb time)']
n = df['Vector Length(n)']

t_d = df_d['Load Time(sec)']
m_d = df_d['Nb points(cluster*perturb time)']
n_d = df_d['Vector Length(n)']

t2 = df2['Load Time(sec)']
m2 = df2['Nb points(cluster*perturb time)']
n2 = df2['Vector Length(n)']

t3 = df_power['Load Time(sec)']
m3 = df_power['Nb points(cluster*perturb time)']
n3 = df_power['Vector Length(n)']

t4 = df_power10['Load Time(sec)']
m4 = df_power10['Nb points(cluster*perturb time)']
n4 = df_power10['Vector Length(n)']

t4_d = df_power10_d['Load Time(sec)']
m4_d = df_power10_d['Nb points(cluster*perturb time)']
n4_d = df_power10_d['Vector Length(n)']

t5 = df_power5['Load Time(sec)']
m5 = df_power5['Nb points(cluster*perturb time)']
n5 = df_power5['Vector Length(n)']

t5s = df_power5s['Load Time(sec)']
m5s = df_power5s['Nb points(cluster*perturb time)']
n5s = df_power5s['Vector Length(n)']

t_spectra = df_spectra['Load Time(sec)']
m_spectra = df_spectra['Nb points(cluster*perturb time)']
n_spectra = df_spectra['Vector Length(n)']

t_spectra4 = df_spectra4['Load Time(sec)']
m_spectra4 = df_spectra4['Nb points(cluster*perturb time)']
n_spectra4 = df_spectra4['Vector Length(n)']

'''
t3 = df3['Load Time(sec)']
m3 = df3['Nb points(cluster*perturb time)']
n3 = df3['Vector Length(n)']
'''

fig = plt.figure(figsize=plt.figaspect(0.5))
#plt.subplot(211)
#threedee = plt.figure().gca(projection='3d')
#threedee.plot_surface(m,n,t)

threedee = fig.add_subplot(1, 2, 1, projection='3d')

#threedee = plt.figure().gca()
#threedee.hold(True)


threedee.scatter(m,n,t,s=10,c="blue",label='Pure CV')
threedee.scatter(m2,n2,t2,s=10,c="red",label='CV Eigen')
threedee.scatter(m3,n3,t3,s=10,c="orange",label='Power with 10 iter')
#threedee.scatter(m5,n5,t5,s=10,c="black",label='Power with epsilon')
threedee.scatter(m4,n4,t4,s=10,c="green",label='Power with 100 iter')
#threedee.scatter(m3,n3,t3,s=20,c="blue",label='Eigensolver')


'''
threedee.plot_trisurf(m,n,t)
threedee.plot_trisurf(m2,n2,t2)
threedee.plot_trisurf(m3,n3,t3)
threedee.plot_trisurf(m4,n4,t4)
'''

threedee.set_xlabel('Nb points')
threedee.set_ylabel('Vector Length')
threedee.set_zlabel('Load Time (s)')

#threedee.legend((p1._facecolors3d, p2._facecolors3d, p3._facecolors3d), ('label1', 'label2', 'label3'))
threedee.legend()

###############################
#plt.subplot(212)
threedee2 = fig.add_subplot(1, 2, 2, projection='3d')
#threedee2 = plt.figure().gca(projection='3d')

###threedee2.scatter(m_d,n_d,t_d,s=10,c="orange",label='Pure CV')
#threedee2.scatter(m5,n5,t5,s=10,c="red",label='Power with epsilon')
threedee2.scatter(m5s,n5s,t5s,s=10,c="red",label='Power with epsilon')
###threedee2.scatter(m4_d,n4_d,t4_d,s=10,c="green",label='Power with 100 iters')
threedee2.scatter(m_spectra,n_spectra,t_spectra,s=10,c="blue",label='Spectra-ncv=6')
########threedee2.scatter(m_spectra4,n_spectra4,t_spectra4,s=10,c="red",label='Spectra-ncv=4')

threedee2.set_xlabel('Nb points')
threedee2.set_ylabel('Vector Length')
threedee2.set_zlabel('Load Time (s)')

#threedee.legend((p1._facecolors3d, p2._facecolors3d, p3._facecolors3d), ('label1', 'label2', 'label3'))
threedee2.legend()

plt.show()

'''
    print(df.values[0])
    print(df.at[1,"Platform"])
    print(df.columns)
    print(df.shape)
    #df = df.append(df2)
    '''
