import sys
import os
import random
import time

# seed random
# f min max
if(len(sys.argv) != 3 and len(sys.argv) != 5):
    print("Usage: generate_data.py [length] [dimension] \n\t\t#Generate random data between -1 and 1")
    print("Usage: generate_data.py [length] [dimension] [min] [max] \n\t\t#Generate random data between min and max")
    exit(0)

n = int(sys.argv[1])
m = int(sys.argv[2])

if(len(sys.argv) == 3):
    min = -1
    max = 1
else:
    min = int(sys.argv[3])
    max = int(sys.argv[4])
    if min > max:
        min,max = max,min

if(n < 2):
    print("Vector Length should be at least 2.")
    exit(0)

print("Generate a "+ str(n) +"*" + str(m) + " data file")
file = open("data_rand_"+str(int(time.time()))+".txt","w")

file.write( str(n) + " " + str(m) + "\n")
for nn in range(n):
    for mm in range(m):
        file.write( str(random.random() * (max-min) + min ) + "\t")
    file.write("\n")

file.close()
