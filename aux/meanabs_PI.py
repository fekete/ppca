import sys
import os
import random
import time
import math

def extractNumber(str):
    newstr = ''.join((ch if ch in '0123456789.-' else ' ') for ch in str)
    listOfNumbers = [float(i) for i in newstr.split()]
    return listOfNumbers


algo = []
algo1=[]
algo2=[]
algo3=[]
algo4=[]

prec1 = []
prec2 = []
prec3 = []
prec4=[]

init1 = 0
init2 = 0
speed1 = 0
speed1_2 = 0
speed2 = 0

count = 0
batch = 0

with open(sys.argv[1],'r') as f:
    for line in f:
        line = line.strip('\n')
        if("*" in line): count+=1
        elif(count == 1): algo1.append(line)
        elif(count == 2): algo2.append(line)
        elif(count == 3): algo3.append(line)
        else: algo4.append(line)


for line in algo1:
    prec1.append(float(line))
for line in algo2:
    prec2.append(float(line))
for line in algo3:
    prec3.append(float(line))
for line in algo4:
    prec4.append(float(line))

from matplotlib import pyplot as plt
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(1, 1, 1)

ax.plot(range(len(prec1)),prec1,'-r.',label = "PI")
ax.plot(range(len(prec2)),prec2,'-b.',label = "Optimal")
ax.plot(range(len(prec3)),prec3,'-y.',label = "Best Heavy Ball")
ax.plot(range(len(prec4)),prec4,'-g.',label = "Our")


ax.set_yscale('log')
ax.set_xlim(0)


ax.legend(loc="best",framealpha=0.7)
ax.set_xlabel('Iterations')
ax.set_ylabel('Mean Absolute Unsigned Error')

plt.show()

