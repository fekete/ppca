import sys
import os
import random
import time
import math

def extractNumber(str):
    newstr = ''.join((ch if ch in '0123456789.-' else ' ') for ch in str)
    listOfNumbers = [float(i) for i in newstr.split()]
    return listOfNumbers


baseline = []
prec0 = []
prec0_2 = []

algo1=[]
prec1 = []
prec1_2 = []

algo2=[]
prec2 = []
prec2_2 = []

algo3=[]
prec3 = []
prec3_2 = []

algo4=[]
prec4 = []
prec4_2 = []

init1 = 0
init2 = 0
speed1 = 0
speed2 = 0

count = 0
batch = 0

with open(sys.argv[1],'r') as f:
    for line in f:
        line = line.strip('\n')
        if("*" in line): count+=1
        elif("==" in line and count == 0): baseline.append(line)
        elif(count == 1): algo1.append(line)
        elif(count == 2): algo2.append(line)
        elif(count == 3): algo3.append(line)
        else: algo4.append(line)


for line in baseline:
    ppp = line.split(" == ")
    prec0.append(float(ppp[0]))
    prec0_2.append(float(ppp[1]))

for line in algo1:
    ppp = line.split(" == ")
    prec1.append(float(ppp[0]))
    prec1_2.append(float(ppp[1]))
    
for line in algo2:
    ppp = line.split(" == ")
    prec2.append(float(ppp[0]))
    prec2_2.append(float(ppp[1]))
    
for line in algo3:
    ppp = line.split(" == ")
    prec3.append(float(ppp[0]))
    prec3_2.append(float(ppp[1]))

for line in algo4:
    ppp = line.split(" == ")
    prec4.append(float(ppp[0]))
    prec4_2.append(float(ppp[1]))


from matplotlib import pyplot as plt
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(1, 1, 1)

ax.plot(range(len(prec1)),prec1,'-r.',label = "PI")
ax.plot(range(len(prec1)),prec1_2,'-b.')

ax.plot(range(len(prec2)),prec2,'--r.',label = "Optimal")
ax.plot(range(len(prec2)),prec2_2,'--b.')

ax.plot(range(len(prec3)),prec3,'-xr',label = "Best Heavy Ball")
ax.plot(range(len(prec3)),prec3_2,'-xb')

ax.plot(range(len(prec4)),prec4,'--rx',label = "Our")
ax.plot(range(len(prec4)),prec4_2,'--bx')

ax.axhline(y=prec0, color='grey', linestyle=':')
ax.axhline(y=prec0_2, color='grey', linestyle=':')

             
ax.set_yscale('log')
ax.set_xlim(0)

ax.legend(loc="best",framealpha=0.7)
ax.set_xlabel('Iterations')
ax.set_ylabel('Two Largest Eigenvalues')

plt.show()

