import sys
import os
import random
import time
import math

file_text_PP=[]
first_PP = []
second_PP = []

file_text_P=[]
first_P = []
second_P = []

file_text_PPM=[]
first_PPM = []
second_PPM = []

with open('data_PP.txt','r') as f:
    for line in f:
        file_text_PP.append(list(line.strip('\n').split(',')))
#print(file_text)

for line in file_text_PP:
    if len(line[0]) > 1:
        if line[0][0] == '1':
            for t in line[0].split(' '):
                if len(t)>0 and t!='1:':
                    first_PP.append(float(t))
        if line[0][0] == '2':
            for t in line[0].split(' '):
                if len(t)>0 and t!='2:':
                    second_PP.append(float(t))

with open('data_P.txt','r') as f:
    for line in f:
        file_text_P.append(list(line.strip('\n').split(',')))
for line in file_text_P:
    if len(line[0]) > 1:
        if line[0][0] == '1':
            for t in line[0].split(' '):
                if len(t)>0 and t!='1:':
                    first_P.append(float(t))
        if line[0][0] == '2':
            for t in line[0].split(' '):
                if len(t)>0 and t!='2:':
                    second_P.append(float(t))

with open('data_PPM.txt','r') as f:
    for line in f:
        file_text_PPM.append(list(line.strip('\n').split(',')))
for line in file_text_PPM:
    if len(line[0]) > 1:
        if line[0][0] == '1':
            for t in line[0].split(' '):
                if len(t)>0 and t!='1:':
                    first_PPM.append(float(t))
        if line[0][0] == '2':
            for t in line[0].split(' '):
                if len(t)>0 and t!='2:':
                    second_PPM.append(float(t))




from matplotlib import pyplot as plt
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)

ax.plot(first_P,label = "P")
ax.plot(first_PP,label = "PP")
ax.plot(first_PPM,label = "PPM")
ax.set_yscale('log')

ax2.plot(second_P,label = "P")
ax2.plot(second_PP,label = "PP")
ax2.plot(second_PPM,label = "PPM")
ax2.set_yscale('log')

ax.legend(loc="best",framealpha=0.7)
ax.set_xlabel('Number of iterations')
ax.set_ylabel('$1 - (w_t^Tu_1)^2$')
ax2.legend(loc="best",framealpha=0.7)
ax2.set_xlabel('Number of iterations')
ax2.set_ylabel('$1 - (w_t^Tu_2)^2$')
plt.show()

