import sys
import os
import random
import time
import math

def extractNumber(str):
    newstr = ''.join((ch if ch in '0123456789.-' else ' ') for ch in str)
    listOfNumbers = [float(i) for i in newstr.split()]
    return listOfNumbers


algo = []
algo1=[]
algo2=[]
algo3=[]

prec1 = []
time1 = []

prec2 = []
time2 = []

prec3 = []
time3 = []

init1 = 0
init2 = 0
speed1 = 0
speed2 = 0

count = 0
batch = 0

with open(sys.argv[1],'r') as f:
    for line in f:
        line = line.strip('\n')
        if("*" in line): count+=1
        elif(count == 1): algo1.append(line)
        elif(count == 2): algo2.append(line)
        elif(count == 3):
            if("==" in line):  algo3.append(line)
            else: batch = int(line)


for line in algo1:
    if(line[0] == 'E'): speed1 = extractNumber(line)[0]
    elif(line[0] == 'I'): init1 = extractNumber(line)[0]
    else:
        ppp = line.split(" == ")
        prec1.append(float(ppp[0]))
        time1.append(float(ppp[1]))
    
for line in algo2:
    if(line[0] == 'E'): speed2 = extractNumber(line)[0]
    elif(line[0] == 'I'): init2 = extractNumber(line)[0]
    else:
        ppp = line.split(" == ")
        prec2.append(float(ppp[0]))
        time2.append(float(ppp[1]))
    
for line in algo3:
    ppp = line.split(" == ")
    prec3.append(float(ppp[0]))
    time3.append(float(ppp[1]))

time1 = map(lambda x: x * speed1 + init1, time1)
time2 = map(lambda x: x * speed2 + init2, time2)

from matplotlib import pyplot as plt
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(1, 1, 1)

ax.plot(time1,prec1,'-r.')
ax.plot(time2,prec2,'-b.')
ax.plot(time3,prec3,'xg')

batch/=2
# zip joins x and y coordinates in pairs
for x,y in zip(time3,prec3):

    label = batch
    batch*=2
    
    ax.annotate(label, # this is the text
                 (x,y), # this is the point to label
                 textcoords="offset points", # how to position the text
                 xytext=(0,10), # distance from text to points (x,y)
                 ha='center') # horizontal alignment can be left, right or center

ax.set_yscale('log')
ax.set_xlim(0)


##ax.legend(loc="best",framealpha=0.7)
ax.set_xlabel('Time (seconds)')
ax.set_ylabel('Mean Absolute Unsigned Error')

plt.show()

