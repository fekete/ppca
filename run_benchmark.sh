#!/usr/bin/env bash
set -eu -o pipefail
if [ $# -eq 0 ] ; then
    echo "Usage: $0 <build-dir>"
    exit 1
fi

CONDA=$(which conda)||true

if [[ -x "$CONDA" ]] ; then
    echo "conda executable found: $CONDA"
else
    echo "conda executable NOT found"
    echo "- download and install miniconda (see https://docs.conda.io/en/latest/miniconda.html)"
    echo "- do what is needed to include the conda executable in your PATH environment variable"
    echo "(the default path to conda executable is $HOME/miniconda3/bin/conda)"
    exit 1
fi
CONDA_ROOT=$(dirname $(dirname $CONDA))

build_dir="$1"
cd "${build_dir}" || exit
python3 ../datasets/download.py
unzip -u ppca_input.zip

# Remove previous benchmarks results
rm -rf ppca

DATASETS=`./ppca_benchmark --datasets`

for dataset in $DATASETS
do
    ./ppca_benchmark $dataset 10000
done

cd ppca
python3 ../../results/generate_plots.py $DATASETS
