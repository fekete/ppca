// simple version of all algorithms

#include "algorithm.h"

Matrix powerMethod(Matrix data, Matrix init, double _epsilon, int& nbIter, int maxIter, const Matrix *eigenVector, const double eigenvalue){
    Matrix x = init / cv::norm(init);
    Matrix xPre;
    double delta;
    double time = 0;
    clock_t tStart1,tStart2;

    for(nbIter = 0;nbIter<maxIter;nbIter++){

        tStart1 = clock();

        xPre = x.clone();

        x = data * x;
        x = x / cv::norm(x);

        delta = 1 - pow(xPre.dot(x),2) / pow(x.dot(x),2);

        tStart2 = clock();
        time += (double) (tStart2 - tStart1) / CLOCKS_PER_SEC;
        ///Print time
        //cout << time << " ";

        ///Angle eigenvector //
        cout << 1 - pow(eigenVector->t().dot(x),2) / pow(x.dot(x),2) << " ";
        ///Difference eigen value //cout << abs(eigenvalue - rayleigh(data,x)) << " ";

        if(abs(delta) < _epsilon){
            cout << endl;
            break;
        }
    }
    cout << endl;
    return x;
}

Matrix progressivePowerMethod(Matrix data, Matrix init, double _epsilon, int& nbIter, int maxIter,
                                bool& finished, int step, const Matrix *eigenVector, const double eigenvalue, double &time){
    if(finished) return init;
    clock_t tStart1,tStart2;

    Matrix x = init / cv::norm(init);
    Matrix xPre;
    double delta;
    for(int i=0;nbIter<maxIter && i<step ;i++,nbIter++){

        tStart1 = clock();

        xPre = x.clone();

        x = data * x;
        x = x / cv::norm(x);

        delta = 1 - pow(xPre.dot(x),2) / pow(x.dot(x),2);

        tStart2 = clock();

        //cout << delta;
        ///Angle eigenvector //
        cout << 1 - pow(eigenVector->t().dot(x),2) / pow(x.dot(x),2) << " ";
        ///Difference eigen value //cout << abs(eigenvalue - rayleigh(data,x)) << " ";
        time += (double) (tStart2 - tStart1) / CLOCKS_PER_SEC;
        //cout << time << " ";

        if(abs(delta) < _epsilon){
            finished = true;
            cout << endl;
            break;
        }
    }
    cout << endl;
    return x;
}

void progressivePowerMethodMomentum(Matrix data, Matrix& eigVec, Matrix& eigVecPrev, double& eigenVal, double beta,
                                    double _epsilon, int& nbIter, int maxIter, bool& finished, int step,
                                    const Matrix *eigenVector, const double eigenvalue, double &time){
    if(finished) return;
    clock_t tStart1,tStart2;

    Matrix eigVec_tmp;
    double eigVec_norm;
    double delta;
    for(int i=0;nbIter<maxIter && i<step ;i++,nbIter++){

        tStart1 = clock();

        eigVec_tmp = eigVec.clone();
        eigVec = data * eigVec - beta * eigVecPrev;
        eigVec_norm = cv::norm(eigVec);
        eigVec = eigVec / eigVec_norm;
        eigVecPrev = eigVec_tmp / eigVec_norm ;

        delta = 1 - pow(eigVec_tmp.dot(eigVec),2) / pow(eigVec.dot(eigVec),2);

        tStart2 = clock();
        ///Angle eigenvector
        cout << 1 - pow(eigenVector->t().dot(eigVec),2) / pow(eigVec.dot(eigVec),2) << " ";
        ///Difference eigen value //cout << abs(eigenvalue - rayleigh(data,eigVec)) << " ";
        time += (double) (tStart2 - tStart1) / CLOCKS_PER_SEC;
        //cout << time << " ";

        if(abs(delta) < _epsilon){
            finished = true;
            eigenVal =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
            cout << endl;
            break;
        }


    }cout << endl;
    eigenVal =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
    //double delta = 1 - pow(eigVec_tmp.dot(eigVec),2) / pow(eigVec.dot(eigVec),2);
    //finished = abs(delta) < _epsilon;
}

Matrix generateMiniBatch(Matrix data, int _batch_size){
    Matrix miniBatch = Matrix::zeros(data.rows,_batch_size,data.type());
    for(int x=0;x<_batch_size;x++){
        int selected = rand() % data.cols;
        for(int y=0;y<data.rows;y++)
            miniBatch.at<double>(y,x) = data.at<double>(y,selected);
    }
    //miniBatch = meanSubtraction(miniBatch);
    Matrix _data_covar = miniBatch * miniBatch.t() / miniBatch.cols;
    //Matrix _data_covar = miniBatch * miniBatch.t() * 5e-4;

    //printMat(miniBatch); cout << endl;

    return _data_covar;
}

Matrix progressivePowerMethodMiniBatch(Matrix origin_data, Matrix init,double& eigVal, double _epsilon, int& nbIter, int maxIter,
                               bool& finished, int step, const Matrix *eigenVector, int batch_size){
    if(finished) return init;

    Matrix x = init / cv::norm(init);
    Matrix xPre;
    double delta;

    Matrix data = generateMiniBatch(origin_data,batch_size);
    for(int i=0;nbIter<maxIter && i<step ;i++,nbIter++){
        xPre = x.clone();

        x = data * x;
        x = x / cv::norm(x);

        delta = 1 - pow(xPre.dot(x),2) / pow(x.dot(x),2);
        cout << 1 - pow(eigenVector->t().dot(x),2) / pow(x.dot(x),2) << " ";

        if(abs(delta) < _epsilon){
            finished = true;
            eigVal = rayleigh(data, init);
            cout << endl;
            break;
        }
    }
    eigVal = rayleigh(data, init);
    cout << endl;
    return x;
}
