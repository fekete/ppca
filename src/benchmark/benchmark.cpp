#include "benchmark.h"
#include <iomanip>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <sys/stat.h>

#include "../algo/myPCA.h"
#include "../algo/prog_randomizedSVD.h"
#include "../algo/incrementalPCA.h"
#include "../algo/prog_blockPowerMomentum.h"
#include "../algo/prog_blockPower.h"
#include "../data/dataGenerator.h"
#include "../global_val.h"

#include <opencv2/core/eigen.hpp>
#include <Eigen/Eigenvalues> 

static bool verbose = true;

enum MEASURE {
    mean_abs_err = 0,
    eigenvalue = 1,
    eigenvector = 2
};

static const MEASURE measure = mean_abs_err;
static const bool tuneBatchSize = false;
static const bool x_axis_exec_time = false;
static Vector baseline_eigvec1, baseline_eigvec2;
static std::string generator("--help");
static int _rows, _cols;
static bool generate_file_date_time = false;
static int TOO_LARGE = 2000;

/*
  Read with:

  import pandas as pd
  df = pd.read_csv('SINEWAVE_20200407_072159_Data.txt', index_col=False, sep=' ')
 */

//static string csv_sep(", ");
static string csv_sep(" ");

static void
csv_header(ofstream& fout) {
    fout << "Dataset" << csv_sep
         << "Rows" << csv_sep        
         << "Cols" << csv_sep        
         << "Algorithm" << csv_sep
         << "Stage" << csv_sep
         << "BatchSize" << csv_sep
         << "Duration" << csv_sep
         << "eigenvalue1" << csv_sep
         << "eigenvalue2" << csv_sep
         << "error" << endl;
}

static void
csv_write(ofstream& fout,
          const string& algorithm,
          int stage,
          int batch_size,
          double duration,
          double value1 = NAN,
          double value2 = NAN,
          double error = NAN) {
    fout << generator << csv_sep
         << _rows << csv_sep
         << _cols << csv_sep
         << algorithm << csv_sep
         << stage << csv_sep
         << batch_size << csv_sep
         << duration << csv_sep;
    if (! isnan(value1))
        fout << value1;
    fout << csv_sep;
    if (! isnan(value2))
        fout << value2;
    fout << csv_sep;
    if (! isnan(error))
        fout << error;
    fout << endl;
}
static const int INIT = 0;
static const int FINAL = -1;

static Matrix
runBaseline(Matrix _origin_data, ofstream& fout) {
    string algo = "Baseline";
    double exec_time;

    std::cerr << "Computing " << algo << "...";
    auto t3 = std::chrono::high_resolution_clock::now();
    ProgressivePowerMethodBlockMomentum prog_algo(algo, _origin_data, _maxIter);
    auto t4 = std::chrono::high_resolution_clock::now();
    auto total_time = std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()/1e6;
    csv_write(fout, algo, 0, 0, total_time);
    std::cerr << " covar: " << total_time << "s ";

    prog_algo.epsilon(1e-12);

    for(int i=0; i<5000; i++){
        prog_algo.partial_fit(true, &exec_time);
        total_time += exec_time;

        if(prog_algo.is_terminated())
            break;
    }
    Matrix baseline = prog_algo.transform();
    auto eigval1 = prog_algo.eigenvalues()[0];
    auto eigval2 = prog_algo.eigenvalues()[1];
    baseline_eigvec1 = eigval1;
    baseline_eigvec2 = eigval2;
    csv_write(fout, algo, FINAL, 0, total_time, eigval1, eigval2, 0);

    std::cerr << "done: "<< total_time << "s" << std::endl;
    return baseline;
}

static void
runEigen(Matrix _origin_data, ofstream& fout, Matrix baseline) {
    string algo = "Eigen";
  
    if (_origin_data.rows > TOO_LARGE) {
        std::cerr << "Skipping " << algo
                  << " because " << _origin_data.rows << " > " << TOO_LARGE << std::endl;
        return;
    }
    std::cerr << "Computing " << algo << "...";
    Eigen::MatrixXd data;
    cv::cv2eigen(_origin_data.t(), data);
    // Mean centering data.
    auto t1 = std::chrono::high_resolution_clock::now();
    Eigen::MatrixXd centered = data.rowwise() - data.colwise().mean();

    // Compute the covariance matrix.
    Eigen::MatrixXd cov = centered.adjoint() * centered;
    cov = cov / (data.rows() - 1);

    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(cov);
    auto t2 = std::chrono::high_resolution_clock::now();
    auto total_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()/1e6;
    csv_write(fout, algo, 0, 0, total_time);
    std::cerr << " covar: " << total_time << "s ";
    
   
    Eigen::VectorXd eigenvalues = eig.eigenvalues();
    // std::cerr << "Eigenvalues: " << eigenvalues << std::endl;
    double eigval1 = eigenvalues[3];
    double eigval2 = eigenvalues[2];
    auto t3 = std::chrono::high_resolution_clock::now();
    total_time += std::chrono::duration_cast<std::chrono::microseconds>(t3-t2).count()/1e6;
    Eigen::MatrixXd evecs = eig.eigenvectors();
    Eigen::MatrixXd pcaTransform = evecs.rightCols(2);

    // Map the dataset in the new two dimensional space.
    Eigen::MatrixXd transformed = data * pcaTransform;
    Matrix result;
    eigen2cv(transformed, result);
    double error = mean_absolute_unsigned_error(baseline, result.t());
  
    csv_write(fout, algo, FINAL, 0, total_time, eigval1, eigval2, error);
    std::cerr << "done: "<< total_time << "s" << std::endl;
}

static void
runOpenCVAll(Matrix _origin_data, ofstream& fout, Matrix baseline) {
    string algo = "OpenCVAll";
  
    if (_origin_data.rows > TOO_LARGE) {
        std::cerr << "Skipping " << algo
                  << " because " << _origin_data.rows << " > " << TOO_LARGE << std::endl;
        return;
    }
    std::cerr << "Computing " << algo << "...";
    auto t1 = std::chrono::high_resolution_clock::now();
    cv::PCA pca_analysis(_origin_data, Matrix(), cv::PCA::DATA_AS_COL);
    auto t2 = std::chrono::high_resolution_clock::now();
    auto total_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()/1e6;
    csv_write(fout, algo, 0, 0, total_time);
    std::cerr << " covar: " << total_time << "s ";
  
    double eigval1 = pca_analysis.eigenvalues.at<double>(0);
    double eigval2 = pca_analysis.eigenvalues.at<double>(1);
    auto t3 = std::chrono::high_resolution_clock::now();
    total_time += std::chrono::duration_cast<std::chrono::microseconds>(t3-t2).count()/1e6;
    Matrix result = pca_analysis.project(_origin_data);
    double error = 0; //mean_absolute_unsigned_error(baseline, result);
  
    csv_write(fout, algo, FINAL, 0, total_time, eigval1, eigval2, error);
    std::cerr << "done: "<< total_time << "s" << std::endl;
}

static void
runOpenCV2(Matrix _origin_data, ofstream& fout, Matrix baseline) {
    string algo = "OpenCV2";
  
    if (_origin_data.rows > TOO_LARGE) {
        std::cerr << "Skipping " << algo
                  << " because " << _origin_data.rows << " > " << TOO_LARGE << std::endl;
        return;
    }
    std::cerr << "Computing " << algo << "...";
    auto t1 = std::chrono::high_resolution_clock::now();
    cv::PCA pca_analysis(_origin_data, Matrix(), cv::PCA::DATA_AS_COL, 2);
    auto t2 = std::chrono::high_resolution_clock::now();
    auto total_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()/1e6;
    csv_write(fout, algo, 0, 0, total_time);
    std::cerr << " covar: " << total_time << "s ";
  
    double eigval1 = pca_analysis.eigenvalues.at<double>(0);
    double eigval2 = pca_analysis.eigenvalues.at<double>(1);
    auto t3 = std::chrono::high_resolution_clock::now();
    total_time += std::chrono::duration_cast<std::chrono::microseconds>(t3-t2).count()/1e6;
    Matrix result = pca_analysis.project(_origin_data);
    // std::cerr << "Projection results " << result.size << std::endl;
    double error = mean_absolute_unsigned_error(baseline, result);
  
    csv_write(fout, algo, FINAL, 0, total_time, eigval1, eigval2, error);
    std::cerr << "done: "<< total_time << "s" << std::endl;
}


static void
runBlockPowerMethodWithMomentum(Matrix _origin_data, ofstream& fout, Matrix baseline) {
    string algo = "ProgressiveBlockPowerMethodWithMomentum";
    double exec_time;

    std::cerr << "Computing " << algo << "...";
    //bool update_Momentum = true;
    auto t3 = std::chrono::high_resolution_clock::now();
    ProgressivePowerMethodBlockMomentum prog_algo(algo, _origin_data, _maxIter);
    auto t4 = std::chrono::high_resolution_clock::now();
    auto total_time = std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()/1e6;
    csv_write(fout, algo, INIT, 0, total_time);

    if(!tuneBatchSize) prog_algo.set_step(1);
    prog_algo.epsilon(1e-8);

    for(int i=0; i<5000; i++){
        prog_algo.partial_fit(tuneBatchSize, &exec_time);
        total_time += exec_time;

        auto eigval1 = prog_algo.eigenvalues()[0];
        auto eigval2 = prog_algo.eigenvalues()[1];
        Matrix result = prog_algo.transform();
        double error = mean_absolute_unsigned_error(baseline, result);
        csv_write(fout, algo, i+1, 0, exec_time, eigval1, eigval2, error);

        if (prog_algo.is_terminated())
            break;
    }
    std::cerr << "done: " << total_time << "s" << std::endl;
}

static void
runRandomized(Matrix _origin_data, ofstream& fout, Matrix baseline) {
    string algo = "ProgressiveRandomizedSVD";
    double exec_time = 0;

    std::cerr << "Computing " << algo << "...";
    auto t3 = std::chrono::high_resolution_clock::now();
    ProgressiveRandomizedSVD prog_algo(algo, _origin_data, _maxIter);
    auto t4 = std::chrono::high_resolution_clock::now();
    auto total_time = std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()/1e6;
    csv_write(fout, algo, INIT, 0, total_time);
    if(!tuneBatchSize) prog_algo.set_step(1);
    prog_algo.epsilon(1e-8);

    for(int i=0; i<5000; i++){
        prog_algo.partial_fit(tuneBatchSize, &exec_time);
        total_time += exec_time;
        auto eigval1 = prog_algo.eigenvalues()[0];
        auto eigval2 = prog_algo.eigenvalues()[1];
        Matrix result = prog_algo.transform();
        double error = mean_absolute_unsigned_error(baseline, result);
        csv_write(fout, algo, i+1, 0, exec_time, eigval1, eigval2, error);

        if (prog_algo.is_terminated())
            break;
    }
    std::cerr << "done: " << total_time << "s" << std::endl;
}    

static void
runIncrementalPCA(Matrix _origin_data, ofstream& fout, Matrix baseline, int inc_pca_batch_size) {
    string algo = "ProgressiveIncrementalPCA";
    // assuming an average of 4 characters per value (3+space)
    // bytes = _origin_data.rows*_origin_data.cols*4
    // assuming a bandwidth of 20MB/s, 100ms of reading is 1MB
    // number of lines per second is 20MB / (_origin_data.colsb*4)
    const int batch_size_cnt = 3;
    int batch_sizes[batch_size_cnt] = {
        2*_cols,
        5*_cols,
        1000
    };

    for(int c=0; c<batch_size_cnt; c++) {
        inc_pca_batch_size = batch_sizes[c];
        std::cerr << "Computing " << algo
                  << " inc_pca_batch_size " << inc_pca_batch_size << "...";
        double exec_time = 0;
        auto t3 = std::chrono::high_resolution_clock::now();
        IncrementalPCA prog_algo(algo, _origin_data, _maxIter, inc_pca_batch_size);
        auto t4 = std::chrono::high_resolution_clock::now();
        auto total_time = std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()/1e6;
        csv_write(fout, algo, INIT, inc_pca_batch_size, total_time);

        for(int i=0;i<5000;i++){
            prog_algo.partial_fit(false, &exec_time);
            total_time += exec_time;
            auto eigval1 = prog_algo.eigenvalues()[0];
            auto eigval2 = prog_algo.eigenvalues()[1];
            Matrix result = prog_algo.transform();
            double error = mean_absolute_unsigned_error(baseline, result);
            csv_write(fout, algo, i+1, inc_pca_batch_size, exec_time, eigval1, eigval2, error);

            if (prog_algo.is_terminated())
                break;
        }
        std::cerr << "done: " << total_time << "s" << std::endl;
    }
}    

static void
runIncrementalPCAPython(Matrix _origin_data, ofstream& fout, Matrix baseline, int inc_pca_batch_size) {
    string prog = "scikit-learn";
    string outfile = "data.csv";
    string baselinefile = "baseline.csv";
    string command = "python3 ../sklearn_benchmark.py "+outfile+" "+baselinefile;
    double total_time = 0;

    std::cerr << "Computing with " << prog << "..." << std::endl;
    DataGenerator::write_csv(outfile, _origin_data);
    DataGenerator::write_csv(baselinefile, baseline);
    int error = std::system(command.c_str());
    if (error != 0) {
        std::cerr << "Error in program " << command << std::endl;
        return;
    }
    int linenum = 0;
    for (ifstream datain("out.csv"); datain; linenum++) {
        string line;
        getline(datain, line);
        if(!line.empty() && *line.rbegin() == '\r') {
            line.erase(line.length()-1, 1);
        }
        if (line.length()==0)
            break;
        if (linenum == 0) {
            if (line != "Algorithm Stage BatchSize Duration eigenvalue1 eigenvalue2 error")
                std::cerr << "out.csv: Unexpected header" << std::endl;
            continue; // skip first line
        }
        istringstream iss(line);
        string algo;
        int stage;
        int batchsize;
        double exec_time;
        double eigenval1;
        double eigenval2;
        double error;
        iss >> algo >> stage >> batchsize >> exec_time;
        if (stage == 0) {
            csv_write(fout, algo, INIT, batchsize, exec_time);
            total_time = exec_time;
            linenum = 0;
        }
        else {
            iss >> eigenval1 >> eigenval2 >> error;
            csv_write(fout, algo, linenum, batchsize, exec_time,
                      eigenval1, eigenval2, error);
            total_time += exec_time;
        }
    }
    std::cerr << "done: " << total_time << "s" << std::endl;
}

static void
runDataset(const string& filename,
           DataGenerator * dataGenerator,
           int inc_pca_batch_size) {
    double init_time;

    auto fout = ofstream(filename);
    fout << fixed << std::setprecision(15);

     if (verbose)
          std::cerr << "Loading data ...";
    auto t3 = std::chrono::high_resolution_clock::now();
    Matrix _origin_data = dataGenerator->generateData();
    _rows = _origin_data.cols;
    _cols = _origin_data.rows;

    auto t4 = std::chrono::high_resolution_clock::now();
    init_time = std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()/1e6;
    if (verbose)
         std::cerr << "done" << 
              " size: " << _origin_data.size << " in " << init_time << "s" << std::endl;
#if NOCSV
    fout << _origin_data.size << endl;
#else
    csv_header(fout);
#endif

    Matrix baseline = runBaseline(_origin_data, fout);
    runOpenCVAll(_origin_data, fout, baseline);
    runOpenCV2(_origin_data, fout, baseline);
    runEigen(_origin_data, fout, baseline);
    runBlockPowerMethodWithMomentum(_origin_data, fout, baseline);
    runRandomized(_origin_data, fout, baseline);
    runIncrementalPCA(_origin_data, fout, baseline, inc_pca_batch_size);
    runIncrementalPCAPython(_origin_data, fout, baseline, inc_pca_batch_size);
    _origin_data.release();

    baseline_eigvec1.release();
    baseline_eigvec2.release();
}

static string create_filename(const string& dataset) {
    std::string tmp_file("ppca");
    mkdir(tmp_file.c_str(), 0777); // create the directory always
    tmp_file += "/";
    tmp_file += dataset;
    if (generate_file_date_time) {
        time_t now = time(0);
        tm *ltm = localtime(&now);
        char tmp[64];
        strftime(tmp, sizeof(tmp), "%Y%m%d_%H%M%S_Data", ltm);
        tmp_file += "_";
        tmp_file += tmp;
    }
    tmp_file += ".txt";
    if (verbose)
        std::cerr << "Results in file " << tmp_file << std::endl;
    return tmp_file;
}

static void syntax(const char * prog) {
    std::cerr << "syntax: " << prog << " <generator> [LIMIT_FOR_PCA]" << std::endl;
    std::cerr << "or: " << prog << " --help" << std::endl;
    std::cerr << " to see this message" << std::endl;
    std::cerr << "or: " << prog << " --datasets" << std::endl;
    std::cerr << "  to see the list of generators" << std::endl;
    std::cerr << "LIMIT FOR PCA is the # of dimensions above which PCA is not attempted" << std::endl;
    std::cerr << "The generators are:" << std::endl;
    std::cerr << "IRIS SINEWAVE NORMAL MNIST LFW CIFAR10 NORB" << std::endl;
}     

int main(int argc, char* argv[]) {
    int inc_pca_batch_size = 1000;
    if (argc > 1)
        generator = argv[1];
    if (generator == "--help") {
        syntax(argv[0]);
        exit(0);
    }
    else if (generator == "--datasets") {
        std::cout << "IRIS SINEWAVE NORMAL MNIST LFW CIFAR10 NORB" << std::endl;
        exit(0);
    }
    if (argc > 2) {
        char * end;
        long int v = strtol(argv[2], &end, 10);
        if (end == argv[2] || v <= 0) {
            syntax(argv[0]);
            exit(1);
        }
        TOO_LARGE = (int)v;
    }
    
    cout << "OpenCV version : " << CV_VERSION << endl;
    cout << " Max dimension for non-progressive PCA: " << TOO_LARGE << endl;
    DataGenerator * dataGenerator = nullptr;
    if (generator == "MNIST") {
        dataGenerator = new MNISTDataGenerator();
        inc_pca_batch_size = 5000;
    }
    else if (generator == "SINEWAVE") {
        dataGenerator = new SineWaveDataGenerator(1000, 2000);
        // dataGenerator = new SineWaveDataGenerator();
        inc_pca_batch_size = 10000;//450; //35000;
    }
    else if (generator == "NORMAL") {
        dataGenerator = new NormalDataGenerator(1000, 12000);
        // dataGenerator = new NormalDataGenerator();
        inc_pca_batch_size = 20000;//750;
    }
    else if (generator == "LFW") {
        dataGenerator = new FileDataGenerator("lfw_RGB.txt");
        inc_pca_batch_size = 750;
    }
    else if (generator == "CIFAR10") {
        dataGenerator = new CIFAR10DataGenerator();
        inc_pca_batch_size = 2000;
    }
    else if (generator == "NORB") {
        dataGenerator = new NorbDataGenerator();
        inc_pca_batch_size = 700;
    }
    else if (generator == "IRIS") {
        dataGenerator = new FileDataGenerator("lris.txt");
    }
    else {
        syntax(argv[0]);
        exit(1);
    }
    if (verbose)
        std::cerr << "Generator: " << generator
                  << " batch_size: " << inc_pca_batch_size << std::endl;
    runDataset(create_filename(generator),
               dataGenerator,
               inc_pca_batch_size);
    delete dataGenerator;
    return 0;
}

// cout << cv::norm(result) << endl;
// cout << prog_algo->eigenvalues()[0] << endl;
