#include <iomanip>
#include <iostream>
#include <fstream>

#include "../tools.h"
#include "../data/dataGenerator.h"

enum MEASURE {
    mean_abs_err = 0,
    eigenvalue = 1,
    eigenvector = 2
};

static const MEASURE measure = mean_abs_err;

Matrix power(Matrix data, Matrix& eigVec, Matrix& eigVecPrev, double beta, int nb_components)
{
    Matrix qq = data * eigVec;
    if(beta != 0) qq -= beta * eigVecPrev;
    Matrix eigVec_tmp = concatTwoRows(qq,eigVec);
    Matrix eigVec_stack = myQR_complet(eigVec_tmp,nb_components);
    eigVec = eigVec_stack(cv::Rect(0,0,nb_components,eigVec.rows));
    eigVecPrev = eigVec_stack(cv::Rect(0,eigVec.rows,nb_components,eigVec.rows));
    eigVec =  myQR_complet(eigVec,nb_components);
    qq.release(); eigVec_tmp.release(); eigVec_stack.release();
    return eigVec;
}

int main(){
    //auto dataGenerator = new FileDataGenerator("lris.txt");
    auto dataGenerator = new MNISTDataGenerator();
    //auto dataGenerator = new FileDataGenerator("lfw_RGB.txt");
    //auto dataGenerator = new SineWaveDataGenerator();
    //Matrix _origin_data = dataGenerator->generateData(20000,4000);
    //auto dataGenerator = new FileDataGenerator("data_rand_600Mo.txt");

    Matrix _origin_data = dataGenerator->generateData();
    Matrix _data_meanSub = meanSubtraction(_origin_data);
    Matrix _data_covar = covMat(_data_meanSub, false);

    cout << fixed << std::setprecision(15);
    srand (1);
    Matrix _eigenvectors1 = initRandomRowVector(_data_covar.rows);
    Matrix _eigenvectors2 = initRandomRowVector(_data_covar.rows);
    Matrix _eigenvectors3 = initRandomRowVector(_data_covar.rows);

    Matrix _components_2 = concatTwoCols(_eigenvectors1,_eigenvectors2);
    Matrix _components_3 = concatTwoCols(_components_2,initRandomRowVector(_eigenvectors3.rows));

    Matrix _eigenbasis_norm;
    Matrix _eigenbasis2, _eigenbasis_prev2, _baseline2;
    Matrix _eigenbasis3, _eigenbasis_prev3, _baseline3;
    double _baseline_eigval[3];
    double _eigval[3];
    double _beta;

    Matrix _baseline_result;
    Matrix _tmp_result;
    Matrix ev;

    const int _max_iter = 50;

    /// PI 1e-12 First for 3
    _eigenbasis3 = _components_3.clone();
    _eigenbasis_prev3 = cv::Mat::zeros(_eigenbasis3.rows,3,CV_64F);
    for(int i=0; i<_max_iter; i++){
        _eigenbasis_norm = _eigenbasis3.clone();
        _eigenbasis3 = power(_data_covar, _eigenbasis3, _eigenbasis_prev3, 0, 3);
        if(measureEigenvectors(_eigenbasis_norm,_eigenbasis3) < 1e-12)
            break;
    }
    _baseline3 = _eigenbasis3.clone();
    _baseline2 = _baseline3(cv::Rect(0,0,2,_baseline3.rows));

    ev = _baseline3.t() * _data_covar * _baseline3;
    for (int c = 0; c < 3; c++)
        _baseline_eigval[c] = ev.at<double>(c, c);

    if(measure == 0){
        _baseline_result = _baseline2.t() * _data_meanSub;
    }
    if(measure == 1) {
        cout << _baseline_eigval[0] << " == " << _baseline_eigval[1] << " == " << _baseline_eigval[2] << endl;
    }

    /// PI 1e-7
    cout << "***********************************" << endl;
    _eigenbasis2 = _components_2.clone();
    _eigenbasis_prev2 = cv::Mat::zeros(_eigenbasis2.rows,2,CV_64F);
    for(int i=0; i<_max_iter; i++){
        _eigenbasis_norm = _eigenbasis2.clone();
        _eigenbasis2 = power(_data_covar, _eigenbasis2, _eigenbasis_prev2, 0, 2);

        if(measure == 0){
            _tmp_result = _eigenbasis2.t() * _data_meanSub;
            cout << mean_absolute_unsigned_error(_baseline_result,_tmp_result) << endl;
        }
        if(measure == 1) {
            Matrix ev = _eigenbasis2.t() * _data_covar * _eigenbasis2;
            for (int c = 0; c < 2; c++)
                _eigval[c] = ev.at<double>(c, c);
            cout << _eigval[0] << " == " << _eigval[1] << endl;
        }

        ///cout << " >>>>>> " << measureEigenvectors(_eigenbasis_prev2, _eigenbasis2) << endl;
        if(measureEigenvectors(_eigenbasis_norm,_eigenbasis2) < 1e-7)
            break;
    }

    /// PIM opt 1e-7
    cout << "***********************************" << endl;
    _eigenbasis2 = _components_2.clone();
    _eigenbasis_prev2 = cv::Mat::zeros(_eigenbasis2.rows,2,CV_64F);
    _beta = pow(_baseline_eigval[2],2)/4;
    for(int i=0; i<_max_iter; i++){
        _eigenbasis_norm = _eigenbasis2.clone();
        _eigenbasis2 = power(_data_covar, _eigenbasis2, _eigenbasis_prev2, _beta, 2);

        if(measure == 0){
            _tmp_result = _eigenbasis2.t() * _data_meanSub;
            cout << mean_absolute_unsigned_error(_baseline_result,_tmp_result) << endl;
        }
        if(measure == 1) {
            Matrix ev = _eigenbasis2.t() * _data_covar * _eigenbasis2;
            for (int c = 0; c < 2; c++)
                _eigval[c] = ev.at<double>(c, c);
            cout << _eigval[0] << " == " << _eigval[1] << endl;
        }

        if(measureEigenvectors(_eigenbasis_norm,_eigenbasis2) < 1e-7)
            break;
    }

    /// PI Ball 1e-7
    cout << "***********************************" << endl;
    _eigenbasis2 = _components_2.clone();
    _eigenbasis_prev2 = cv::Mat::zeros(_eigenbasis2.rows,2,CV_64F);
    // use the largest eigenvalue as the initial momentum
    ev = _components_2.t() * _data_covar * _components_2;

    _beta = pow(ev.at<double>(0, 0),2)/4;
    ///_beta = pow(rayleigh(_data_covar,_components_2(cv::Rect(0,0,1,_eigenbasis2.rows))),2) /4;
    //_beta = mean(_data_covar).val[0] / _data_covar.rows;

    const double speed[5] = {2.0/3,0.99,1,1.01,1.5};
    double _eigval_tmp[2];
    double eigval_summax;
    double _beta_tmp = 0;
    Matrix _eigenbasis2_tmp,_eigenbasis_prev2_tmp;
    bool finish = false;
    for(int i=0; i<_max_iter/10 && !finish; i++){
        eigval_summax = -1;
        for(int c=0;c<5;c++){
            _eigenbasis2_tmp = _eigenbasis2.clone();
            //_eigenbasis_prev2_tmp = _eigenbasis_prev2.clone();
            _eigenbasis_prev2_tmp = cv::Mat::zeros(_eigenbasis2.rows,2,CV_64F);

            for(int t=0;t<10;t++){
                _eigenbasis2_tmp = power(_data_covar, _eigenbasis2_tmp, _eigenbasis_prev2_tmp, _beta * speed[c], 2);
            }

            Matrix ev = _eigenbasis2_tmp.t() * _data_covar * _eigenbasis2_tmp;
            _eigval_tmp[0] = ev.at<double>(0, 0);
            _eigval_tmp[1] = ev.at<double>(1, 1);

            if(eigval_summax < _eigval_tmp[0] + _eigval_tmp[1]){
                eigval_summax = _eigval_tmp[0] + _eigval_tmp[1];
                _beta_tmp = _beta * speed[c];
            }

        }
        _beta = _beta_tmp;
        ///cout << " >>> " << _beta << endl;
        _eigenbasis_prev2 = cv::Mat::zeros(_eigenbasis2.rows,2,CV_64F);

        for(int i=0; i<10; i++){
            _eigenbasis_norm = _eigenbasis2.clone();
            _eigenbasis2 = power(_data_covar, _eigenbasis2, _eigenbasis_prev2, _beta, 2);

            if(measure == 0){
                _tmp_result = _eigenbasis2.t() * _data_meanSub;
                cout << mean_absolute_unsigned_error(_baseline_result,_tmp_result) << endl;
            }
            if(measure == 1) {
                Matrix ev = _eigenbasis2.t() * _data_covar * _eigenbasis2;
                for (int c = 0; c < 2; c++)
                    _eigval[c] = ev.at<double>(c, c);
                cout << _eigval[0] << " == " << _eigval[1] << endl;
            }

            if(measureEigenvectors(_eigenbasis_norm,_eigenbasis2) < 1e-7){
                finish = true;
                break;
            }
        }

    }

    // PIM our 1e-7
    cout << "***********************************" << endl;
    _eigenbasis3 = _components_3.clone();
    _eigenbasis_prev3 = cv::Mat::zeros(_eigenbasis3.rows,3,CV_64F);

    for(int i=0; i<_max_iter; i++){
        auto _eigenbasis_norm = _eigenbasis3.clone();
        _eigenbasis3 = power(_data_covar, _eigenbasis3, _eigenbasis_prev3, 0, 3);

        if(measure == 0){
            _tmp_result = _eigenbasis3(cv::Rect(0,0,2,_baseline3.rows)).t() * _data_meanSub;
            cout << mean_absolute_unsigned_error(_baseline_result,_tmp_result) << endl;
        }
        if(measure == 1) {
            Matrix ev = _eigenbasis3.t() * _data_covar * _eigenbasis3;
            for (int c = 0; c < 3; c++)
                _eigval[c] = ev.at<double>(c, c);
            cout << _eigval[0] << " == " << _eigval[1] << endl;
        }

        if(measureEigenvectors(_eigenbasis_norm,_eigenbasis3) < 1e-2)
            break;
    }

    _eigenbasis2 = _eigenbasis3(cv::Rect(0,0,2,_eigenbasis3.rows));
    _eigenbasis_prev2 = _eigenbasis_prev3(cv::Rect(0,0,2,_eigenbasis3.rows));
    _beta = pow(_baseline_eigval[2],2)/4;
    for(int i=0; i<_max_iter; i++){
        auto _eigenbasis_norm = _eigenbasis2.clone();
        _eigenbasis2 = power(_data_covar, _eigenbasis2, _eigenbasis_prev2, _beta, 2);

        if(measure == 0){
            _tmp_result = _eigenbasis2.t() * _data_meanSub;
            cout << mean_absolute_unsigned_error(_baseline_result,_tmp_result) << endl;
        }
        if(measure == 1) {
            Matrix ev = _eigenbasis2.t() * _data_covar * _eigenbasis2;
            for (int c = 0; c < 2; c++)
                _eigval[c] = ev.at<double>(c, c);
            cout << _eigval[0] << " == " << _eigval[1] << endl;
        }

        if(measureEigenvectors(_eigenbasis_norm,_eigenbasis2) < 1e-7)
            break;
    }

    _origin_data.release();_data_meanSub.release();_data_covar.release();
    delete dataGenerator;
    return 0;
}
