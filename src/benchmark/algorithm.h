// simple version of all algorithms

#ifndef PPCA_ALGORITHM_H
#define PPCA_ALGORITHM_H

#include "../tools.h"

Matrix powerMethod(Matrix data, Matrix init, double _epsilon, int& nbIter, int maxIter, const Matrix *eigenVector, const double eigenvalue);
Matrix progressivePowerMethod(Matrix data, Matrix init, double _epsilon, int& nbIter, int maxIter,
                                    bool& finished, int step, const Matrix *eigenVector, const double eigenvalue, double &time);
void progressivePowerMethodMomentum(Matrix data, Matrix& eigVec, Matrix& eigVecPrev, double& eigenVal, double beta,
                                    double _epsilon, int& nbIter, int maxIter, bool& finished, int step,
                                    const Matrix *eigenVector, const double eigenvalue, double &time);
Matrix progressivePowerMethodMiniBatch(Matrix origin_data, Matrix init,double& eigVal, double _epsilon, int& nbIter, int maxIter,
                                        bool& finished, int step, const Matrix *eigenVector, int step_size, double &time);

Matrix generateMiniBatch(Matrix data, int _batch_size);

#endif //PPCA_ALGORITHM_H
