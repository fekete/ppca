#include "dataGenerator.h"
#include <fstream>

static std::string dataPath("ppca_input/");

void DataGenerator::setPath(const std::string& path) {
    dataPath = path;
}
std::string DataGenerator::getPath() {
    return dataPath;
}



void
DataGenerator::write_csv(const std::string& filename, Matrix mat) {
    std::ofstream out(filename, std::ofstream::binary);
    out << cv::format(mat.t(), cv::Formatter::FMT_CSV) << std::endl;
}

DataGenerator::DataGenerator(const std::string& dataset_name)
        : _current_dataset(dataset_name)
{
}

DataGenerator::~DataGenerator()
{
}

void DataGenerator::changeSeed(){
    _seed = time(NULL);
}

Matrix DataGenerator::generateData(int _dimension,int _nb_vectors){
    return generateData();
}

Matrix DataGenerator::read_file(string file){
    std::string line;

    ifstream inFile(dataPath+file);
    if (!inFile) {
        cerr << "Unable to open file " << file << endl;
        exit(1); // terminate with error
    }

    //cout << std::count(std::istreambuf_iterator<char>(inFile), std::istreambuf_iterator<char>(), '\n') << endl;

    std::getline(inFile, line);
    int n_tmp, m_tmp;
    //inFile >> n_tmp >> m_tmp;
    istringstream iss(line);
    iss >>  n_tmp >> m_tmp;

    double num;
    //inFile >> num;
    //cout << "Loading the data " ;
    Matrix DataMat = Matrix::zeros(n_tmp, m_tmp, CV_64F);

    // streampos oldpos = inFile.tellg();
    std::getline(inFile, line);
    iss.str(line);
    iss.clear();
    int count = 0;
    while (iss >> num) { count++; }
    if(count != m_tmp && count != n_tmp){
      cerr << "File error, data size incorrect. First vector contains " << count << "  numbers. "<< endl;
      return DataMat;
    }
    bool reverse = (count == n_tmp);
    // inFile.seekg (oldpos);

    if(!reverse){
        for (int i = 0; i < n_tmp; i++) {
            iss.str(line);
            iss.clear();
            count = 0;
            for (int j = 0; j < m_tmp; j++) {
                if (iss >> num) { count++; }
                DataMat.at<double>(i, j) = num;
            }
            if (count != m_tmp) {
                cerr << "File error, data size incorrect. Vector: " << i << endl;
                return DataMat;
            }
            ///progress_loading = (double)j/m_tmp;
            std::getline(inFile, line);
        }
    }
    else{
        //cout << 1 << endl;
        for (int i = 0; i < m_tmp; i++) {
            iss.str(line);
            iss.clear();
            count = 0;
            for (int j = 0; j < n_tmp; j++) {
                if (iss >> num) { count++; }
                DataMat.at<double>(j, i) = num;
            }
            if (count != n_tmp) {
                cerr << "File error, data size incorrect. Vector: " << i << endl;
                return DataMat;
            }
            std::getline(inFile, line);
            ///progress_loading = (double)j/m_tmp;
        }
    }

    if(n_tmp > 500)
        vector_length = 500;
    else vector_length = n_tmp;
    nb_vectors = m_tmp;

    if(nb_vectors > 5000)
        nb_vectors = 5000;
    perturb_time_tmp = nb_vectors;

    //printMat(DataMat);
    //cout << " ... Done " << endl;
    slider_n_min = 3;
    slider_n_max = n_tmp;
    slider_m_min = 2;
    slider_m_max = m_tmp;
    return DataMat;
}

Matrix DataGenerator::read_label(string file){
    std::string line;

    ifstream inFile(dataPath+file);
    if (!inFile) {
        cerr << "Unable to open file" << file << endl;
        exit(1); // terminate with error
    }

    std::getline(inFile, line);
    int n_tmp;
    istringstream iss(line);
    iss >>  n_tmp;

    double num;
    //inFile >> num;
    //cout << "Loading the data " ;
    Matrix LabelMat = Matrix::zeros(n_tmp, 1, CV_64F);
    for (int i = 0; i < n_tmp; i++) {
        std::getline(inFile, line);
        iss.clear();
        iss.str(line);
        if (iss >> num) {
            LabelMat.at<double>(i, 0) = num;
            ///progress_loading = (double)j/m_tmp;
        } else {
            cerr << "File error, data size incorrect. Vector: " << i << endl;
            return LabelMat;
        }
    }
    return LabelMat;
}

NormalDataGenerator::NormalDataGenerator(const std::string& dataset_name)
  : DataGenerator(dataset_name),
    _dimension(vector_length),
    _nb_vectors(nb_vectors / cluster) {}

NormalDataGenerator::NormalDataGenerator()
  : NormalDataGenerator("Normal") {}

NormalDataGenerator::NormalDataGenerator(int dimension, int nb_vectors)
  : NormalDataGenerator() {
    _dimension = dimension;
    _nb_vectors = nb_vectors;
}

Matrix
NormalDataGenerator::generateData() {
    Matrix DataMat = generateData(_dimension, _nb_vectors);
    slider_n_min = 10;
    slider_n_max = 5000;
    slider_m_min = 10;
    slider_m_max = 5000;
    return DataMat;
}

// Matrix NormalDataGenerator::generateData(){

//     // generate normal distributed data
//     Matrix _origin_data = Matrix::zeros( vector_length, nb_vectors, CV_64F );
//     cv::theRNG().state = 12345;
//     double mean = 0.0;
//     double stddev = 1.0;
//     // randn = normal distribution; randu = random
//     randn(_origin_data, cv::Scalar(mean), cv::Scalar(stddev));
//     ///progress_loading = 1;

//     slider_n_min = 10;
//     slider_n_max = 10000;
//     slider_m_min = 10;
//     slider_m_max = 10000;

//     return _origin_data;
// }

Matrix NormalDataGenerator::generateData(int _dimension,int _nb_vectors) {
    // generate normal distributed data
    Matrix _origin_data = Matrix::zeros( _dimension, _nb_vectors, CV_64F );
    cv::theRNG().state = 12345;
    double mean = 0.0;
    double stddev = 1.0;
    randn(_origin_data, cv::Scalar(mean), cv::Scalar(stddev));
    //randu(_origin_data, cv::Scalar(-1), cv::Scalar(1));

    //Matrix a,b;
    //cv::meanStdDev ( _origin_data.col(0), a, b );

    slider_n_min = 10;
    slider_n_max = 10000;
    slider_m_min = 10;
    slider_m_max = 10000;
    return _origin_data;
}


SineWaveDataGenerator::SineWaveDataGenerator()
  : NormalDataGenerator("Sine wave") {}

SineWaveDataGenerator::SineWaveDataGenerator(int dimension, int nb_vectors)
  : SineWaveDataGenerator() {
    _dimension = dimension;
    _nb_vectors = nb_vectors;
}

vector<vector<double>> SineWaveDataGenerator::generateGridData(double fmin, double fmax, double omin, double omax, int fn, int on){
    vector<vector<double>> vectors;
    double fdiff = (fmax - fmin)/(fn+1);
    double odiff = (omax - omin)/(on+1);
    for(double f = fmin+fdiff; f < fmax; f += fdiff) {
        for (double o = omin+odiff; o < omax; o += odiff){
            double val[2] = {f,o};
            vector<double> v(val,val+2);
            vectors.push_back(v);
        }
    }
    //for (int i = fn * on -1; i >= 0; i--)
    //   cout << vectors[i][0] << " = " << vectors[i][1] << "\n";
    return vectors;
}

vector<vector<double>> SineWaveDataGenerator::perturbParam(vector<vector<double>> vec, int times){
    vector<vector<double>> vectors;
    for(int i = 0; i < cluster; i++) {
        // basic parameters
        double val[2] = {vec[i][0], vec[i][1]};
        vector<double> v(val, val + 2);
        vectors.push_back(v);

        // perturb the basic parameters for (t-1) times
        for(int j=1;j<times;j++) {
            double val[2] = {vec[i][0] + randDouble(-perturb_value_freq, perturb_value_freq), vec[i][1] + randDouble(-perturb_value_offset, perturb_value_offset)};
            vector<double> v(val, val + 2);
            vectors.push_back(v);
        }
    }
    return vectors;
}

// Given a vector (freq,offset), generate n values using sine function
vector<double> SineWaveDataGenerator::generateSineValues(vector<double> v, int length){
    vector<double> result(length);
    for(int i=0;i<length;i++){
        ////double value = 1 * sin(2 * M_PI * (i / v[0] * 10.0)  + v[1] / maxOffset*2 * M_PI );

        double value = 1 * sin((M_PI * v[0])/1800 * i  + v[1] * M_PI /1800 );
        result[i] = value;
    }
    return result;
}

Matrix SineWaveDataGenerator::generateData(int _dimension,int _nb_vectors){
    Matrix DataMat = Matrix::zeros( _dimension, _nb_vectors * cluster, CV_64F );
    vector<vector<double>> vectors = generateGridData(100,500,0,100,2,3);
    vector<vector<double>> vectorsP = perturbParam(vectors, _nb_vectors);

    vector<int> shuffle = generateShuffleVector(_nb_vectors * cluster);

    for ( int j=0; j<_nb_vectors * cluster; j++ )
    {
        vector<double> vec = generateSineValues(vectorsP[j],_dimension);
        for ( int i=0; i<_dimension; i++ )
        {
            DataMat.at<double>(i, j) = vec[i]; //shuffle[j]////
        }
        ///progress_loading = (double)j/nb_vectors;
    }
    return DataMat;
}

static inline int  reverseInt(int i) {
    unsigned char c1, c2, c3, c4;

    c1 = i & 255;
    c2 = (i >> 8) & 255;
    c3 = (i >> 16) & 255;
    c4 = (i >> 24) & 255;

    return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
}

// code from https://blog.csdn.net/wanty_chen/article/details/80139450
Matrix MNISTDataGenerator::read_mnist_image(const string fileName) {
    ifstream file(fileName, ios::binary);

    if (! file) {
        cerr << "Cannot find mnist dataset " << fileName << endl;
        exit(1);
    }
    file.read((char*)&magic, sizeof(magic));
    file.read((char*)&num_items, sizeof(num_items));
    file.read((char*)&rows, sizeof(rows));
    file.read((char*)&cols, sizeof(cols));
    //cout << magic_number << " " << number_of_images << " " << n_rows << " " << n_cols << endl;

    magic = reverseInt(magic);
    num_items = reverseInt(num_items);
    rows = reverseInt(rows);
    cols = reverseInt(cols);
    unsigned size = rows*cols;
    unsigned char* image = new unsigned char[size];

    Matrix DataMat(size, num_items, CV_64F);
    for (unsigned i = 0; i < num_items; i++) {
        file.read((char*)image, size);
        const unsigned char * ptr = image;
        for (unsigned j = 0; j < rows * cols; j++) {
            double pixel_value = double(*ptr++) / 255.0;
            DataMat.at<double>(j, i) = pixel_value;
        }
    }
    delete[] image;
    return DataMat;
}

Matrix MNISTDataGenerator::read_mnist_label_idx1_ubyte(const string fileName) {
    int magic_number;

    Matrix LabelMat;

    ifstream file(fileName, ios::binary);
    if (! file) {
        cerr << "Cannot find mnist label dataset" << endl;
        exit(1);
    }      
    file.read((char*)&magic_number, sizeof(magic_number));
    file.read((char*)&num_labels, sizeof(num_labels));
    magic_number = reverseInt(magic_number);
    num_labels = reverseInt(num_labels);
    LabelMat = Matrix::zeros(1, num_labels, CV_64F);
    unsigned char * labels = new unsigned char[num_labels];
    file.read((char*)labels, num_labels);
    if (! file) {
        cerr << "Cannot find mnist label dataset" << endl;
        exit(1);
    }
    for (unsigned i = 0; i < num_labels; i++) {
        LabelMat.at<double>(0, i) = double(labels[i]);
    }
    delete []labels;
    return LabelMat;
}

Matrix MNISTDataGenerator::read_mnist_label_idx2_int(const string fileName) {
    int magic_number;
    int number_of_col;

    Matrix LabelMat;

    ifstream file(fileName, ios::binary);
    if (! file) {
        cerr << "Cannot find mnist label dataset" << endl;
        exit(1);
    }
    file.read((char*)&magic_number, sizeof(magic_number));
    file.read((char*)&num_labels, sizeof(num_labels));
    file.read((char*)&number_of_col, sizeof(number_of_col));

    magic_number = reverseInt(magic_number);
    num_labels = reverseInt(num_labels);
    number_of_col = reverseInt(number_of_col);

    LabelMat = Matrix::zeros(num_labels, 1, CV_64F);
    for (unsigned i = 0; i < num_labels; i++) {
        for(int j=0;j<number_of_col;j++){
            int temp = 0;
            file.read((char*)&temp, sizeof(temp));
            if( j == 0){
                LabelMat.at<double>(i, 0) = (double)reverseInt(temp);
            }
        }
    }
    return LabelMat.t();
}

Matrix MNISTDataGenerator::generateData(){
    _mnist_dataset = read_mnist_image(dataPath+"MNIST/train-images-idx3-ubyte");
    _mnist_dataset_label = read_mnist_label_idx1_ubyte(dataPath+"MNIST/train-labels-idx1-ubyte");

    ///_mnist_dataset = read_mnist_image("../../input/MNIST/xnist-images-idx3-ubyte");
    ///_mnist_dataset_label = read_mnist_label_idx2_int("../../input/MNIST/xnist-labels-idx2-int");

    ///progress_loading = 1;

    vector_length = 784;
    perturb_time_tmp = 1000;

    slider_n_min = 10;
    slider_n_max = rows*cols;
    slider_m_min = 10;
    slider_m_max = num_items;

    //cout << _mnist_dataset.size << endl;

    return _mnist_dataset;
}

int MNISTDataGenerator::getPointColor(int index){
    if(index >= 0 && unsigned(index) < num_items)
        //return int(_mnist_dataset_label->at<char>(0,index));
        return int(_mnist_dataset_label.at<double>(0,index));
    //else
    //    cerr << "Index Error" << endl;
    return -1;
}

Matrix FileDataGenerator::generateData(){
    return read_file(current_dataset);
}

Matrix CIFAR10DataGenerator::generateData(){
    _dataset = read_file("CIFAR10/CIFAR10.txt");
    _label = read_label("CIFAR10/CIFAR10_label.txt");
    ///progress_loading = 1;

    /*
    Matrix gray = Matrix::zeros(_dataset.rows/3,_dataset.cols,_dataset.type());
    for(int i=0;i<gray.rows;i++){
        for(int j=0; j<gray.cols;j++){
            gray.at<double>(i,j) = (_dataset.at<double>(i,j) + _dataset.at<double>(_dataset.rows/3 + i,j) + _dataset.at<double>(_dataset.rows/3 * 2 + i,j)) / 3;
        }
    }
    slider_n_max = slider_n_max / 3;
        return gray;
        */
    return _dataset;
}

int CIFAR10DataGenerator::getPointColor(int index){
    if(index < _label.size[0] && index >= 0)
        //return int(_mnist_dataset_label->at<char>(0,index));
        return int(_label.at<double>(index,0));
    //else
    //    cerr << "Index Error" << endl;
    return -1;
}

Matrix NorbDataGenerator::generateData(){
    _dataset = read_file("NORB/norb.txt");
    _label = read_label("NORB/norb_label_cat.txt"); //norb_label_cat
    ///progress_loading = 1;
    //cout << _dataset.size << endl;
    //cout << _label.size << endl;
    return _dataset;
}

int NorbDataGenerator::getPointColor(int index){
    if(index < _label.size[0] && index >= 0)
        return int(_label.at<double>(index,0));
    //else
    //    cerr << "Index Error" << endl;
    return -1;
}
