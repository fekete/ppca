#ifndef PPCA_DATAGENERATOR_H
#define PPCA_DATAGENERATOR_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <array>
#include "../global_val.h"
#include "../tools.h"

using namespace std;

class DataGenerator {
public:
    static void setPath(const std::string& path);
    static std::string getPath();
    static void write_csv(const std::string& filename, Matrix mat);
    DataGenerator(const std::string& dataset_name);
    virtual ~DataGenerator();
    const std::string& name() const { return _current_dataset; }
    void changeSeed();
    virtual Matrix generateData(int _dimension,int _nb_vectors);
    virtual Matrix generateData() = 0;
    ///virtual int getPointColor(int index);

protected:
    std::string _current_dataset;
    Matrix read_file(string file);
    Matrix read_label(string file);
};

class NormalDataGenerator : public DataGenerator {
public:
    NormalDataGenerator();
    NormalDataGenerator(int dimension, int nb_vetor);
    Matrix generateData(int _dimension,int _nb_vectors) override;
    Matrix generateData() override;
protected:
    NormalDataGenerator(const std::string& dataset_name);
    int _dimension;
    int _nb_vectors;
};

class SineWaveDataGenerator : public NormalDataGenerator {
public:
    SineWaveDataGenerator();
    SineWaveDataGenerator(int dimension, int nb_vetor);
    Matrix generateData(int _dimension,int _nb_vectors) override;

protected:
    vector<vector<double>> generateGridData(double fmin, double fmax, double omin, double omax, int fn, int on);
    vector<vector<double>> perturbParam(vector<vector<double>> vec, int times);
    vector<double> generateSineValues(vector<double> v, int length);
};

class CIFAR10DataGenerator : public DataGenerator {
public:
    CIFAR10DataGenerator() : DataGenerator("CIFAR10") {};
    Matrix generateData() override;
    int getPointColor(int index);
private:
    Matrix _dataset;
    Matrix _label;
};

class NorbDataGenerator : public DataGenerator {
public:
    NorbDataGenerator() : DataGenerator("NORB") {};
    Matrix generateData() override;
    int getPointColor(int index);
private:
    Matrix _dataset;
    Matrix _label;
};

class MNISTDataGenerator : public DataGenerator {
public:
    MNISTDataGenerator():DataGenerator("MNIST"){};
    Matrix generateData() override;
    int getPointColor(int index);

private:
    ///uint32_t swap_endian(uint32_t val);
    ///void read_mnist_cv(const char* image_filename, const char* label_filename);
    Matrix read_mnist_label_idx1_ubyte(const string fileName);
    Matrix read_mnist_label_idx2_int(const string fileName);
    Matrix read_mnist_image(const string fileName);

    uint32_t magic;
    uint32_t num_items;
    uint32_t num_labels;
    uint32_t rows;
    uint32_t cols;

    Matrix _mnist_dataset;
    Matrix _mnist_dataset_label;
};

class FileDataGenerator : public DataGenerator {
public:
    FileDataGenerator():DataGenerator(current_dataset){};
    FileDataGenerator(string name):DataGenerator(name){ current_dataset = name; };
    Matrix generateData() override;

protected:
    // TODO: Check the dataset format
    bool checkFile();
};

#endif //PPCA_DATAGENERATOR_H
