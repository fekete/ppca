#ifndef PPCA_WORKER_H
#define PPCA_WORKER_H

#include <thread>
#include <mutex>
#include <atomic>
#include <iostream>

#include "global_val.h"
#include "algo/myPCA.h"
#include "data/dataGenerator.h"
#include "benchmark.h"
#include "algo/ppca_core.h"

using namespace std;

Matrix generateData(string dataSet);


class Worker {
public:
    Worker();
    ~Worker() {_worker.join();};
    void updatePoints();
    void update_thread();
    void startUpdatePoints();
    void startBenchMark();
    void progressive_pause_continue();
    void abort_calculation();
    void quit();
    bool getDisableWidgetsRequest();
    void changeSeed();
    GLdouble* getVertexArray();
    int getPointColor(int index);
    int number_points_calculated();

protected:
    Matrix generateData(string dataSet);

private:
    BenchMark *benchmark;
    myPCA *mypca;
    DataGenerator *dataGenerator;
    ProgressiveStateSingleton* _progressiveStateSingleton;
    std::thread _worker;
    double generate_data_time;

    Matrix DataMat;
    Matrix DataMat_tmp;

    bool _reset_PCA_settings;
    bool _quit_thread;
    std::atomic<bool> _disable_all_widgets_request;

    GLdouble* pointVertex;
    GLdouble* pointVertex_tmp;

};

#endif //PPCA_WORKER_H
