#include "global_val.h"
#include <fstream>

int vector_length = 100;
const int cluster = 6;
int current_cluster = 6;
int perturb_time = 100;
int nb_vectors = perturb_time * cluster;
int perturb_value_freq = 30;
int perturb_value_offset = 6;
const int maxOffset = 100;

// for the widget
int perturb_time_tmp = perturb_time;

bool need_load_file = false;

double ZOOM = 1;
double off_x = 0;
double off_y = 0;

unsigned int _seed = 1;

int slider_n_min = 10;
int slider_n_max = 5000;
int slider_m_min = 10;
int slider_m_max = 5000;

std::string current_dataset = "Sine wave";
std::string current_dataset_render = "Sine wave";

int stream_count = 0;
// must follow: length % row = 0
int stream_max_length = 100;
std::string* column_stream = new std::string[stream_max_length];

// control multi-thread
std::mutex mut;
std::condition_variable condv;

bool workerExec = false;
std::atomic<bool> readyToPrint(false);

bool printTimeRes = false;

bool show_result_window = true;

#ifdef PPCA_GUI
glm::mat4 trans;
#endif

