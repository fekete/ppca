#include "worker.h"

extern void resetPlot(); // from main.cpp

Worker::Worker(){
    benchmark = new BenchMark();
    mypca = nullptr;
    dataGenerator = nullptr;
    _reset_PCA_settings = true;
    _quit_thread = false;
    _worker = std::thread(&Worker::update_thread, this);
    _disable_all_widgets_request.store(false);
}

Matrix Worker::generateData(string dataSet){
    srand (_seed);
    if(!dataSet.compare("Sine wave")) {
        if(need_load_file) {
            progress_loading = 0.0;
            resetParam(false);
            need_load_file = false;
        }
        resetPlot();
        dataGenerator = new SineWaveDataGenerator();
        DataMat.release();
        DataMat = dataGenerator->generateData();
        // Change the current dataset after it has loaded (Put this line in the end for all methods), otherwise the program will crash.
        current_dataset_render = current_dataset;
        return DataMat;
    }
    else if(!dataSet.compare("Normal")) {
        if(need_load_file) {
            progress_loading = 0.0;
            resetParam(false);
            need_load_file = false;
        }
        resetPlot();
        dataGenerator = new NormalDataGenerator();
        DataMat.release();
        DataMat = dataGenerator->generateData();
        current_dataset_render = current_dataset;
        return DataMat;
    }
    else{
        if(need_load_file) {
            progress_loading = 0.0;
            if(!dataSet.compare("MNIST")){ dataGenerator = new MNISTDataGenerator(); }
            else if(!dataSet.compare("CIFAR10")){ dataGenerator = new CIFAR10DataGenerator(); }
            else if(!dataSet.compare("NORB")){ dataGenerator = new NorbDataGenerator(); }
            else dataGenerator = new FileDataGenerator();
            DataMat.release();
            DataMat = dataGenerator->generateData();
            need_load_file = false;
        }
        resetPlot();
        Matrix subDataMat = DataMat( cv::Rect( 0, 0,  perturb_time_tmp , vector_length ) );
        current_dataset_render = current_dataset;
        return subDataMat;
    }
}

void Worker::updatePoints(){
    if(_reset_PCA_settings){
        auto t1 = std::chrono::high_resolution_clock::now();
        DataMat_tmp = generateData(current_dataset);
        //cout << DataMat.size << endl;
        //cout << DataMat_tmp.size << endl;

        mypca = new myPCA(DataMat_tmp);
        _reset_PCA_settings = false;

        auto t2 = std::chrono::high_resolution_clock::now();
        generate_data_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;
    }

    mypca->setPrintTimeRes(printTimeRes);
    Matrix DataMatProj = mypca->calculate();
/// disable this when benchmark

    if(!pointVertex_tmp) free(pointVertex_tmp);
    pointVertex_tmp = new GLdouble[nb_vectors * 2];
    // Load points
    for ( int j=0; j<nb_vectors && j<DataMatProj.cols; j++) {
        pointVertex_tmp[2 * j] = DataMatProj.at<double>(0, j);
        pointVertex_tmp[2 * j + 1] = DataMatProj.at<double>(1, j);
    }

    pointVertex = pointVertex_tmp;
    ///
    readyToPrint.store(true);
}

void Worker::update_thread()
{
    unsigned int update_delay = 0;
    while(true){
        _disable_all_widgets_request.store(false);

        if(benchmark->status() == BenchMark::state_waiting){
            std::unique_lock<std::mutex> lk(mut);
            condv.wait(lk, []{return workerExec;});
        }

        std::time_t start_time = std::time(nullptr);
        auto t2 = std::chrono::high_resolution_clock::now();

        if(_quit_thread) break;
        _progressiveStateSingleton->getState()->_abort_calculation = false;
        _progressiveStateSingleton->getState()->_pause_calculation = false;
        _reset_PCA_settings = true;
        _disable_all_widgets_request.store(true);

        generate_data_time = 0;
        do{
            updatePoints();
            while(_progressiveStateSingleton->getState()->_pause_calculation)  this_thread::sleep_for(chrono::milliseconds(update_delay));
        } while(!_progressiveStateSingleton->getState()->_abort_calculation && _progressiveStateSingleton->getState()->_start_progressive);

        auto t3 = std::chrono::high_resolution_clock::now();
        double time_exec2 = std::chrono::duration_cast<std::chrono::milliseconds>(t3 - t2).count()/1000.0 - generate_data_time;

        if(printTimeRes)
            cout << "\n[Global] PCA exec time: " << time_exec2 << " s\n";

        if(show_result_window /*&& !_progressiveStateSingleton->getState()->_abort_calculation*/) {
            column_stream[stream_count++] = to_string(start_time);
            column_stream[stream_count++] = to_string(time_exec2);// + "s";
            column_stream[stream_count++] = to_string(nb_vectors);
            column_stream[stream_count++] = to_string(vector_length);
            column_stream[stream_count++] = mypca->getMethod();

            if(stream_count >= stream_max_length){
                // reallocate the result buffer
                std::string* tmp_stream = new std::string[stream_max_length * 2];
                std::copy(column_stream, column_stream + stream_max_length, tmp_stream);
                column_stream = tmp_stream;
                stream_max_length *= 2;
                cout << "Reallocate : " << stream_max_length << endl;
            }
        }

        _progressiveStateSingleton->getState()->_start_progressive = false;
        delete mypca; mypca = nullptr;
        if(benchmark->status() == BenchMark::state_waiting)
            workerExec = false;
        else{
            benchmark->exec();
        }
    }
}

// unlock the worker thread
void Worker::startUpdatePoints(){
    workerExec = true;
    condv.notify_all();
}

void Worker::startBenchMark(){
    int n_min = 1000;
    int n_max = 6000;
    int dim_min = 1000;
    int dim_max = 6000;
    int step_n = 1000;
    int step_dim = 1000;

    benchmark->init(dim_min,dim_max,step_dim,n_min,n_max,step_n);
    benchmark->setStatus(BenchMark::state_running);

    workerExec = true;
    condv.notify_all();

}

void Worker::progressive_pause_continue(){
    _progressiveStateSingleton->getState()->_pause_calculation = !_progressiveStateSingleton->getState()->_pause_calculation;
}

void Worker::abort_calculation(){
    _progressiveStateSingleton->getState()->_abort_calculation = true;
}

void Worker::quit(){
    _quit_thread = true;
}

bool Worker::getDisableWidgetsRequest(){
    return _disable_all_widgets_request.load();
}

void Worker::changeSeed(){
    dataGenerator->changeSeed();
}

GLdouble* Worker::getVertexArray(){
    return pointVertex;
}

int Worker::getPointColor(int index){
    MNISTDataGenerator* mdg = dynamic_cast<MNISTDataGenerator*>(dataGenerator);
    if(mdg != nullptr) {
        return mdg->getPointColor(index);
    }
    CIFAR10DataGenerator* cdg = dynamic_cast<CIFAR10DataGenerator*>(dataGenerator);
    if(cdg != nullptr) {
        return cdg->getPointColor(index);
    }
    NorbDataGenerator* ndg = dynamic_cast<NorbDataGenerator*>(dataGenerator);
    if(ndg != nullptr) {
        return ndg->getPointColor(index);
    }
    return -1;
}

int Worker::number_points_calculated(){
    if(mypca == nullptr)
        return 0;
    //cout << mypca->number_points_calculated() << endl;
    return mypca->number_points_calculated();
}