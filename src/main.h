#ifndef PPCA_MAIN_H
#define PPCA_MAIN_H

#include <iostream>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <vector>
#include <random>
#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <algorithm>
#include <ctime>
#include <array>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <iomanip>
#include <zconf.h>
#include "./imgui/imgui.h"
#include "./imgui/imgui_impl_glfw.h"
#include "./imgui/imgui_impl_opengl3.h"
#include "./imgui/imgui_internal.h"
#include "./imgui/imgui_plot.h"

#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
//#include "/usr/local/Cellar/glew/2.1.0/include/GL/glew.h"
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>  // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

#ifdef _WIN32
#include <Windows.h>
#endif

#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

#include <omp.h>

#include <GLFW/glfw3.h>
#include "tools.h"
#include "global_val.h"
#include "worker.h"
#include "window_aux.h"

using namespace std;

const int SCREEN_WIDTH = 1280;//640
const int SCREEN_HEIGHT = 720;//480

Worker* worker;

#if __APPLE__
const char *vertexShaderSource = "#version 330 core\n"
                                 "uniform mat4 transform;\n"
                                 "layout (location = 0) in vec2 aPos;\n"
                                 "void main() {\n"
                                 "   gl_Position =  transform * vec4(aPos.x, aPos.y, 0.0, 1.0) ;\n"
                                 "}\n\0";

const char *fragmentShaderSource = "#version 330 core\n"
                                   "precision highp float;\n"
                                   "out vec4 FragmentColor;\n"
                                   "uniform vec4 ourColor;\n"
                                   "void main() {\n"
                                   "   FragmentColor = ourColor;\n" //vec4(1.0f, 0.5f, 0.2f, 1.0f)
                                   "}\n\0";
#else
const char *vertexShaderSource = "#version 300 es\n"
                                 "uniform mat4 transform;\n"
                                 "layout (location = 0) in vec2 aPos;\n"
                                 "void main() {\n"
                                 "   gl_Position =  transform * vec4(aPos.x, aPos.y, 0.0, 1.0) ;\n"
                                 "}\n\0";

const char *fragmentShaderSource = "#version 300 es\n"
                                   "precision highp float;\n"
                                   "out vec4 FragmentColor;\n"
                                   "uniform vec4 ourColor;\n"
                                   "void main() {\n"
                                   "   FragmentColor = ourColor;\n" //vec4(1.0f, 0.5f, 0.2f, 1.0f)
                                   "}\n\0";
#endif

#endif
