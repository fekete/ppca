#include "benchmark.h"

void BenchMark::init(int dim_min,int dim_max, int dim_step, int n_min, int n_max, int n_step){
    if(dim_min > dim_max || n_min > n_max || n_step == 0 || dim_step == 0){
        cerr << "Benchmark range error" << endl;
        return;
    }
    _current_nb = 0;
    _current_dim = 0;

    _max_nb = (n_max-n_min)/n_step +1;

    if(_max_nb > 100) _max_nb = 100;
    test_range_nb = new int[_max_nb];
    for(int i=0; i<_max_nb; i++)
        test_range_nb[i] = n_min + i * n_step;

    _max_dim = (dim_max-dim_min)/dim_step + 1;
    if(_max_dim > 100) _max_dim = 100;
    test_range_dim = new int[_max_dim];
    for(int i=0; i<_max_dim; i++)
        test_range_dim[i] = dim_min + i * dim_step;

}

void BenchMark::exec(){
    if(_current_dim >= _max_dim){
        _state = state_waiting;
        return;
    }

    vector_length = test_range_dim[_current_dim];
    perturb_time_tmp = test_range_nb[_current_nb];

    if(!current_dataset.compare("Sine wave")){
        nb_vectors = perturb_time_tmp * cluster;
    }
    else{
        nb_vectors = perturb_time_tmp;
    }

    _current_nb++;
    if( _current_nb >= _max_nb){
        _current_nb = 0;
        _current_dim++;
    }
}