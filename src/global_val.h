#ifndef PPCA_GLOBAL_VAL_H
#define PPCA_GLOBAL_VAL_H

#include <string>
#include <atomic>
#include <mutex>
#include <condition_variable>
#ifdef PPCA_GUI
#include <GL/glew.h>
// Shut up warning messages from GLM
#define GLM_FORCE_RADIANS 
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
extern glm::mat4 trans;
#endif

#include "tools.h"

extern double progress_loading; // in main.cpp

extern int vector_length;

extern const int cluster;
extern int current_cluster;
// This value is directly linked to the size of buffer and it mustn't be updated unless all the calculation are done, change .._tmp instead.
extern int perturb_time;
extern int nb_vectors;
extern int perturb_value_freq;
extern int perturb_value_offset;
extern const int maxOffset;

// for the widget
extern int perturb_time_tmp;

extern bool need_load_file;

extern double ZOOM;
extern double off_x;
extern double off_y;

extern int slider_n_min;
extern int slider_n_max;
extern int slider_m_min;
extern int slider_m_max;

extern std::string current_dataset;
extern std::string current_dataset_render;

extern std::ofstream output_CSVfile;

extern int stream_count;
// must follow: length % row = 0
extern int stream_max_length;
extern std::string* column_stream;

// control multi-thread
extern std::mutex mut;
extern std::condition_variable condv;

extern bool workerExec;
extern std::atomic<bool> readyToPrint;

extern bool printTimeRes;

extern bool show_result_window;

void resetParam(bool update);

extern unsigned int _seed;

//extern const char* plot_text;


#endif //PPCA_GLOBAL_VAL_H
