// Benchmark with UI

#ifndef PPCA_BENCHMARK_H_UI
#define PPCA_BENCHMARK_H_UI

#include "global_val.h"

using namespace std;

class BenchMark{
public:
    BenchMark() { _state = state_waiting; };
    void init(int dim_min,int dim_max, int dim_step, int n_min, int n_max, int n_step);
    void exec();

    enum state_benchmark {
        state_waiting = 0,
        state_running = 1
    };

    state_benchmark status(){ return _state; };
    void setStatus(state_benchmark s){ _state = s; }


protected:
    int* test_range_nb;
    int* test_range_dim;

private:
    state_benchmark _state;
    int _current_nb;
    int _current_dim;
    int _max_nb;
    int _max_dim;
};

#endif //PPCA_BENCHMARK_H_UI
