#include "window_aux.h"

void updateZoom(){
    trans = glm::mat4(1.0f);
    trans = glm::translate(trans,glm::vec3(off_x,off_y,0.0));
    trans = glm::scale(trans, glm::vec3(ZOOM, ZOOM, 1.0));
}

void testComplete2(){
    //resetParam(true);
    int i,p;
    double max = 1000;
    cout << 9;
    std::unique_lock<std::mutex> lk2(mut);
    cout << 0;
    for(i=100;i<max+1;i+=100,vector_length=i) {
        perturb_time_tmp = 100;
        for (p = 100; p < max+1; p += 100, perturb_time_tmp = p) {
            //startUpdatePoints();
            cout << 1;
            condv.wait(lk2, []{return workerExec;});
            cout << 2;
        }
        cout << i/max * 100 <<  "% done. " << endl;
    }
}


// Keyboard interaction
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    if (key == GLFW_KEY_Z && action == GLFW_PRESS){
        ZOOM /=2;
        updateZoom();
        cout << "ZOOM updated: " << ZOOM << "\n";
    }
    if (key == GLFW_KEY_X && action == GLFW_PRESS){
        ZOOM *=2;
        updateZoom();
        cout << "ZOOM updated: " << ZOOM << "\n";
    }
    /*
     *  @deprecated
    if (key == GLFW_KEY_N && action == GLFW_PRESS){
        if(vector_length<300) vector_length+=10;
        else if(vector_length<1000) vector_length+=100;
        else if(vector_length<10000) vector_length+=1000;
        else vector_length+=10000;
        startUpdatePoints();
        cout << "vector length updated: " << vector_length << "\n";
    }
    if (key == GLFW_KEY_M && action == GLFW_PRESS){
        if(vector_length>10){
            if(vector_length<300) vector_length-=10;
            else if(vector_length<1000) vector_length-=100;
            else if(vector_length<10000) vector_length-=1000;
            else vector_length-=10000;
            startUpdatePoints();
        }
        cout << "Vector length value updated: " << vector_length << "\n";
    }
    if (key == GLFW_KEY_1 && action == GLFW_PRESS){
        perturb_value_freq +=1;
        startUpdatePoints();
        cout << "perturb value for frequence updated: " << perturb_value_freq << "\n";
    }
    if (key == GLFW_KEY_2 && action == GLFW_PRESS){
        if(perturb_value_freq>1) {
            perturb_value_freq -=1;
            startUpdatePoints();
        }
        cout << "perturb value for frequence updated: " << perturb_value_freq << "\n";
    }
    if (key == GLFW_KEY_3 && action == GLFW_PRESS){
        perturb_value_offset +=1;
        startUpdatePoints();
        cout << "perturb value for frequence updated: " << perturb_value_offset << "\n";
    }
    if (key == GLFW_KEY_4 && action == GLFW_PRESS){
        if(perturb_value_offset>1) {
            perturb_value_offset -=1;
            startUpdatePoints();
        }
        cout << "perturb value for frequence updated: " << perturb_value_offset << "\n";
    }
    if (key == GLFW_KEY_P && action == GLFW_PRESS){
        if(perturb_time<1000) perturb_time+=100;
        else if(perturb_time<10000) perturb_time+=1000;
        else perturb_time+=10000;
        nb_vectors = perturb_time * cluster;
        startUpdatePoints();
        cout << "perturb times updated: " << perturb_time << "\n";
    }
    if (key == GLFW_KEY_O && action == GLFW_PRESS){
        if(perturb_time>100){
            if(perturb_time<=1000) perturb_time-=100;
            else if(perturb_time<=10000) perturb_time-=1000;
            else perturb_time-=10000;
            nb_vectors = perturb_time * cluster;
            startUpdatePoints();
        }
        cout << "perturb times updated: " << perturb_time << "\n";
    }
    if (key == GLFW_KEY_R && action == GLFW_PRESS){
        resetParam(true);
        cout << " All param reseted \n";
    }

    if (key == GLFW_KEY_T && action == GLFW_PRESS){
        bool printT = printTimeRes;
        printTimeRes = false;
        //std::thread testThread(testComplete);
        //testComplete();
        //resetParam(true);
        startBenchMark();
        printTimeRes = printT;
    }
     */
}


void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}