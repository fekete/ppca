#ifndef PPCA_WINDOW_AUX_H
#define PPCA_WINDOW_AUX_H

#include "./imgui/imgui.h"
#include "global_val.h"
#include "worker.h"
#include <GLFW/glfw3.h>

void HelpMarker(const char* desc);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void updateZoom();

#endif //PPCA_WINDOW_AUX_H
