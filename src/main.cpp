#include "main.h"
#include "algo/myPCA.h"

double progress_loading = 0.0;
static double progress_calculating_first = 0.0;
static double progress_calculating_second = 0.0;
static float plot_values1[60];
static float plot_values2[60];
static std::string convergence_res = "Here shows the partial results.";
std::atomic<bool> disable_all_widgets(false);

static void printPartialResults(double epsilon, int nbIters1, int nbIters2, 
                                double time1, double time2) {
    std::stringstream ss;
    ss <<  "Epsilon: " << epsilon << endl
        << "First eigenvector :" << endl
        << "Converge after "<< nbIters1 << " iterations. " << endl
        << "Each interation takes " << time1 << " s" << endl << endl
        << "Second eigenvector :" << endl
        << "Converge after " << nbIters2 << " iterations. " << endl
        << "Each interation takes " << time2 << " s";

    convergence_res = ss.str();
}


void resetParam(bool update){
    vector_length = 1000;
    perturb_value_freq = 30;
    perturb_value_offset = 6;
    perturb_time_tmp = 1000;
    nb_vectors = perturb_time_tmp * cluster;
    ZOOM = 1;
    off_x = 0;
    off_y = 0;
    if(update) {
        worker->startUpdatePoints();
        updateZoom();
    }
}

void resetPlot() {
    progress_calculating_first = 0.0;
    progress_calculating_second = 0.0;

    for(int i=0;i<60;i++){
        plot_values1[i] = 0;
        plot_values2[i] = 0;
    }

    //plot_values_offset1 = 59;
    //plot_values_offset2 = 59;
}

static void updatePlot(double current_result, double epsilon) 
{
    if(PCA::progress_update_first){
        progress_calculating_first = log10(abs(current_result))/log10(abs(epsilon));
        memmove(plot_values1,plot_values1+1,sizeof(float)*(59));
        plot_values1[59] = log10(abs(current_result));
        //plot_values_offset1++;

        //plot_text = std::to_string(current_result).c_str();
    }
    if(PCA::progress_update_second){
        progress_calculating_second = log10(abs(current_result))/log10(abs(epsilon));

        memmove(plot_values2,plot_values2+1,sizeof(float)*(59));
        plot_values2[59] = log10(abs(current_result));

        //*(plot_values2+plot_values_offset2) = log10(current_result);
        //plot_values_offset2++;
    }
}

int main() {
    if (!glfwInit())
        return -1;
    PCA::updatePlotCb = updatePlot;
    PCA::partialResultsCb = printPartialResults;
/**/
#if __APPLE__
    // not this -> GL 3.2 + GLSL 150
    // GL 4.1 + GLSL 410
    // https://support.apple.com/en-us/HT202823
    // https://en.wikipedia.org/wiki/OpenGL_Shading_Language
    const char* glsl_version = "#version 330"; //410 //#version 300 es
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 300 es";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Data projection", NULL, NULL);
    if (!window) {
        glfwTerminate();
        worker->quit();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);

    glfwSwapInterval(1);  // avoid wasting a lot of CPU and GPU cycles.



#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);
    // check for shader compile errors
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // fragment shader
    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    // check for shader compile errors
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // link shaders
    int shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    // check for linking errors
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);


    //ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    worker = new Worker();
    worker->updatePoints();
    updateZoom();

    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);


    glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 2 * sizeof(double), (void*)0);
    glEnableVertexAttribArray(0);
    //glBufferData(GL_ARRAY_BUFFER, sizeof(GLdouble) * nb_vectors * 2, NULL, GL_DYNAMIC_DRAW);
    ///
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLdouble) * nb_vectors * 2, worker->getVertexArray(), GL_STATIC_DRAW);

    glBindVertexArray(0);


    /* Get opengl resources from now */

    double transparency = 0.5;
    static string selected = "";
    int number_points_to_print = 0;

    /* Create file names in the combo box. */
    string file_res = exec("ls -1 ../../input/*.txt | xargs -n 1 basename");
    //cout << file_res << endl;
    std::vector<std::string> v;
    splitString(file_res, v, "\n");
    const char** items;
    const int nb_preset_dataset = 5;
    //cout << items.count() << endl;
    items = new const char*[v.size()+nb_preset_dataset];
    items[0] = "Sine wave";
    items[1] = "MNIST";
    items[2] = "Normal";
    items[3] = "CIFAR10";
    items[4] = "NORB";
    for(std::size_t i=0;i<v.size();i++){
        items[i+nb_preset_dataset] = v[i].c_str();
    }

    static int item_current = 0;

    // Settings for multi-line chart
    const char* plot_first = "First Eigenvalue";
    const char* plot_second = "Second Eigenvalue";
    const char* plot_names[2];
    plot_names[0] = plot_first;
    plot_names[1] = plot_second;
    ImColor plot_colorA = ImColor(255,0,0);
    ImColor plot_colorB = ImColor(0,255,0);
    ImColor plot_colori[2];
    plot_colori[0] = plot_colorA;
    plot_colori[1] = plot_colorB;

    const float* plot_values_couple[2];
    plot_values_couple[0] = plot_values1;
    plot_values_couple[1] = plot_values2;

    bool show_dataset_window = true;
    bool show_convergence_window = true;

    static bool cluster_reached_max;
    static bool cluster_reached_min;

    // Loop until the user closes the window
    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

   ///     bool show_demo_window = true;
   ///     if (show_demo_window)
   ///         ImGui::ShowDemoWindow(&show_demo_window);
        static bool printT = printTimeRes;

        disable_all_widgets.store(worker->getDisableWidgetsRequest());

        if (show_result_window){
            printTimeRes = false;

            //ImGui::SetNextWindowContentSize(ImVec2(420.0f, 0.0f)); ImGui::GetWindowSize()
            ImGui::SetNextWindowSizeConstraints(ImVec2(420.0f, 150.0f), ImVec2(600.0f, SCREEN_HEIGHT));
            ImGui::Begin("Results", &show_result_window,ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoSavedSettings); //ImGuiWindowFlags_AlwaysAutoResize

            if (disable_all_widgets.load())
            {
                ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
            }

            ImGui::Text("Here shows the result of execution time." );
            if (ImGui::Button("Clear all results")){
                //output_stream.str("");
                stream_count=0;
            }
            ImGui::SameLine();
            // TODO: Custom Path?
            if (ImGui::Button("Save to a CSV file")){

                if(stream_count != 0){
                    generateCSVFile();
                    output_CSVfile << "Start Timestamp;Load Time(sec);Nb vectors;Vector Length(Dimension);Platform;Nb Threads;Dataset;Algorithm\n";
                    for(int i=0;i<stream_count;i++){
                        if(i%5 == 4) output_CSVfile << "MacBook Pro Yuheng;" << 1 << ";" << items[item_current] << ";" << column_stream[i] << "\n";
                        else output_CSVfile << column_stream[i] << ";";

                    }
                    output_CSVfile.close();
                    ImGui::OpenPopup("CSV File Generated");
                }
                else{
                    ImGui::OpenPopup("Data required");
                }
            }

            if (ImGui::BeginPopupModal("CSV File Generated",NULL, ImGuiWindowFlags_NoResize))
            {
                ImGui::Text("CSV File successfully created at /output file. \nDo you want to clear the results in the list? ");
                if (ImGui::Button("YES. REMOVE ALL the results")){
                    stream_count=0;
                    ImGui::CloseCurrentPopup();
                }

                if (ImGui::Button("No, keep the results"))
                    ImGui::CloseCurrentPopup();
                ImGui::EndPopup();
            }

            if (ImGui::BeginPopupModal("Data required",NULL, ImGuiWindowFlags_NoResize))
            {
                ImGui::Text("The result list is empty. Please generate some data. ");
                if (ImGui::Button("Close"))
                    ImGui::CloseCurrentPopup();
                ImGui::EndPopup();
            }


            ImGui::Columns(5, "Execution time results");
            ImGui::SetColumnWidth(0,90);
            ImGui::SetColumnWidth(1,90);
            ImGui::SetColumnWidth(2,60);
            ImGui::SetColumnWidth(3,65);
            ImGui::SetColumnWidth(4,140);
            ImGui::Separator();
            ImGui::Text("Time series"); ImGui::NextColumn();
            ImGui::Text("Exec time"); ImGui::NextColumn();
            ImGui::Text("Nb Vec."); ImGui::NextColumn();
            ImGui::Text("Dim."); ImGui::NextColumn();
            ImGui::Text("Algorithm"); ImGui::NextColumn();
            ImGui::Columns(1);
            ImGui::Separator();


            ImGui::SetNextWindowContentWidth(460.0f);
            ImGui::BeginChild("##scrollingregion", ImVec2(0, ImGui::GetWindowSize().y - 100), false); //ImGuiWindowFlags_HorizontalScrollbar
            ImGui::Columns(5, "Execution time results");
            ImGui::SetColumnWidth(0,90);
            ImGui::SetColumnWidth(1,90);
            ImGui::SetColumnWidth(2,60);
            ImGui::SetColumnWidth(3,65);
            ImGui::SetColumnWidth(4,240);

            for (int i = 0; i < stream_count; i++)
            {
                // WARNING: time series may be the same, so it exists duplicate ID, so we may not be able to select certain lines.
                if(i%5 == 0) {
                    if (ImGui::Selectable(column_stream[i].c_str(), !selected.compare(column_stream[i]),
                                          ImGuiSelectableFlags_SpanAllColumns)) {
                        selected = column_stream[i];
                    }
                    ImGui::NextColumn();
                    continue;
                }
                ImGui::Text("%s", column_stream[i].c_str()); ImGui::NextColumn();
            }
            ImGui::Columns(1);
            ImGui::Separator();

            ImGui::EndChild();

            if (disable_all_widgets.load())
            {
                ImGui::PopItemFlag();
                ImGui::PopStyleVar();
            }

            ImGui::End();
        }
        else
            printTimeRes = printT;

        if (show_dataset_window){
            //ImGui::SetNextWindowSizeConstraints(ImVec2(420.0f, 150.0f), ImVec2(420.0f, SCREEN_HEIGHT));
            ImGui::Begin("Select dataset", &show_dataset_window,ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse); //ImGuiWindowFlags_AlwaysAutoResize
            ImGui::Text("Current dataset: %s", items[item_current]);

            if (disable_all_widgets.load())
            {
                ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
            }

            if(ImGui::Combo("", &item_current, items, v.size()+nb_preset_dataset)){
                need_load_file = true;
                current_dataset = items[item_current];
                worker->startUpdatePoints();
            }

            ImGui::SameLine();
            HelpMarker("This list includes default dataset(sine wave) and user's dataset from /input directory. The programme scans all files in /input directory and keeps all the *.txt files. ");

            if (disable_all_widgets.load())
            {
                ImGui::PopItemFlag();
                ImGui::PopStyleVar();
            }

            ImGui::End();
        }
        /*
            ImGui::Begin("Projection Result", NULL);
            ImTextureID my_tex_id = io.Fonts->TexID;
            float my_tex_w = (float)io.Fonts->TexWidth;
            float my_tex_h = (float)io.Fonts->TexHeight;

            ImGui::Text("%.0fx%.0f", my_tex_w, my_tex_h);
            //ImVec2 pos = ImGui::GetCursorScreenPos();
            ImGui::Image(my_tex_id, ImVec2(my_tex_w, my_tex_h), ImVec2(0,0), ImVec2(1,1), ImVec4(1.0f,1.0f,1.0f,1.0f), ImVec4(1.0f,1.0f,1.0f,0.5f));
            ImGui::End();
        */

        if(show_convergence_window){
            ImGui::Begin("Result - Convergence", &show_convergence_window,ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse); //ImGuiWindowFlags_AlwaysAutoResize

            ImGui::ProgressBar(progress_loading, ImVec2(0.0f,0.0f));
            ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
            ImGui::Text("Loading data");

            ImGui::ProgressBar(progress_calculating_first, ImVec2(0.0f,0.0f));
            ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
            ImGui::Text("1st Eigen");

            ImGui::ProgressBar(progress_calculating_second, ImVec2(0.0f,0.0f));
            ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
            ImGui::Text("2nd Eigen");

            //ImGui::PlotLines("Convergence", plot_values1, IM_ARRAYSIZE(plot_values1), plot_values_offset1, "", -8.0f, 1.0f, ImVec2(0,180));
            ImGui::PlotMultiLines("Convergence", 2, plot_names, plot_colori, plot_values_couple, IM_ARRAYSIZE(plot_values1), -8.0f, 1.0f, ImVec2(0,180));
            //cout << IM_ARRAYSIZE(plot_values1) << endl;

            ImGui::Text("%s",convergence_res.c_str());
            ImGui::End();
        }

        // Main window
        {
            //static float f = 0.0f;
            //static int counter = 0;
            const static double  double_min = 0.01/2,  double_max = 128*2;
            const static double  min_off = -2.0,  max_off = 2.0;
            static string info;

            //const int n_min = 1, n_max = 5000;
            // TODO: redo layout
            ImGui::Begin("Parameters setting", NULL, ImGuiWindowFlags_AlwaysAutoResize); //  ,NULL, ImGuiWindowFlags_AlwaysAutoResize //ImGuiWindowFlags_NoResize //ImGuiWindowFlags_NoSavedSettings
            ImGui::SetWindowSize(ImVec2(430,195));

            if (disable_all_widgets.load())
            {
                ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
                info = " [Calculating]";
            }

            if (ImGui::Button("Change seed"))
                worker->changeSeed();
            ImGui::SameLine();
            if (ImGui::Button("Recalculate"))
                worker->startUpdatePoints();
            ImGui::SameLine();
            if (ImGui::Button("Reset All parameters"))
                resetParam(true);
            ImGui::SameLine();
            if (ImGui::Button("Benchmarks"))
                worker->startBenchMark();
            ImGui::SameLine();
            ImGui::Text("%s",info.c_str());

            ImGui::Checkbox("Show final results", &show_result_window);      // Edit bools storing our window open/close state
            ImGui::SameLine();
            ImGui::Checkbox("Show partial results", &show_convergence_window);      // Edit bools storing our window open/close state
            ImGui::SameLine();
            ImGui::Checkbox("Select dataset", &show_dataset_window);

            //ImGui::Checkbox("Generate result in CSV", &generateCSV);
            //ImGui::SameLine();


            //ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
/*
            ImGui::SameLine();
            ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.42f);
            ImGui::ColorEdit3("Background color", (float*)&clear_color); // Edit 3 floats representing a color
*/

            ImGui::Text("Algorithm Chosen: "); ImGui::SameLine();

            ImGui::PushItemWidth(280);
            if(ImGui::Combo("", &myPCA::algo_chosen, myPCA::algorithm_names, int(myPCA::ALGO_LAST))){
                worker->startUpdatePoints();
            }
            ImGui::PopItemWidth();
/*
            if(ImGui::RadioButton("OpenCV PCA", &algo_chosen, 0))  { startUpdatePoints(); }ImGui::SameLine();
            if(ImGui::RadioButton("OpenCV Eigen", &algo_chosen, 1)) {startUpdatePoints(); } ImGui::SameLine();
            if(ImGui::RadioButton("Eigensolver", &algo_chosen, 2)) {startUpdatePoints();}
            if(ImGui::RadioButton("Power Method", &algo_chosen, 3)) {startUpdatePoints();} ImGui::SameLine();
            if(ImGui::RadioButton("Spectra", &algo_chosen, 4)) {startUpdatePoints(); } ImGui::SameLine();
            if(ImGui::RadioButton("Prog Power", &algo_chosen, 5)) {startUpdatePoints(); } ImGui::SameLine();
            if(ImGui::RadioButton("Prog Power+M", &algo_chosen, 6)) {startUpdatePoints(); }
*/
            ImGui::Columns(2);
            ImGui::SetColumnWidth(0, 150);

            // update zoom in real time
            //ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.24f);
            if(ImGui::SliderScalar("ZOOM",  ImGuiDataType_Double ,&ZOOM, &double_min, &double_max,  "%.2f", 3.3f)){
                updateZoom();
            }


            if(ImGui::SliderScalar("X Pos",  ImGuiDataType_Double ,&off_x, &min_off, &max_off,  "%.3f", 1.0f)){
                updateZoom();
            }

            if(ImGui::VSliderScalar("Y Pos",  ImVec2(40.0f, 40.0f) , ImGuiDataType_Double ,&off_y, &min_off, &max_off,  "%.2f", 1.0f)){
                updateZoom();
            }


            ImGui::NextColumn();
            ImGui::SetColumnWidth(1, 250);

            //ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.24f);
            // update other param after changing the param

            ImGui::SliderInt("Length", &vector_length, slider_n_min, slider_n_max);
            if (ImGui::IsItemDeactivatedAfterChange()){
                worker->startUpdatePoints();
            }
            if (ImGui::IsItemHovered())
                ImGui::SetTooltip("Vector length");

            ImGui::SliderInt("Nb points", &perturb_time_tmp, slider_m_min, slider_m_max);
            if (ImGui::IsItemDeactivatedAfterChange()){
                if(!current_dataset.compare("Sine wave")){
                    nb_vectors = perturb_time_tmp * cluster;
                }
                else{
                    nb_vectors = perturb_time_tmp;
                }
                worker->startUpdatePoints();
            }
            if (ImGui::IsItemHovered())
                ImGui::SetTooltip("Number of points for each cluster");

            if(!current_dataset.compare("Sine wave")) {
                ImGui::SliderInt("Frequency", &perturb_value_freq, 1, 100);
                if (ImGui::IsItemDeactivatedAfterChange()){
                    worker->startUpdatePoints();
                }
                if (ImGui::IsItemHovered())
                    ImGui::SetTooltip("Perturb value for frequency");

                ImGui::SliderInt("Offset", &perturb_value_offset, 1, 100);
                if (ImGui::IsItemDeactivatedAfterChange()){
                    worker->startUpdatePoints();
                }
                if (ImGui::IsItemHovered())
                    ImGui::SetTooltip("Perturb value for offset");
            }
            //ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            if (disable_all_widgets.load())
            {
                ImGui::PopItemFlag();
                ImGui::PopStyleVar();
                info = "";
            }

            ImGui::Columns(1);
/*            ImGui::Text("Dataset modification");
            ImGui::SameLine();

            cluster_reached_max = current_cluster >= cluster;
            cluster_reached_min = current_cluster <= 1;

            if (cluster_reached_max)
            {
                ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
            }

            if (ImGui::Button("Add a cluster")){ current_cluster++; }
            ImGui::SameLine();

            if (cluster_reached_max)
            {
                ImGui::PopItemFlag();
                ImGui::PopStyleVar();
            }

            if (cluster_reached_min)
            {
                ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
            }

            if (ImGui::Button("Remove a cluster")){ current_cluster--; }

            if (cluster_reached_min)
            {
                ImGui::PopItemFlag();
                ImGui::PopStyleVar();
            }
*/
            ImGui::Text("Progressive Computation");
            ImGui::SameLine();
            if (ImGui::Button("ABORT"))
                worker->abort_calculation();
            ImGui::SameLine();
            if (ImGui::Button("PAUSE/Continue")){
                if(workerExec){
                    worker->progressive_pause_continue();
                    //info = pause_calculation ? " [Paused]" : " [Calculating]";
                }
            }
            ImGui::End();
        }
        //cout << "%";
        int display_w, display_h;
        //glfwMakeContextCurrent(window);
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        //glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(shaderProgram);

        unsigned int transformLoc = glGetUniformLocation(shaderProgram, "transform");
        glUniformMatrix4fv(transformLoc, 1, GL_FALSE, &trans[0][0]);

        glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
        //cout << "$";
        // update the buffer if the data in buffer are updated
        if(readyToPrint.load()){
///
            glBufferData(GL_ARRAY_BUFFER, sizeof(GLdouble) * nb_vectors * 2, worker->getVertexArray(), GL_STATIC_DRAW);
            //cout << "Re-create buffer done" << endl;
            perturb_time = perturb_time_tmp;
            number_points_to_print = worker->number_points_calculated() == 0 ? nb_vectors : worker->number_points_calculated();
            readyToPrint.store(false);
        }
/// disable this when benchmark
        //glEnable(GL_POINT_SMOOTH);

        if(!current_dataset_render.compare("Sine wave")) {
            for (int count = 0; count < cluster; count++) {
                glPointSize(6);
                int vertexColorLocation = glGetUniformLocation(shaderProgram, "ourColor");
                glUniform4f(vertexColorLocation, (count + 1) % 2, (count + 1) / 2 % 2, (count + 1) / 2 / 2,
                            transparency);

                glDrawArrays(GL_POINTS, count * perturb_time + 1, perturb_time - 1);


                glPointSize(22);
                glUniform4f(vertexColorLocation, (count + 1) % 2, (count + 1) / 2 % 2, (count + 1) / 2 / 2, 1.0f);
                glDrawArrays(GL_POINTS, count * perturb_time, 1);
            }

        }
        else if(!current_dataset_render.compare("MNIST") || !current_dataset_render.compare("CIFAR10") || !current_dataset_render.compare("NORB")){
            if(number_points_to_print<1000) glPointSize(20);
            else if(number_points_to_print < 10000) glPointSize(10);
            else if(number_points_to_print < 50000) glPointSize(7);
            else if(number_points_to_print < 100000) glPointSize(4);
            else glPointSize(2);

            for(int i=0;i<perturb_time;i++){
                int vertexColorLocation = glGetUniformLocation(shaderProgram, "ourColor");
                switch(worker->getPointColor(i)){
                    case 0: glUniform4f(vertexColorLocation, 1.0f, 0.0f, 0.0f, 1.0f); break;
                    case 1: glUniform4f(vertexColorLocation, 1.0f, 0.549f, 0.0f, 1.0f); break;
                    case 2: glUniform4f(vertexColorLocation, 1.0f, 1.0f, 0.3f, 1.0f); break;
                    case 3: glUniform4f(vertexColorLocation, 0.196f, 0.804f, 0.196f, 1.0f); break;
                    case 4: glUniform4f(vertexColorLocation, 0.000f, 0.392f, 0.000f, 1.0f); break;
                    case 5: glUniform4f(vertexColorLocation, 0.125f, 0.698f, 0.667f, 1.0f); break;
                    case 6: glUniform4f(vertexColorLocation, 0.000f, 1.000f, 1.000f, 1.0f); break;
                    case 7: glUniform4f(vertexColorLocation, 0.275f, 0.510f, 0.706f, 1.0f); break;
                    case 8: glUniform4f(vertexColorLocation, 0.000f, 0.000f, 0.545f, 1.0f); break;
                    case 9: glUniform4f(vertexColorLocation, 0.482f, 0.408f, 0.933f, 1.0f); break;
                    default: glUniform4f(vertexColorLocation, 1.000f, 0.894f, 0.769f, 1.0f);//cerr << "Unknown return code in MNIST dataset: " << worker->getPointColor(i) << endl;
                }
                glDrawArrays(GL_POINTS, i, 1);
            }
        }
        else{
            if(number_points_to_print<1000)
                glPointSize(20);
            else if(number_points_to_print < 10000)
                glPointSize(10);
            else glPointSize(5);
            int vertexColorLocation = glGetUniformLocation(shaderProgram, "ourColor");
            glUniform4f(vertexColorLocation, 1.000f, 0.894f, 0.769f, 1.0f);
            glDrawArrays(GL_POINTS, 0, perturb_time);
        } ///
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(window);
    }
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    ImGui_ImplOpenGL3_Shutdown();
    ImGui::DestroyContext();

    glfwTerminate();
    worker->quit();
    workerExec = true;
    condv.notify_all();
    delete worker;
    return 0;
}
