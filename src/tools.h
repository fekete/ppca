#ifndef PPCA_TOOLS_H
#define PPCA_TOOLS_H

#include <opencv2/opencv.hpp>
#include <chrono>
//#include <opencv2/core/eigen.hpp>

using namespace std;

typedef cv::Mat Matrix;
typedef cv::Mat Vector;
using cv::norm;
using cv::sqrt;
inline int rows(const Matrix& m) { return m.rows; }
inline int cols(const Matrix& m) { return m.cols; }

void printMat( Matrix _data );
double randDouble(double min, double max);
void read_directory(const std::string& name, vector<string>& v);
std::string exec(const char* cmd);
void splitString(const std::string& s, std::vector<std::string>& v, const std::string& c);
//Matrix concat(Matrix v1, Matrix v2);
Matrix concatTwoCols(Matrix v1, Matrix v2);
Matrix concatTwoRows(Matrix v1, Matrix v2);
void generateCSVFile();
Matrix meanSubtraction(Matrix _data);
Matrix meanSubtraction(Matrix _data, Matrix& m);
Matrix covMat(Matrix _data, bool transpose);
Vector generateRandomVector(int length);
Vector generateRandomVectorWithPreviousOne(int length, Vector prev);
void HouseHolderQR(const Matrix &A, Matrix &Q, Matrix &R);
void qr_thin( Matrix &i_A, Matrix &o_Q );
Matrix myQR(Matrix &A);
Matrix myQR_complet(Matrix &A, int nbCols);
Matrix diagonalize(Matrix vec);
void sqrt(Matrix& mat);
double rayleigh(Matrix data, Vector vec);
double measureEigenvectors(Matrix mat1,Matrix mat2);
Matrix removeFirstEigenVal(Matrix A, Vector eigVec1, double eigVal);
Matrix initRandomRowVector(int length);
Matrix generateMatrixFromEigenvalue(int R,int C, Matrix sigma, Matrix &Q, Matrix &U);
double mean_absolute_unsigned_error(Matrix mat1,Matrix mat2);
vector<int> generateShuffleVector(int length);

extern std::ofstream output_CSVfile;



#endif
