#include "tools.h"
#include <Eigen/Dense>
#include <fstream>
#include <iomanip>
#include <memory>
#include <dirent.h>
#include <random>
#include "algo/pca.hpp" // to get progress_update_first/second

std::ofstream output_CSVfile;

//const char* plot_text = "0.0";
//bool progress_update_first = true;
//bool progress_update_second = true;

void printMat( Matrix _data )
{
    Matrix data = cv::Mat_<double>(_data);
    for ( int i=0; i<data.rows; i++ )
    {
        for ( int j=0; j< data.cols; j++ )
        {
            cout << std::setw(20) << data.at<double>(i,j) << std::setw(20);
        }
        cout << endl;
    }
    cout << endl << endl;
}

double randDouble(double min, double max){
    return min + static_cast <double> (rand()) /( static_cast <double> (RAND_MAX/(max-min)));
}

void read_directory(const std::string& name, vector<string>& v)
{
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);
}

std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

void splitString(const std::string& s, std::vector<std::string>& v, const std::string& c)
{
    std::string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while(std::string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2-pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if(pos1 != s.length())
        v.push_back(s.substr(pos1));
}
/*
Matrix concat(Matrix v1, Matrix v2){
    Matrix eigVec =  Matrix::zeros( 2, v1.rows, v1.type() );
    for ( int j=0; j<v1.rows; j++ )
        eigVec.at<double>(0,j) = v1.at<double>(j,0);
    for ( int j=0; j<v1.rows; j++ )
        eigVec.at<double>(1,j) = v2.at<double>(j,0);
    return eigVec;
}
*/
Matrix concatTwoCols(Matrix v1, Matrix v2){
    Matrix eigVec =  Matrix::zeros( v1.rows,v1.cols+v2.cols, v1.type() );
    for ( int j=0; j<v1.rows; j++ ) {
        for (int i = 0; i < v1.cols; i++)
            eigVec.at<double>(j,i) = v1.at<double>(j,i);
        for (int i = 0; i < v2.cols; i++)
            eigVec.at<double>(j,i+v1.cols) = v2.at<double>(j,i);
    }
    return eigVec;
}

Matrix concatTwoRows(Matrix v1, Matrix v2){
    Matrix eigVec =  Matrix::zeros( v1.rows+v2.rows,v1.cols, v1.type() );
    for ( int j=0; j<v1.cols; j++ ) {
        for (int i = 0; i < v1.rows; i++)
            eigVec.at<double>(i,j) = v1.at<double>(i,j);
        for (int i = 0; i < v2.rows; i++)
            eigVec.at<double>(i+v1.rows,j) = v2.at<double>(i,j);
    }
    return eigVec;
}

void generateCSVFile(){
    time_t now = time(0);
    tm *ltm = localtime(&now);
    char tmp[64];
    strftime(tmp, sizeof(tmp), "%Y%m%d_%H%M%S_Data",ltm );
    string filename = tmp;
    output_CSVfile.open ("../../output/"+filename+".csv");
}


// TODO: merge this
Matrix meanSubtraction(Matrix _data){

    int _R = _data.rows;
    int _C = _data.cols;

    Matrix m = Matrix::zeros( _R, 1, _data.type() );
 ///Matrix std = Matrix::zeros( _R, 1, _data.type() );

    // mean subtraction
    for ( int i=0; i<_R; i++ )
        for ( int j=0; j<_C; j++ ) {
            m.at<double>(i, 0) += _data.at<double>(i, j);
///std.at<double>(i, 0) += pow(_data.at<double>(i, j),2);
        }

    m = m/_C;
///cv::sqrt(std/_C,std);

    Matrix data_meanSub =  Matrix::zeros( _R, _C, _data.type() );
    for ( int i=0; i<_R; i++ )
        for ( int j=0; j<_C; j++ ){
            data_meanSub.at<double>(i,j) = (_data.at<double>(i,j) - m.at<double>(i,0));
///if(std.at<double>(i,0) != 0) data_meanSub.at<double>(i,j) /= std.at<double>(i,0);
        }
    return data_meanSub;
}

// col as data
Matrix meanSubtraction(Matrix _data, Matrix& m){
    int _R = _data.rows;
    int _C = _data.cols;
/*
    if(_C == 1) {
        m = _data.clone();
        return _data.clone();
    }
*/
    m = Matrix::zeros( _R, 1, _data.type() );

    // mean subtraction
    for ( int i=0; i<_R; i++ )
        for ( int j=0; j<_C; j++ )
            m.at<double>(i,0) += _data.at<double>(i,j);

    m = m/_C;

    Matrix data_meanSub =  Matrix::zeros( _R, _C, _data.type() );
    for ( int i=0; i<_R; i++ )
        for ( int j=0; j<_C; j++ )
            data_meanSub.at<double>(i,j) = _data.at<double>(i,j) - m.at<double>(i,0);

    return data_meanSub;
}

Matrix covMat(Matrix _data, bool transpose){
    int _C = _data.cols;
    if(transpose)
        return _data.t() * _data / _C;
    else
        return _data * _data.t() / _C;
}

Matrix generateRandomVector(int length){
    Matrix _vec = Matrix::zeros( length, 1, CV_64F );
    for(int i=0;i<length;i++){
        _vec.at<double>(i,0) =  randDouble(-1,1);
    }
    _vec = _vec / cv::norm(_vec);
    return _vec;
}

// insert random value between previous eigenvector
Matrix generateRandomVectorWithPreviousOne(int length, Matrix prev){
    Matrix _vec = Matrix::zeros( length, 1, CV_64F );
    for(int i=0;i<prev.rows;i++){
        _vec.at<double>(2*i,0) =  prev.at<double>(i,0);
        if(2*i+1 < length)
            _vec.at<double>(2*i+1,0) =  prev.at<double>(i,0)+1e-3;
    }
    _vec = _vec / cv::norm(_vec);
    return _vec;
}

/*
// insert random value AFTER previous eigenvector
Matrix generateRandomVectorWithPreviousOne(int length, Matrix prev){
    Matrix _vec = Matrix::zeros( length, 1, CV_64F );
    int i;
    for(i=0;i<prev.rows;i++){
        _vec.at<double>(i,0) =  prev.at<double>(i,0);
    }
    for(;i<length;i++){
        _vec.at<double>(i,0) =  randDouble(-1,1);
    }
    _vec = _vec / cv::norm(_vec);
    return _vec;
}
*/

// copy from Internet. Works almost well, but cannot deal with special case. (ex:first col = 0)
void HouseHolderQR(const Matrix &A, Matrix &Q, Matrix &R)
{
    assert ( A.channels() == 1 );
    assert ( A.rows >= A.cols );
    auto sign = [](double value) { return value >= 0 ? 1: -1; };
    const auto totalRows = A.rows;
    const auto totalCols = A.cols;
    R = A.clone();
    Q = Matrix::eye ( totalRows, totalRows, A.type() );
    for ( int col = 0; col < A.cols; ++ col )
    {
        Matrix matAROI = Matrix ( R, cv::Range ( col, totalRows ), cv::Range ( col, totalCols ) );
        Matrix y = matAROI.col ( 0 );
        auto yNorm = norm ( y );
        Matrix e1 = Matrix::eye ( y.rows, 1, A.type() );
        Matrix w = y + sign(y.at<double>(0,0)) *  yNorm * e1;
        Matrix v = w / norm( w );
        Matrix vT; cv::transpose(v, vT );
        Matrix I = Matrix::eye( matAROI.rows, matAROI.rows, A.type() );
        Matrix I_2VVT = I - 2 * v * vT;
        Matrix matH = Matrix::eye ( totalRows, totalRows, A.type() );
        Matrix matHROI = Matrix(matH, cv::Range ( col, totalRows ), cv::Range ( col, totalRows ) );
        I_2VVT.copyTo ( matHROI );
        R = matH * R;
        Q = Q * matH;
    }
}

// copy from git, use Eigen to calculate QR decomposition
void qr_thin( Matrix &i_A, Matrix &o_Q ) {
    // copy
    Eigen::MatrixXd A_E(i_A.rows, i_A.cols);
    for( int i=0; i<i_A.rows; ++i )
        for( int j=0; j<i_A.cols; ++j )
            A_E(i, j) = i_A.at<double>(j, i);
    // qr
    Eigen::HouseholderQR< Eigen::MatrixXd > qr_E(A_E);
    Eigen::MatrixXd I(Eigen::MatrixXd::Identity(A_E.rows(), A_E.cols()));
    Eigen::MatrixXd Q = qr_E.householderQ() * I;
    // return
    o_Q = Matrix(Q.rows(), Q.cols(), CV_64F);
    for( int i=0; i<Q.rows(); ++i )
        for( int j=0; j<Q.cols(); ++j )
            o_Q.at<double>(i, j) = Q(i, j);
}

// https://blog.csdn.net/joeland209/article/details/62492471
Matrix myQR(Matrix &A){
    Matrix Q1 = A.col(0); //(cv::Rect(0,0,1,A.rows));
    Matrix Q2 = A.col(1); //(cv::Rect(1,0,1,A.rows)).clone();

    Q2 = Q2 - Q1.dot(Q2)/pow(cv::norm(Q1),2) * Q1;

    Q1 = Q1 / cv::norm(Q1);
    Q2 = Q2 / cv::norm(Q2);

    Matrix Q = concatTwoCols(Q1,Q2);
    //printMat(Q); cout << endl;
    return Q;
}

Matrix myQR_complet(Matrix &A, int nbCols){
    if(nbCols < 1) exit(-1);
    if(nbCols > A.cols) nbCols = A.cols;
    Matrix Q = A(cv::Rect(0,0,nbCols,A.rows)).clone();
    if(A.cols < 2)  return A;
    for(int i=1;i<nbCols;i++){
        Matrix Q_base = Q(cv::Rect(i,0,1,A.rows));
        Matrix Q_base_origin = Q(cv::Rect(i,0,1,A.rows)).clone();

        for(int j=0;j<i;j++){
            Matrix Q_tmp = Q(cv::Rect(j,0,1,A.rows)).clone();
            Q_base = Q_base - Q_tmp.dot(Q_base_origin)/ pow(cv::norm(Q_tmp),2) * Q_tmp;
        }

    }

    for(int i=0;i<nbCols;i++){
        Matrix Q_tmp = Q(cv::Rect(i,0,1,A.rows));
        Q_tmp = Q_tmp / cv::norm(Q_tmp);
    }

    return Q;
}

Matrix diagonalize(Matrix vec){
    int r = vec.rows;
    int c = vec.cols;
    bool colVec = r>c;
    Matrix mat;
    if((r==1 && c!=1) || (r!=1 && c==1)){
        int dim = colVec?r:c;
        mat = Matrix::zeros(dim,dim,CV_64F);
        for(int i=0;i<dim;i++)
            if(colVec)  mat.at<double>(i,i) = vec.at<double>(0,i);
            else mat.at<double>(i,i) = vec.at<double>(i,0);
    }
    return mat;
}

void sqrt(Matrix& M) {
    int cols = M.cols, 
        rows = M.rows;
    if(M.isContinuous()) {
            cols *= rows;
            rows = 1;
    }
    for(int i = 0; i < rows; i++) {
        double* Mi = M.ptr<double>(i);
        for(int j = 0; j < cols; j++)
            Mi[j] = sqrt(Mi[j]);
    }
}

double rayleigh(Matrix data, Matrix vec){
    return (data * vec).dot(vec) / vec.dot(vec);
}

double measureEigenvectors(Matrix mat1,Matrix mat2){
    // TODO: check size of mat  = same
    double accuracy = 0;
    for(int i=0; i<mat1.cols; i++){
        Matrix a = mat1(cv::Rect(i,0,1,mat2.rows));
        Matrix b = mat2(cv::Rect(i,0,1,mat2.rows));
        double val = abs(1 - pow(a.dot(b),2) / pow(b.dot(b),2));
/*
        cout << " [============" << endl;
        printMat(a.t());
        printMat(b.t());
        cout << " ============]" << endl;
*/
        //double val = abs(1 - (pow(mat1(cv::Rect(i,0,1,mat2.rows)).dot(mat2(cv::Rect(i,0,1,mat2.rows))),2) / pow(mat2(cv::Rect(i,0,1,mat2.rows)).dot(mat2(cv::Rect(i,0,1,mat2.rows))),2)));
        accuracy = val > accuracy ? val : accuracy;
        //cout << " @@@@@@@@@@@@@@@@@@ VAL = " << val << endl;
    }
    return accuracy;
}

Matrix removeFirstEigenVal(Matrix A, Vector eigVec1,double eigVal){
    return A - eigVec1 * eigVal / eigVec1.dot(eigVec1) * eigVec1.t();
}

Matrix initRandomRowVector(int length){
    Matrix vec = Matrix::zeros( length, 1, CV_64F );
    for(int i=0;i<length;i++){
        vec.at<double>(i,0) =  randDouble(-1,1);
    }
    vec = vec / cv::norm(vec);
    return vec;
}

double mean_absolute_unsigned_error(Matrix mat1,Matrix mat2){
    Matrix mat3 = (mat1.cols > mat2.cols) ?
        mat1.colRange(0, mat2.cols) : mat1;
    return mean(abs(abs(mat3)-abs(mat2))).val[0];
}

Matrix generateMatrixFromEigenvalue(int R,int C, Matrix sigma, Matrix& Q, Matrix& U){

    sigma = diagonalize(sigma);
    sqrt(sigma);


    Matrix A1 = Matrix::zeros( C, R, CV_64F );
    for ( int i=0; i<R; i++ )
        for ( int j=0; j<C; j++ )
            A1.at<double>(i,j) = randDouble(-1,1);
    qr_thin(A1,Q);
    //cout << Q.size << endl;
    Matrix A2 = Matrix::zeros( R, R, CV_64F );
    for ( int i=0; i<R; i++ )
        for ( int j=0; j<R; j++ )
            A2.at<double>(i,j) = randDouble(-1,1);
    Matrix ___R1,___R2;
    //cout << A2.size << endl;
    //HouseHolderQR(A1,Q,___R1);
    qr_thin(A2,U);
    //printMat(Q); cout << endl;
    //printMat(___R1); cout << endl;
    //data = sqrt(n)*Q*diagm(sqrt(Lambda))*U'
    Matrix A_raw = sqrt(C) * Q * sigma * U;
    //cout << Q.size << endl;
    //cout << U.size << endl;
    //Matrix A_raw = sqrt(C) * Q * sigma * U.t();
    ///cout << " Mat Q :" << endl; printMat(Q); cout << endl;
    ///cout << " Mat U :" << endl;printMat(U.t()); cout << endl;

    return A_raw;
}

Matrix generateMatrixFromEigenvalue222(int R,int C, Matrix sigma, Matrix& Q, Matrix& U){

    sigma = diagonalize(sigma);
    sqrt(sigma);


    Matrix A1 = Matrix::zeros( R, C, CV_64F );
    for ( int i=0; i<R; i++ )
        for ( int j=0; j<C; j++ )
            A1.at<double>(i,j) = randDouble(-1,1);
    qr_thin(A1,Q);
    Matrix A2 = Matrix::zeros( R, R, CV_64F );
    for ( int i=0; i<R; i++ )
        for ( int j=0; j<R; j++ )
            A2.at<double>(i,j) = randDouble(-1,1);
    //Matrix ___R1,___R2;
    qr_thin(A2,U);
    //HouseHolderQR(A1,Q,___R1);
    //HouseHolderQR(A2,U,___R2);

    //data = sqrt(n)*Q*diagm(sqrt(Lambda))*U'
    Matrix A_raw = sqrt(C) *U.t() * sigma * Q;
    //cout << Q.size << endl;
    //cout << U.size << endl;
    //Matrix A_raw = sqrt(C) * Q * sigma * U.t();
    ///cout << " Mat Q :" << endl; printMat(Q); cout << endl;
    ///cout << " Mat U :" << endl;printMat(U.t()); cout << endl;

    return A_raw;
}

vector<int> generateShuffleVector(int length){
    vector<int> res(length);
    for(int i=0;i<length;i++)
        res[i] = i;

    std::shuffle(res.begin(), res.end(), default_random_engine(0));
    return res;
}
