#ifndef PPCA_PROG_BLOCKPOWERMOMENTUM_H
#define PPCA_PROG_BLOCKPOWERMOMENTUM_H

#include "progressive.hpp"

class ProgressivePowerMethodBlockMomentum : public Progressive {
public:
    ProgressivePowerMethodBlockMomentum(const std::string& name, const Matrix data, int maxIter)
      : Progressive(name,data,maxIter){_init();};
    virtual run_step_return run_step(steps_t steps) override;

protected:
    ProgressivePowerMethodBlockMomentum::run_step_return progressive_power(Matrix data, Matrix& eigVec, steps_t steps_to_do);
    void _partial_fit() override;

private:
    double _beta;
    Vector _eigVecPrevPrev;
    void _init();
    int update_momentum_count;
    Matrix _eigenbasis;

    bool update_momentum;
    double _epsilon_momentum = 1e-2;
};

#endif //PPCA_PROG_BLOCKPOWERMOMENTUM_H
