#ifndef PPCA_MINIBATCH_H
#define PPCA_MINIBATCH_H

#include "pca.hpp"

class MiniBatch : public PCA {
public:
    MiniBatch(const std::string& name, Matrix data, int maxIter, int batch_size);
    void fit() override;
protected:
    int _batch_size;

    Matrix power(Matrix data, Vector init, int epoch);
    Matrix power2(Matrix data, Vector init, int& nbIters, int maxIter, double& eigVal, bool secondEigen);
    Matrix power3(Matrix data, Vector init, int& nbIters, int maxIter, double& eigVal, bool secondEigen);
    Matrix generateMiniBatch(Matrix data, bool secondEigen);
};

#endif //PPCA_MINIBATCH_H
