#include "pca.hpp"

PCA::PCA(const std::string& name, Matrix data, int max_iter)
  : _name(name), _data(data), _max_iter(max_iter), _epsilon(1e-7)
{
    _transpose = _data.rows > _data.cols;
    _eigenvectors1 =  initRandomRowVector(min(_data.rows,_data.cols));
    _eigenvectors2 =  initRandomRowVector(min(_data.rows,_data.cols));
    _eigenvalues[0] = _eigenvalues[1] = 0.0f;
    _time1 = _time2 = 0;
    _nb_points_using = _data.cols;
}  

PCA::~PCA(){
    _data.release();
    _data_meanSub.release();
    _data_covar.release();
}

bool PCA::progress_update_first;
bool PCA::progress_update_second;
void (* PCA::updatePlotCb)(double, double) = nullptr;
void (* PCA::partialResultsCb)(double, int, int, double, double) = nullptr;

void PCA::updatePlot(double current_result, double epsilon)
{
  if (updatePlotCb)
    updatePlotCb(current_result, epsilon);
}

void PCA::partialResults(double epsilon, int nbIters1, int nbIters2, 
                         double time1, double time2) 
{
  if (partialResultsCb)
    partialResultsCb(epsilon, nbIters1, nbIters2, time1, time2);
}

Matrix PCA::transform(){
    Matrix result;
    _sign_flip();
    if(_transpose)
        result = _components.t() * _data_meanSub.t() * _data_meanSub;
    else
        result = _components.t() * _data_meanSub;
    return result;
}

Matrix PCA::fit_transform(){
    fit();
    return transform();
}

Matrix PCA::inv_project(Matrix _result){
    Matrix inv_result;

    if(_transpose)
        inv_result = (_components.t() * _data_meanSub.t()).t() * _result;
    else
        inv_result = _components * _result;
    return inv_result;
}


void PCA::_sign_flip() {
    for(int i=0;i<_components.cols;i++){
        double min, max;
        Vector v = _components(cv::Rect(i,0,1,_components.rows));
        cv::minMaxLoc(v, &min, &max);
        if(abs(min) >= abs(max) && min < 0)
            v *= -1;
    }
     /*
    double min, max;
    cv::minMaxLoc(_eigenbasis, &min, &max);
    if(abs(min) >= abs(max) && min < 0)
        _eigenbasis *= -1;
        */
}