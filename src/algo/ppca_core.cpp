#include "ppca_core.h"

ProgressiveState::ProgressiveState(){
    _start_progressive = false;
    _abort_calculation = false;
    _pause_calculation = false;
}

ProgressiveState* ProgressiveStateSingleton::_state = 0;