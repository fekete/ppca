#include "prog_lanczos.h"
#include "lanczos_aux/Arnoldi.h"
#include "lanczos_aux/ArnoldiOp.h"
#include "lanczos_aux/Lanczos.h"
//#include "../lanczos/SymEigsSolver.h"

//(doesn't work)

ProgressiveLanczos::run_step_return
ProgressiveLanczos::calculation_eigen(Matrix data, steps_t steps_to_do)
{
    run_step_return ret(state_ready, steps_to_do);
    // Restarting

    //TODO: Adding a loop for i iters and return the result after i iters -> doesn't work.
    // Needs to go inside the code and find how SymEigsBase works.
    while(_i < _max_iter)
    {
        nconv = eigs->num_converged(_epsilon);
        if(nconv >= m_nev){
            ret.first = state_terminated;
            break;
        }


        _nev_adj = eigs->nev_adjusted(nconv);
        eigs->restart(_nev_adj);

        _i++;
        if(_i%(int)steps_to_do == 0)
            break;
    }
    // Sorting results
    eigs->sort_ritzpair(_sort_rule);

    eigs->m_niter += _i + 1;
    //eigs->m_info = (nconv >= m_nev) ? SUCCESSFUL : NOT_CONVERGING;

    //if(eigs->info() == SUCCESSFUL) {
        evalues = eigs->eigenvalues();
        evecs = eigs->eigenvectors().transpose();
    //}

    //cout << evalues.size() << endl;
    //cout << evecs.size() << endl;

    for(int y=0;y<_R;y++){

        _eigenvectors1.at<double>(0,y) = evecs(0,y);
        _eigenvectors2.at<double>(0,y) = evecs(1,y);
    }
    _eigenvalues[0] = evalues(0);
    _eigenvalues[1] = evalues(1);


    return ret;
}

ProgressiveLanczos::run_step_return
ProgressiveLanczos::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second); // already done
    run_step_return ret = calculation_eigen(_data_covar, steps);
    _state1.second +=  steps;
//cout << _state1.second << endl;
    return ret;
}

void ProgressiveLanczos::_init(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);

    _R = _data_covar.rows;
    _C = _data_covar.cols;
    covM = new Eigen::MatrixXd(_R,_R);
    op = new DenseSymMatProd<double>(*covM);
    eigs = new SymEigsBase<double, LARGEST_MAGN, DenseSymMatProd<double>, IdentityBOp>(op, NULL, m_nev, m_ncv);


    for(int x=0;x<_R;x++)
        for(int y=0;y<_C;y++)
            (*covM)(x,y) = _data_covar.at<double>(x,y);
    eigs->init();


    //int nconv = eigs.compute();

    // The m-step Lanczos factorization
    eigs->m_fac.factorize_from(1, m_ncv, eigs->m_nmatop);
    eigs->retrieve_ritzpair();

    _i = 0;
}