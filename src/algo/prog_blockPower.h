#ifndef PPCA_PROG_BLOCKPOWER_H
#define PPCA_PROG_BLOCKPOWER_H

#include "progressive.hpp"

class ProgressivePowerMethodBlock : public Progressive {
public:
    ProgressivePowerMethodBlock(const std::string& name, const Matrix data, int maxIter):
            Progressive(name,data,maxIter){_init();};
    virtual run_step_return run_step(steps_t steps);

protected:
    ProgressivePowerMethodBlock::run_step_return progressive_power(Matrix data, Matrix &eigVec, steps_t steps_to_do);

private:
    void _init();
};

#endif //PPCA_PROG_BLOCKPOWER_H
