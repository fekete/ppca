#ifndef PPCA_PROG_POWERMETHOD_H
#define PPCA_PROG_POWERMETHOD_H

#include "progressive.hpp"

class ProgressivePowerMethod : public Progressive {
public:
    ProgressivePowerMethod(const std::string& name, const Matrix data, int maxIter):
        Progressive(name,data,maxIter){_init();};
    virtual run_step_return run_step(steps_t steps);

protected:
    run_step_return progressive_power(Matrix data, Vector eigVec, double& eigVal, steps_t steps_to_do);

private:
    Matrix _data2;
    void _init();
};

#endif //PPCA_PROG_POWERMETHOD_H
