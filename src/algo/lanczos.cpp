#include "lanczos.h"

MyLanczos::MyLanczos(const std::string& name, Matrix data, int _max_iter)
  : PCA(name, data, _max_iter)
{}

void MyLanczos::_lanczos(Matrix data, Vector init, int max_iter, Matrix& V, Matrix& T){
    int n = rows(init);
    //m = 2;
    if (max_iter > n)
        max_iter = n;
    V = Matrix::zeros(max_iter,n,CV_64F);
    T = Matrix::zeros(max_iter,max_iter,CV_64F);
    Vector v = Vector::zeros(n,1,CV_64F);

    double beta = 0;
    init = init / norm(init);
    Vector w0 = data*init;
    double alpha = w0.dot(init);
    Vector w = w0 - alpha * init;

    T.at<double>(0,0) = alpha;
    for(int x=0;x<n;x++)
        V.at<double>(0,x) = init.at<double>(x,0);

    for(int j=1;j<max_iter;j++){

        beta = sqrt( w.dot(w) );
        //beta = cv::norm(w);

        if(beta!=0)
            v = w/beta;
        else{
            for(int i=0;i<n;i++)
                v.at<double>(i,0) =  randDouble(-1,1);
            v = v / norm(v);
        }

        for(int x=0;x<n;x++)
            V.at<double>(j,x) = v.at<double>(x,0);

        w0 = data*v;
        alpha = w0.dot(v);
        w = w0 - alpha * v - beta * init;

        init = v.clone();

        T.at<double>(j,j) = alpha;
        T.at<double>(j,j-1) = beta;
        T.at<double>(j-1,j) = beta;
    }
}

void MyLanczos::fit(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);

    Vector w0 = Vector::zeros(_data_covar.rows,1,CV_64F);

    for(int i=0;i<_data_covar.rows;i++)
        w0.at<double>(i,0) =  randDouble(-1,1);

    Matrix V,T;
    _lanczos(_data_covar, w0, _max_iter,V,T); //_data.rows

    Matrix eigenvalues;
    Matrix eigenvectors;

    eigen(T, eigenvalues, eigenvectors);

    eigenvectors = eigenvectors * V;
    eigenvectors = eigenvectors.t();


    for(int i=0;i<_data_covar.rows;i++){
        _eigenvectors1.at<double>(i,0) = eigenvectors.at<double>(i,0);
        _eigenvectors2.at<double>(i,0) = eigenvectors.at<double>(i,1);
    }

    _eigenvalues[0] = eigenvalues.at<double>(0,0);
    _eigenvalues[1] = eigenvalues.at<double>(1,0);

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);
}
