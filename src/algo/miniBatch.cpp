#include "miniBatch.h"

MiniBatch::MiniBatch(const std::string& name, Matrix data, int maxIter, int batch_size)
  : PCA(name, data, maxIter), _batch_size(batch_size>0 ? batch_size:1)
{
    _max_iter = 500;
    _eigenvectors1 =  initRandomRowVector(_data.rows);
    _eigenvectors2 =  initRandomRowVector(_data.rows);
}

Matrix MiniBatch::generateMiniBatch(Matrix data, bool secondEigen){
    Matrix miniBatch = Matrix::zeros(data.rows,_batch_size,data.type());
    for(int x=0;x<_batch_size;x++){
        int selected = rand() % data.cols;
        //cout << selected << " ";
        for(int y=0;y<data.rows;y++)
            miniBatch.at<double>(y,x) = data.at<double>(y,selected);
    }

    //cout << endl;
    //miniBatch = meanSubtraction(miniBatch);
    Matrix _data_covar = miniBatch * miniBatch.t() / cols(miniBatch);
     //OJA Algo
     //Matrix _data_covar = miniBatch * miniBatch.t() * 5e-04;

    if(secondEigen)
        _data_covar = _data_covar - _eigenvectors1 * _eigenvalues[0] / _eigenvectors1.dot(_eigenvectors1) * _eigenvectors1.t();

    return _data_covar;
}

// iterate "epoch" times
Matrix MiniBatch::power(Matrix data, Vector init, int epoch){
    Matrix x = init / norm(init);
    for(int i=0;i<epoch;i++){
        x = data * x;
        x = x / norm(x);
    }
    return x;
}

// stops if the angle of two eigenvalues is small enough
Matrix MiniBatch::power2(Matrix data, Vector init, int& nbIters, int maxIter, double& eigVal, bool secondEigen){
    Vector x = init / norm(init);
    double delta;
    for(int i=0;i<maxIter;i++,nbIters++){
        double eigValPre = eigVal;
        Matrix x_batch = generateMiniBatch(data, secondEigen);
        //x = x + x_batch * x;
        x = x_batch * x;
        x = x / norm(x);
        eigVal = (x_batch * x).dot(x) / x.dot(x);

        delta = eigVal - eigValPre;
        //cout << delta << " ";
        //_epsilon = 1e-15;
        if(abs(delta) < _epsilon){
            break;
        }
        updatePlot(delta,_epsilon);
    } //cout << endl;
    return x;
}

// stops if the angle of two eigenvectors is small enough
Matrix MiniBatch::power3(Matrix data, Vector init, int& nbIters, int maxIter, double& eigVal, bool secondEigen){
    Vector x = init / norm(init);
    double delta;
    for(int i=0;i<maxIter;i++,nbIters++) {
        Vector xPre = x.clone();
        Vector x_batch = generateMiniBatch(data, secondEigen);
        x = x_batch * x;
        x = x / norm(x);

        delta = 1 - pow(xPre.dot(x),2) / pow(x.dot(x),2);
        //cout << delta << " ";
        //_epsilon = 1e-10;

        if(abs(delta) < _epsilon){
            eigVal = (x_batch * x).dot(x) / x.dot(x);
            break;
        }

    }
    //cout << endl;
    return x;
}


// https://github.com/HazyResearch/Accelerated-PCA/blob/master/Eigensolvers.jl
void MiniBatch::fit(){
     _data_meanSub = meanSubtraction(_data);
     _transpose = false;

    int nbIters1 = 0, nbIters2 = 0;
    double time1,time2;
    progress_update_first = true;
    auto t1 = std::chrono::high_resolution_clock::now();
    _eigenvectors1 = power2(_data_meanSub, _eigenvectors1 , nbIters1, _max_iter, _eigenvalues[0], false);
    auto t2 = std::chrono::high_resolution_clock::now();
    progress_update_first = false;
    _eigenvectors2 = power2(_data_meanSub, _eigenvectors2 , nbIters2, _max_iter, _eigenvalues[1], true);
    auto t3 = std::chrono::high_resolution_clock::now();

    time1 = nbIters1 == 0 ? 0 : std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0 / nbIters1;
    time2 = nbIters2 == 0 ? 0 : std::chrono::duration_cast<std::chrono::milliseconds>(t3-t2).count()/1000.0 / nbIters2;
    partialResults(_epsilon, nbIters1,nbIters2, time1, time2);

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);
}
