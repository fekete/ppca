#ifndef PPCA_PROG_POWERMOMENTUM_H
#define PPCA_PROG_POWERMOMENTUM_H

#include "progressive.hpp"

class ProgressivePowerMomentum : public Progressive {
public:
    ProgressivePowerMomentum(const std::string& name, const Matrix data, int maxIter):
            Progressive(name,data,maxIter){_init();};
    virtual run_step_return run_step(steps_t steps);

protected:
    run_step_return progressive_power_M(Matrix data, Vector& eigVec, Vector& eigVecPrev, double& eigVal, double& beta, steps_t steps);
    run_step_return progressive_power_M2(Matrix data, Vector& eigVec, Vector& eigVecPrev, double& eigVal, double& beta, steps_t steps);
    run_step_return progressive_power_M3(Matrix data, Vector& eigVec, Vector& eigVecPrev, double& eigVal, double& beta, steps_t steps);
    Vector _eigenvectors1Prev;
    Vector _eigenvectors2Prev;
    //double _beta1;
    double _beta2;
    double _beta3;
    Vector _eigenvectors3;
    double _eigenvalue3;

private:
    double power(Matrix data, Vector& eigVec, Vector& eigVecPrev, double beta, steps_t steps_to_do, bool& finished);
    double _beta_range[5] = {2/3, 0.99, 1, 1.01, 1.5};
    Matrix _data2;
    Matrix _data3;
    void _init();
};

#endif //PPCA_PROG_POWERMETHOD_H
