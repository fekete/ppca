#ifndef PPCA_COVARIANCEFREE_H
#define PPCA_COVARIANCEFREE_H

#include "pca.hpp"

class PowerMethodCovarianceFree : public PCA {
public:
    PowerMethodCovarianceFree(const std::string& name, Matrix data, int maxIter);
    void fit() override;

protected:
    Matrix power(Matrix data, Matrix init, int& nbIters, int maxIter, double& _eigenValue);
};


#endif //PPCA_COVARIANCEFREE_H
