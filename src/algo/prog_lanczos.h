#ifndef PPCA_PROG_LANCZOS_H
#define PPCA_PROG_LANCZOS_H

#include "progressive.hpp"
#include <Eigen/Dense>
#include "lanczos_aux/SymEigsBase.h"
#include "lanczos_aux/DenseSymMatProd.h"

class ProgressiveLanczos : public Progressive {
public:
    ProgressiveLanczos(const std::string& name, const Matrix data, int maxIter):
            Progressive(name,data,maxIter){_init();};
    virtual run_step_return run_step(steps_t steps);

protected:
    run_step_return calculation_eigen(Matrix data, steps_t steps_to_do);

private:
    int _R;
    int _C;

    Eigen::MatrixXd *covM;
    int _sort_rule = LARGEST_ALGE;
    int nconv = 0;
    Eigen::VectorXd evalues;
    Eigen::MatrixXd evecs;

    DenseSymMatProd<double> *op;
    const int    m_nev = 2;        // number of eigenvalues requested
    const int    m_ncv = 6;        // nb_vectors of Krylov subspace in the Lanczos method
    SymEigsBase<double, LARGEST_MAGN, DenseSymMatProd<double>, IdentityBOp> *eigs;

    int _i, _nev_adj;

    void _init();
};

#endif //PPCA_PROG_LANCZOS_H
