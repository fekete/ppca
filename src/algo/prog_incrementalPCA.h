#ifndef PPCA_PROG_INCREMENTALPCA_H
#define PPCA_PROG_INCREMENTALPCA_H

#include "progressive.hpp"

class ProgressiveIncrementalPCA : public Progressive {
public:
    ProgressiveIncrementalPCA(const std::string& name, const Matrix data, int maxIter, int batch_size):
            Progressive(name,data,maxIter), _batch_size(batch_size>0 ? batch_size:1){_init();};
    virtual run_step_return run_step(steps_t steps) override;
    void adjust_step(double exec_time) override;
    int current_index(){return _index;}
    Matrix transform() override;

protected:
    void calculation_eigen(Matrix data);
    void _partial_fit() override;
    void sklm(Matrix data, Matrix& U0, Matrix& D0, Matrix& mu0, int& n0, double FF, int K);

private:
    Matrix U0, D0, mu0;
    int n0 = 0;
    double FF =1;
    double K = 2;
    Matrix _data_batch;
    int _index = 0;
    int _batch_size;
    Matrix _result;
    void _init();
};

#endif //PPCA_PROG_INCREMENTALPCA_H
