#ifndef PPCA_LANCZOS_H
#define PPCA_LANCZOS_H

#include "pca.hpp"

class MyLanczos : public PCA {
public:
    MyLanczos(const std::string& name, Matrix data, int max_iter);
    void fit() override;

protected:

    void _lanczos(Matrix data, Vector init, int max_iter, Matrix& V, Matrix& T);
};

#endif //PPCA_LANCZOS_H
