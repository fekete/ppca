#ifndef PPCA_PROG_RANDOMIZEDSVD_H
#define PPCA_PROG_RANDOMIZEDSVD_H

#include "progressive.hpp"

class ProgressiveRandomizedSVD : public Progressive {
public:
    ProgressiveRandomizedSVD(const std::string& name, const Matrix data, int maxIter):
            Progressive(name,data,maxIter){_init();};
    virtual run_step_return run_step(steps_t steps) override;
    //Matrix calculate() override;
    void _partial_fit() override;
 ///   Matrix transform() override;
    ////void adjust_step(double exec_time) override;
    ///double calculate_error(Matrix result) override;

protected:
    Matrix block_power(Matrix data, Matrix eigBasis);
    Matrix inv_project(Matrix _result) override;

private:
    void _init();
    Matrix _aux_matrix;
    Matrix _reduced_data;
    int _aux_matrix_size;
    Matrix _result;
    Matrix _result_prev;
    Matrix AQO;
    Matrix _components_prev;
    Matrix _components_tmp;
    Matrix AQ;
    Matrix ATQ;
};

#endif //PPCA_PROG_RANDOMIZEDSVD_H
