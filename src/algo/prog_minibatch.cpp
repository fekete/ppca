#include "prog_minibatch.h"

Matrix ProgressiveMiniBatch::generateMiniBatch(Matrix data){
    _data_meanSub = Matrix::zeros(_transpose?cols(data):rows(data),_batch_size, data.type());

    for(int x=0;x<_batch_size;x++){
        int selected = rand() % min(data.rows,data.cols);
        for(int y=0;y<_data_meanSub.rows;y++){
            if(_transpose)
                _data_meanSub.at<double>(y,x) = data.at<double>(selected,y);
            else
                _data_meanSub.at<double>(y,x) = data.at<double>(y,selected);
        }
    }
    //miniBatch = meanSubtraction(miniBatch);
    if(!_transpose)
        _data_covar = _data_meanSub * _data_meanSub.t() / cols(_data_meanSub);
    else
        _data_covar = _data_meanSub.t() * _data_meanSub / cols(_data_meanSub);
    //Matrix _data_covar = miniBatch * miniBatch.t() * 5e-4;

    cout << " *** " << _data_meanSub.size << " = " << _data_covar.size << endl;

    return _data_covar;
}

ProgressiveMiniBatch::run_step_return
ProgressiveMiniBatch::progressive_power_minibatch(Matrix data, Vector& eigVec, double& eigVal, steps_t steps_to_do, bool secondEigen)
{
    //Matrix eigVecPrev;
    double eigValPrev;
    Matrix miniBatch_covar = generateMiniBatch(data);
    double delta = 0;
    if(secondEigen)
        miniBatch_covar = miniBatch_covar - _eigenvectors1 * _eigenvalues[0] / _eigenvectors1.dot(_eigenvectors1) * _eigenvectors1.t();

    for(int i=0;i<steps_to_do;i++){
        //eigVecPrev = eigVec.clone();
        eigValPrev = eigVal;
        cout << miniBatch_covar.size << " = " << eigVec.size << endl;
        eigVec = miniBatch_covar * eigVec;
        //eigVec = eigVec + miniBatch_covar * eigVec;
        eigVec = eigVec / norm(eigVec);

        eigVal =  (miniBatch_covar * eigVec).dot(eigVec) / eigVec.dot(eigVec);

        //delta = 1 - pow(eigVecPrev.dot(eigVec),2) / pow(eigVec.dot(eigVec),2);
        delta = eigVal - eigValPrev;
        //cout << delta << " ";
        if(abs(delta) < _epsilon){ //cout << endl;
            updatePlot(delta,_epsilon);
            return run_step_return(state_terminated, i);
        }
    }
    updatePlot(delta,_epsilon);
    return run_step_return(state_ready, steps_to_do);
}

ProgressiveMiniBatch::run_step_return
ProgressiveMiniBatch::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second + _state2.second); // already done

    if(_state1.first != state_terminated){
        progress_update_first = true;
        progress_update_second = false;
        auto t1 = std::chrono::high_resolution_clock::now();
        run_step_return ret1 = progressive_power_minibatch(_origin_dataMeanSub, _eigenvectors1, _eigenvalues[0], steps/2, false);
        auto t2 = std::chrono::high_resolution_clock::now();
        _time1 += std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;
        _state1.first = ret1.first;
        _state1.second += ret1.second;
    }

    //if(_state2.first != state_terminated) {
        progress_update_first = false;
        progress_update_second = true;
        auto t3 = std::chrono::high_resolution_clock::now();
        run_step_return ret2 = progressive_power_minibatch(_origin_dataMeanSub, _eigenvectors2, _eigenvalues[1], steps/2, true);
        auto t4 = std::chrono::high_resolution_clock::now();
        _time2 += std::chrono::duration_cast<std::chrono::milliseconds>(t4-t3).count()/1000.0;
        _state2.first = ret2.first;
        _state2.second += ret2.second;
    //}

    if (_state1.first == state_terminated && _state2.first == state_terminated){
        _state = state_terminated;
    }
    else
        _state = state_ready;

    double time1_average = _state1.second == 0 ? 0 : _time1 / _state1.second;
    double time2_average = _state2.second == 0 ? 0 : _time2 / _state2.second;
    partialResults(_epsilon, _state1.second,_state2.second, time1_average, time2_average);
    return run_step_return(_state, _state1.second + _state2.second);
}

void ProgressiveMiniBatch::_init(){
    _epsilon = 1e-6;
    _max_iter = 1000;
    //_transpose = false;
    _origin_dataMeanSub = meanSubtraction(_data);
    _transpose_batch = _batch_size < _data_meanSub.rows;

    if(_transpose){
        _eigenvectors1 =  initRandomRowVector(_batch_size);
        _eigenvectors2 =  initRandomRowVector(_batch_size);
    }

}

void ProgressiveMiniBatch::_partial_fit(){
    Progressive::run_step_return ret = run_step(_step_per_iteration);
    if(ret.first == Progressive::state_terminated){
        _progressiveStateSingleton->getState()->_start_progressive = false;
    }

    if(ret.second >= maxStep()){
        _progressiveStateSingleton->getState()->_abort_calculation = true;
    }

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);

}