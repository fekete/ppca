#include "powerMethod.h"

PowerMethod::PowerMethod(const std::string& name, Matrix data, int maxIter)
  : PCA(name, data, maxIter)
{}

// iterate "epoch" times
Matrix PowerMethod::power(Matrix data, Vector init, int epoch){
    Vector x = init / norm(init);
    for(int i=0;i<epoch;i++){
        x = data * x;
        x /= norm(x);
    }
    return x;
}

// stops if the difference of two eigenvalues is smaller than epsilon
// needs high computational cost , use power3 instead.
Matrix PowerMethod::power2(Matrix data, Vector init, int& nbIters, int maxIter){
    Vector x = init / norm(init);
    double eigVal = 0, eigValPre;
    int count=0;
    for(int i=0;i<maxIter;i++,nbIters++){
        eigValPre = eigVal;

        x = data * x;
        x = x / norm(x);

        eigVal = (data * x).dot(x) / x.dot(x);
        count++;
        if(eigVal - eigValPre < _epsilon)
            break;
    }
    //cout << "Nb iter: " << count << endl;
    return x;
}

// stops if the angle of two eigenvectors is small enough
Matrix PowerMethod::power3(Matrix data, Vector init, int& nbIters, int maxIter){
    Vector x = init / norm(init);
    double delta;
    for(int i=0;i<maxIter;i++,nbIters++){
        Vector xPre = x.clone();

        x = data * x;
        x = x / norm(x);

        delta = measureEigenvectors(xPre,x);
        if(abs(delta) < _epsilon){
            updatePlot(delta,_epsilon);
            break;
        }
        updatePlot(delta,_epsilon);
    }

    return x;
}

void PowerMethod::fit(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);

    int nbIters1 = 0, nbIters2 = 0;
    double time1,time2;
    progress_update_first = true;
    progress_update_second = false;
    auto t1 = std::chrono::high_resolution_clock::now();
    _eigenvectors1 = power3(_data_covar, _eigenvectors1 , nbIters1, _max_iter);
    auto t2 = std::chrono::high_resolution_clock::now();

    _eigenvalues[0] =  (_data_covar * _eigenvectors1).dot(_eigenvectors1) / _eigenvectors1.dot(_eigenvectors1);


    Matrix B = _data_covar - _eigenvectors1 * _eigenvalues[0] / _eigenvectors1.dot(_eigenvectors1) * _eigenvectors1.t();
    progress_update_first = false;
    progress_update_second = true;
    auto t3 = std::chrono::high_resolution_clock::now();
    _eigenvectors2 = power3(B, _eigenvectors2 , nbIters2, _max_iter);
    auto t4 = std::chrono::high_resolution_clock::now();

    _eigenvalues[1] =  (B * _eigenvectors2).dot(_eigenvectors2) / _eigenvectors2.dot(_eigenvectors2);


    time1 = nbIters1 == 0 ? 0 : std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0 / nbIters1;
    time2 = nbIters2 == 0 ? 0 : std::chrono::duration_cast<std::chrono::milliseconds>(t4-t3).count()/1000.0 / nbIters2;
    partialResults(_epsilon, nbIters1,nbIters2, time1, time2);
    //cout << _eigenvalues[0] << " = " <<  _eigenvalues[1] << endl;

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);
}