#include "prog_haar_wavelet.h"

// stops if the angle of two eigenvectors is small enough
Vector ProgressiveHaarWavelet::power(Matrix data, Vector init, int& nbIters ,int maxIter){
    //Matrix x = generateRandomVector(data.rows);
    Vector x = generateRandomVectorWithPreviousOne(rows(data), init);
    double delta;
    nbIters=0;
    for(int i=0;i<maxIter;i++,nbIters++){
        Vector xPre = x.clone(); // was x.clone();
        x = data * x;
        x = x / norm(x);
        delta = 1 - pow(xPre.dot(x),2) / pow(x.dot(x),2);
        if(abs(delta) < _epsilon){
            updatePlot(delta,_epsilon);
            break;
        }
        updatePlot(delta,_epsilon);
    }
    return x;
}

void ProgressiveHaarWavelet::calculation_eigen(steps_t steps_to_do)
{
    int nbIters1 = 0, nbIters2 = 0;
    double time1,time2;
    _haarWavelet -> decompress();
    Matrix _data = _haarWavelet->getCompressedData();
    _data_meanSub = meanSubtraction(_data);
    //_transpose = _data.rows > _data.cols;
    _data_covar = covMat(_data_meanSub, false);

    progress_update_first = true;
    progress_update_second = false;
    auto t1 = std::chrono::high_resolution_clock::now();
    _eigenvectors1 = power(_data_covar,_eigenvectors1,nbIters1,1000);
    auto t2 = std::chrono::high_resolution_clock::now();

    _eigenvalues[0] =  (_data_covar * _eigenvectors1).dot(_eigenvectors1) / _eigenvectors1.dot(_eigenvectors1);
    Matrix B = _data_covar - _eigenvectors1 * _eigenvalues[0] / _eigenvectors1.dot(_eigenvectors1) * _eigenvectors1.t();

    progress_update_first = false;
    progress_update_second = true;

    auto t3 = std::chrono::high_resolution_clock::now();
    _eigenvectors2 = power(B, _eigenvectors2 , nbIters2, 1000);
    auto t4 = std::chrono::high_resolution_clock::now();

    _eigenvalues[1] =  (B * _eigenvectors2).dot(_eigenvectors2) / _eigenvectors2.dot(_eigenvectors2);

    time1 = nbIters1 == 0 ? 0 : std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0 / nbIters1;
    time2 = nbIters2 == 0 ? 0 : std::chrono::duration_cast<std::chrono::milliseconds>(t4-t3).count()/1000.0 / nbIters2;
    partialResults(_epsilon, nbIters1,nbIters2, time1, time2);
}

ProgressiveHaarWavelet::run_step_return
ProgressiveHaarWavelet::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second); // already done
    //cout << " >>>> " << _haarWavelet->getResolution() << endl;
    calculation_eigen(steps);

    if (_haarWavelet->getResolution() == 1)
        _state = state_terminated;
    _state1.second++;
    return run_step_return(_state, _state1.second);
}

void ProgressiveHaarWavelet::_init(){
    _transpose = false;
    _haarWavelet = new HaarWavelet(_data);
    _haarWavelet -> init();
    _eigenvectors1 = cv::Mat::zeros( 2, 1, _data.type() );
    _eigenvectors2 = cv::Mat::zeros( 2, 1, _data.type() );
}

void ProgressiveHaarWavelet::fit(){
    while(true){
        Progressive::run_step_return ret = run_step(1);
        if(ret.first == Progressive::state_terminated){
            _progressiveStateSingleton->getState()->_start_progressive = false;
            break;
        }

        if(ret.second >= maxStep()){
            _progressiveStateSingleton->getState()->_abort_calculation = true;
            break;
        }
    }

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);
}

void ProgressiveHaarWavelet::_partial_fit(){
    Progressive::run_step_return ret = run_step(1);
    if(ret.first == Progressive::state_terminated){
        _progressiveStateSingleton->getState()->_start_progressive = false;
    }

    if(ret.second >= maxStep()){
        _progressiveStateSingleton->getState()->_abort_calculation = true;
    }

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);
}