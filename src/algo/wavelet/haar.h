#ifndef PPCA_HAAR_H
#define PPCA_HAAR_H

#include <opencv2/opencv.hpp>

using namespace std;

class HaarWavelet{
public:
    HaarWavelet(cv::Mat data);
    void compress();
    void decompress();
    // a data matrix merge with detail coefficients
    cv::Mat getData(){return _data;}
    // pure data matrix
    cv::Mat getCompressedData();
    int getResolution(){return _resolution;}
    void init();

protected:
    cv::Mat _data;
    int _resolution;

private:
    int _R;
    int _C;
};

#endif //PPCA_HAAR_H
