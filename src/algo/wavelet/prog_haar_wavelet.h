#ifndef PPCA_PROG_HAAR_WAVELET_H
#define PPCA_PROG_HAAR_WAVELET_H

#include "haar.h"
#include "../progressive.hpp"

class ProgressiveHaarWavelet : public Progressive {
public:
    ProgressiveHaarWavelet(const std::string& name, const Matrix data, int maxIter):
            Progressive(name,data,maxIter){_init();};
    virtual run_step_return run_step(steps_t steps) override;
    void fit() override;

protected:
    void calculation_eigen(steps_t steps_to_do);
    Vector power(Matrix data, Vector init, int& nbIters ,int maxIter);
    void _partial_fit() override;

private:
    HaarWavelet *_haarWavelet;
    void _init();
};


#endif //PPCA_PROG_HAAR_WAVELET_H
