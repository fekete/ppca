#include "haar.h"

HaarWavelet::HaarWavelet(cv::Mat data){
    this->_data = data.clone();
    _resolution = 1;
    _R = _data.rows;
    _C = _data.cols;
}

void HaarWavelet::compress(){
    int data_length = _R;
    for(int i=0;i<log2(_resolution);i++){
        data_length = (data_length+1) / 2;
        if(data_length <= 2) return;
    }

    for(int j=0; j<_C; j++){
        cv::Mat p_tmp = cv::Mat::zeros(data_length,1,CV_64F);
        for(int i=0; 2*i+1<data_length; i++){
            double x =  _data.at<double>(2*i,j);
            double y =  _data.at<double>(2*i+1,j);
            p_tmp.at<double>(i,0) = (x+y)/2;
            p_tmp.at<double>((data_length+1)/2+i,0) = (x-y)/2;
            //cout << (R+1+i)/2 << endl;
        }
        if(data_length % 2){
            p_tmp.at<double>(data_length/2,0) = _data.at<double>(data_length-1,j);
        }
        for(int i=0; i<data_length; i++){
            _data.at<double>(i,j) = p_tmp.at<double>(i,0);
        }

    }
    _resolution *= 2;
}

void HaarWavelet::decompress(){
    int data_length = _R;
    int data_length_prev = 0;
    if(_resolution <= 1) return;
    for(int i=0;i<log2(_resolution);i++){
        data_length_prev = data_length;
        data_length = (data_length+1) / 2;
    }
    //cout << data_length << " = " << data_length_prev << endl;
    for(int j=0; j<_C; j++){
        cv::Mat p_tmp = cv::Mat::zeros(data_length_prev,1,CV_64F);
        for(int i=0; 2*i+1<data_length_prev; i++){
            double x = _data.at<double>(i,j);
            double y = _data.at<double>(data_length+i,j);
            p_tmp.at<double>(2*i,0) = x+y;
            p_tmp.at<double>(2*i+1,0) = x-y;

        }
        if(data_length_prev % 2){
            p_tmp.at<double>(data_length_prev -1,0) = _data.at<double>(data_length-1,j);
        }
        for(int i=0; i<data_length_prev; i++){
            _data.at<double>(i,j) = p_tmp.at<double>(i,0);
        }

    }
    _resolution /= 2;
}

// compress the whole timestamp to 2 values (m*n -> 2*n)
void HaarWavelet::init(){
    for(int i=_R;i>2;i = (i+1)/2)
        compress();
}

cv::Mat HaarWavelet::getCompressedData(){
    int data_length = _R;
    for(int i=0;i<log2(_resolution);i++){
        data_length = (data_length+1) / 2;
    }
    return _data(cv::Rect(0,0,_C,data_length));
}
