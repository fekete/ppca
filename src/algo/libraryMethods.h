#ifndef PPCA_LIBRARYMETHODS_H
#define PPCA_LIBRARYMETHODS_H

#include "pca.hpp"

class LibraryMethod : public PCA {
public:
    LibraryMethod(const std::string& name, Matrix data, int maxIter);
    Matrix fit_transform() override;
protected:
    Matrix calculate_OpenCV_PCA();
    Matrix calculate_OpenCV_SVD();
    Matrix calculate_Eigensolver();
    Matrix calculate_Spectra();
    Matrix calculate_Eigen();
private:
    void fit() override;
};

#endif //PPCA_LIBRARYMETHODS_H
