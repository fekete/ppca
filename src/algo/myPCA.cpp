#include "myPCA.h"
#include "libraryMethods.h"
#include "prog_powerMethod.h"
#include "prog_powerMomentum.h"
#include "prog_lanczos.h"
#include "prog_incrementalPCA.h"
#include "incrementalPCA.h"
#include "prog_blockPower.h"
#include "prog_blockPowerMomentum.h"
#include "lanczos.h"
#include "covarianceFree.h"
#include "randomizedSVD.h"
#include "prog_randomizedSVD.h"
#include "wavelet/prog_haar_wavelet.h"
#include "prog_minibatch.h"
#include "miniBatch.h"
#include "powerMethod.h"
#include "../global_val.h"
#include "ppca_core.h"
#include <iomanip>

myPCA::myPCA(Matrix data) {
    this->_data = data;
    _max_iter = 5000;
    _R = rows(data);
    _C = cols(data);
    _result_prev = Matrix::zeros(2,_C,CV_64F);
}

myPCA::~myPCA() {
    _data.release();
    _data_meanSub.release();
    _data_covar.release();
    delete pca; pca = nullptr;
}

void myPCA::setPrintTimeRes(bool b){
    printTimeRes = b;
}

string myPCA::getMethod(){
    return pca->name();
}

int myPCA::algo_chosen = ALGO_POWER;

const char* myPCA::algorithm_names[ALGO_LAST] = {
    "OpenCV PCA",
    "OpenCV SVD",
    "Eigensolver",
    "Power Method",
    "Spectra",
    "Progressive Power Method",
    "Progressive Power Method + Momentum(broken)",
    "Power Method + Mini batch",
    "Progressive Power Method + Mini batch(doesn't work)",
    "Lanczos(in dev)",
    "Progressive Lanczos(doesn't work)",
    "Haar Wavelet",
    "My Incremental PCA",
    "Progressive Block Power Method + Block",
    "Progressive Block Power Method + Momentum",
    "Covariance Free(in dev)",
    "Randomized SVD",
    "Progressive Randomized SVD",
    "Incremental PCA"
};

void myPCA::_callAlgo() {
    int _batch_size = 10;

    Algo algo = Algo(algo_chosen);
    switch (algo) {
        case ALGO_OPENCV:
            pca = new LibraryMethod("OpenCV PCA", _data, _max_iter);
            break;
        case ALGO_SVD:
            pca = new LibraryMethod("OpenCV SVD", _data, _max_iter);
            break;
        case ALGO_EIGEN:
            pca = new LibraryMethod("Eigensolver", _data, _max_iter);
            break;
        case ALGO_POWER:
            pca = new PowerMethod("Power Method", _data, _max_iter);
            break;
        case ALGO_SPECTRA:
            pca = new LibraryMethod("Spectra", _data, _max_iter);
            break;
        case ALGO_PROG_POWER:
            pca = new ProgressivePowerMethod("Prog Power Method", _data, _max_iter);
            break;
        case ALGO_PROG_POWER_MOMENTUM:
            pca = new ProgressivePowerMomentum("Prog Power Momentum", _data, _max_iter);
            break;
        case ALGO_MINIBATCH:
            pca = new MiniBatch("Mini Batch", _data, _max_iter, _batch_size);
            break;
        case ALGO_PROG_POWER_MINIBATCH:
            pca = new ProgressiveMiniBatch("Prog Mini Batch", _data, _max_iter, _batch_size);
            break;
        case ALGO_LANCZOS:
            // TODO: to see how to choose the best size of auxiliary matrix, how to choose it automatically?
            // Small matrix -> low accuracy ; Big Matrix -> Slow
            pca = new MyLanczos("Lanczos", _data, 6);
            break;
        case ALGO_PROG_LANCZOS:
            pca = new ProgressiveLanczos("Prog Lanczos", _data, _max_iter);
            break;
        case ALGO_HAAR:
            pca = new ProgressiveHaarWavelet("Prog Haar Wavelet", _data, _max_iter);
            break;
        case ALGO_MY_INCREMENTAL:
            pca = new ProgressiveIncrementalPCA("My Incremental PCA", _data, _max_iter, _batch_size);
            break;
        case ALGO_PROG_POWER_BLOCK:
            pca = new ProgressivePowerMethodBlock("Prog Power Block", _data, _max_iter);
            break;
        case ALGO_PROG_POWER_BLOCK_MOMENTUM:
            pca = new ProgressivePowerMethodBlockMomentum("Prog Power Block + M", _data, _max_iter);
            break;
        case ALGO_COV_FREE:
            pca = new PowerMethodCovarianceFree("Covariance Free", _data, _max_iter);
            break;
        case ALGO_RANDOMIZED_SVD:
            pca = new RandomizedSVD("Randomized SVD", _data, _max_iter);
            break;
        case ALGO_PROG_RANDOMIZED_SVD:
            pca = new ProgressiveRandomizedSVD("Prog Randomized SVD", _data, _max_iter);
            break;
        case ALGO_INCREMENTAL:
            pca = new IncrementalPCA("Incremental PCA", _data, _max_iter, _batch_size);
            break;
        default:
            cerr << "Algorithm ID Error : " << algo_chosen << " . Choose Value 0 as default." << endl;
            algo_chosen = 0;
            pca = new LibraryMethod("OpenCV Eigen", _data, _max_iter);
            break;
    }
    progressive = dynamic_cast<Progressive*>(pca);
    if(progressive != nullptr)
        _progressiveStateSingleton->getState()->_start_progressive = true;
}

Matrix myPCA::calculate() {
    Matrix DataMatProj;
    // initialization
    if (progressive == nullptr) {
        auto t1 = std::chrono::high_resolution_clock::now();
        _callAlgo();
        auto t2 = std::chrono::high_resolution_clock::now();
        _timer = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;
    }
    // starts iteration algorithms here
    if (progressive == nullptr) {
        auto* libpca = dynamic_cast<LibraryMethod*>(pca);
        if(libpca == nullptr){
            pca->fit();
            DataMatProj = pca->transform();
        }
        else{
            DataMatProj = pca->fit_transform();
        }
    } else {
        double time = 0;
        progressive->partial_fit(true,&time);
        DataMatProj = pca->transform();
        _timer += time;
    }

    _result = DataMatProj(cv::Rect(0,0,pca->_nb_points_using,2));
    cv::normalize(_result, _result,-0.9,0.9,cv::NORM_MINMAX);

    if (progressive!= nullptr && progressive->is_terminated()){
        progressive = nullptr;
    }

    return DataMatProj;
}

int myPCA::number_points_calculated(){
    if(pca == nullptr)
        return 0;
    return pca->_nb_points_using;
}