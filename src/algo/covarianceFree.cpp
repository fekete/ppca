#include "covarianceFree.h"

PowerMethodCovarianceFree::PowerMethodCovarianceFree(const std::string& name, Matrix data, 
                                                     int maxIter)
  : PCA(name, data, maxIter) { _transpose = false; }

Matrix PowerMethodCovarianceFree::power(Matrix data, Matrix init, int& nbIters, int maxIter, double& _eigenValue){

    double delta;

    Matrix r = initRandomRowVector(data.rows);
    Matrix eigVal,error;

    r /= norm(r);
    for(int i=0;i<maxIter; i++){
        Matrix s = Matrix::zeros(data.rows,1,CV_64F);

        for(int j=0;j<data.cols;j++){
            //Matrix x = A_raw(cv::Rect(0,j,A_raw.cols,1));
            Matrix x = data(cv::Rect(j,0,1,data.rows));

            //cout << x.size << " = " << r.size <<endl;

            s = s + x.dot(r) * x;
        }

        delta = 1 - pow(r.dot(s/norm(s)),2) / pow((s/norm(s)).dot(s/norm(s)),2);


        if(abs(delta) < _epsilon){
            eigVal = r.t()*s;
            _eigenValue = eigVal.at<double>(0,0) / data.cols;
            //cout << i << endl;
            break;
        }

        r = s / norm(s);
        updatePlot(delta,_epsilon);

    }

    return r;
}

void PowerMethodCovarianceFree::fit(){
    _data_meanSub = meanSubtraction(_data);

    int nbIters1 = 0, nbIters2 = 0;
    double time1,time2;
    progress_update_first = true;
    auto t1 = std::chrono::high_resolution_clock::now();
    _eigenvectors1 = power(_data, _eigenvectors1 , nbIters1, _max_iter, _eigenvalues[0]);
    auto t2 = std::chrono::high_resolution_clock::now();

    //_eigenvalues[0] =  (_data * _eigenvectors1).dot(_eigenvectors1) / _eigenvectors1.dot(_eigenvectors1);


    //Matrix B = _data - _eigenvectors1 * _eigenvalues[0] / _eigenvectors1.dot(_eigenvectors1) * _eigenvectors1.t();
    progress_update_first = false;
    auto t3 = std::chrono::high_resolution_clock::now();
    _eigenvectors2 = power(_data, _eigenvectors2 , nbIters2, _max_iter, _eigenvalues[1]);
    auto t4 = std::chrono::high_resolution_clock::now();

    //_eigenvalues[1] =  (B * _eigenvectors2).dot(_eigenvectors2) / _eigenvectors2.dot(_eigenvectors2);


    time1 = nbIters1 == 0 ? 0 : std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0 / nbIters1;
    time2 = nbIters2 == 0 ? 0 : std::chrono::duration_cast<std::chrono::milliseconds>(t4-t3).count()/1000.0 / nbIters2;
    partialResults(_epsilon, nbIters1,nbIters2, time1, time2);

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);
}
