#ifndef PPCA_PROGRESSIVE
#define PPCA_PROGRESSIVE

#include "pca.hpp"
#include "ppca_core.h"
#include <utility> // for std::pair


class Progressive : public PCA {
public:
  typedef double steps_t; // number of steps to run, can contain fractional parts.
  enum state_t {
                state_created = 0,
                state_ready = 1,
                state_running = 2,
                state_blocked = 3,
                state_zombie = 4,
                state_terminated = 5,
                state_invalid = 6
  };
  typedef std::pair<state_t,steps_t> run_step_return;
  static const std::string& state_name(int);

//protected:
  //virtual ~Progressive();

public:
  Progressive(const std::string& name, const Matrix data, int maxIter);
  state_t state() const { return _state; }
  steps_t maxStep() const { return _max_step; }
  int components_number() const { return _nb_components; }
  bool is_terminated() const;
  virtual run_step_return run_step(steps_t steps) = 0;
  virtual void fit() override;
  void partial_fit(bool auto_adjust_step=true, double *time = 0);
  virtual double reconstruction_error(Matrix result);
  virtual void adjust_step(double exec_time);
  virtual void set_step(int batch_size);
  void test_partial_result_converge(Matrix result_prev, Matrix result);

protected:
  state_t          _state;
  steps_t          _max_step;
  run_step_return  _state1;
  run_step_return  _state2;
  ProgressiveStateSingleton* _progressiveStateSingleton;
  steps_t        _step_per_iteration;
  double         _time_constant_second;
  int            _nb_components;
  virtual void _partial_fit();

};

#endif
