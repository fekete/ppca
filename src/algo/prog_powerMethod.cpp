#include "prog_powerMethod.h"

ProgressivePowerMethod::run_step_return
ProgressivePowerMethod::progressive_power(Matrix data, Vector eigVec, double& eigVal, steps_t steps_to_do)
{
    double delta = 0;
    for(int i=0;i<steps_to_do;i++){
        Vector eigVecPrev = eigVec.clone();

        eigVec = data * eigVec;
        eigVec = eigVec / norm(eigVec);

        delta = measureEigenvectors(eigVecPrev,eigVec);

        if(abs(delta) < _epsilon){
            eigVal =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
            updatePlot(delta,_epsilon);
            return run_step_return(state_terminated, i);
        }
    }
    eigVal =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
    updatePlot(delta,_epsilon);
    return run_step_return(state_ready, steps_to_do);
}

ProgressivePowerMethod::run_step_return
ProgressivePowerMethod::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second + _state2.second); // already done

    if(_state1.first != state_terminated){
        progress_update_first = true;
        progress_update_second = false;
        auto t1 = std::chrono::high_resolution_clock::now();
        run_step_return ret1 = progressive_power(_data_covar, _eigenvectors1, _eigenvalues[0], steps/2);
        auto t2 = std::chrono::high_resolution_clock::now();
        _state1.first = ret1.first;
        _state1.second += ret1.second;
        _time1 += std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;
        _data2 = _data_covar - _eigenvectors1 * _eigenvalues[0] / _eigenvectors1.dot(_eigenvectors1) * _eigenvectors1.t();
    }

    //if(_state2.first != state_terminated) {
        progress_update_first = false;
        progress_update_second = true;
        auto t3 = std::chrono::high_resolution_clock::now();
        run_step_return ret2 = progressive_power(_data2, _eigenvectors2, _eigenvalues[1], steps/2);
        auto t4 = std::chrono::high_resolution_clock::now();
        _state2.first = ret2.first;
        _state2.second += ret2.second;
        _time2 += std::chrono::duration_cast<std::chrono::milliseconds>(t4-t3).count()/1000.0;
    //}

    if (_state1.first == state_terminated && _state2.first == state_terminated)
        _state = state_terminated;
    else
        _state = state_ready;

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);

    double time1_average = _state1.second == 0 ? 0 : _time1 / _state1.second;
    double time2_average = _state2.second == 0 ? 0 : _time2 / _state2.second;
    partialResults(_epsilon, _state1.second,_state2.second, time1_average, time2_average);

    return run_step_return(_state, _state1.second + _state2.second);
}

void ProgressivePowerMethod::_init(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);
}
