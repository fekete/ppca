#include "prog_blockPower.h"

ProgressivePowerMethodBlock::run_step_return
ProgressivePowerMethodBlock::progressive_power(Matrix data, Matrix &eigVec, steps_t steps_to_do)
{
    Matrix eigVecPrev,z;
    double delta = 0;
    for(int i=0;i<steps_to_do;i++){
        eigVecPrev = eigVec.clone();
        z = data * eigVec;

        eigVec = myQR_complet(z,2);

        delta = measureEigenvectors(eigVecPrev,eigVec);
        if(abs(delta) < _epsilon){
            //eigVal =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
            updatePlot(delta,_epsilon);
            return run_step_return(state_terminated, i);
        }
    }
    //eigVal =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
    updatePlot(delta,_epsilon);

    Matrix ev = eigVec.t() * data * eigVec;
    _eigenvalues[0] = ev.at<double>(0,0);
    _eigenvalues[1] = ev.at<double>(1,1);

    return run_step_return(state_ready, steps_to_do);
    //return eigVec;
}

ProgressivePowerMethodBlock::run_step_return
ProgressivePowerMethodBlock::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second + _state2.second); // already done

    progress_update_first = true;
    progress_update_second = true;

    if(_state1.first != state_terminated){
        auto t1 = std::chrono::high_resolution_clock::now();
        Progressive::run_step_return ret = progressive_power(_data_covar, _components, steps);
        auto t2 = std::chrono::high_resolution_clock::now();
        _state1.first = ret.first;
        _state1.second += ret.second;
        _time1 += std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;

        _eigenvectors1 = _components(cv::Rect(0,0,1,_components.rows));
        _eigenvectors2 = _components(cv::Rect(1,0,1,_components.rows));
    }

    _state = _state1.first;
    if(_state1.second >= _max_iter) _state = state_terminated;

    double time1_average = _state1.second == 0 ? 0 : _time1 / _state1.second;
    double time2_average = time1_average;
    partialResults(_epsilon, _state1.second,_state1.second, time1_average, time2_average);

    return run_step_return(_state, _state1.second + _state2.second);
}

void ProgressivePowerMethodBlock::_init(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);

    _components = concatTwoCols(_eigenvectors1,_eigenvectors2);
    //_eigenvalues[0] = (_data_covar * _eigenvectors1).dot(_eigenvectors1) / _eigenvectors1.dot(_eigenvectors1);
    //_epsilon = 1e-10;
}