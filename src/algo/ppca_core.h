#ifndef PPCA_PPCA_CORE_H
#define PPCA_PPCA_CORE_H

class ProgressiveState {
public:
    ProgressiveState();
    bool _start_progressive;
    bool _abort_calculation;
    bool _pause_calculation;
};

class ProgressiveStateSingleton
{
public:
    ProgressiveStateSingleton() = delete;
    //TODO: think about thread-safe
    inline static ProgressiveState* getState(){
        if (_state == 0)
        {
            _state = new ProgressiveState();
        }
        return _state;
    }

//    void setStartProgressive(bool b){
//        _state->_start_progressive = b;
//    }

private:
    static ProgressiveState* _state;
};



#endif //PPCA_PPCA_CORE_H
