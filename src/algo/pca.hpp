#ifndef PPCA_PCA
#define PPCA_PCA

#include "../tools.h"
#include <string>

struct PCA {
  PCA(const std::string& name, const Matrix data, int max_iter=10);
  virtual ~PCA();
  const std::string& name() const { return _name; }
  const double& epsilon() const { return _epsilon; }
  void epsilon(double e) { _epsilon = e; }
  const Matrix& data() const;
  const double* eigenvalues() const { return _eigenvalues; }
  const Vector& eigenvectors1() const { return _eigenvectors1; }
  const Vector& eigenvectors2() const { return _eigenvectors2; }

  static bool progress_update_first;
  static bool progress_update_second;
  static void updatePlot(double current_result, double epsilon);
  static void partialResults(double epsilon, int nbIters1, int nbIters2, 
                             double time1, double time2);
  int              _nb_points_using;

protected:
  std::string      _name;
  Matrix           _data;
  Matrix           _data_meanSub;
  Matrix           _data_covar;
  int              _max_iter;
  double           _epsilon;
  double           _eigenvalues[2];
  Vector           _eigenvectors1;
  Vector           _eigenvectors2;
  Matrix           _components;
  double           _time1;
  double           _time2;
  bool             _transpose;
  void             _sign_flip();
public:
  static void (* updatePlotCb)(double current_result, double epsilon);
  static void (* partialResultsCb)(double epsilon, int nbIters1, int nbIters2, 
                                   double time1, double time2);
  virtual void fit() = 0;
  virtual Matrix transform();
  virtual Matrix fit_transform();
  virtual Matrix inv_project(Matrix _result);
  
};

#endif
