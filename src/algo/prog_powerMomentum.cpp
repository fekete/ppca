#include "prog_powerMomentum.h"

double
ProgressivePowerMomentum::power(Matrix data, Vector& eigVec, Vector& eigVecPrev, double beta, steps_t steps_to_do, bool& finished) {
    Vector eigVec_tmp;
    double eigVec_norm;
    for(int i=0;i<steps_to_do;i++){
        eigVec_tmp = eigVec.clone();
        eigVec = data * eigVec - beta * eigVecPrev;
        eigVec_norm = norm(eigVec);
        eigVec = eigVec / eigVec_norm;
        eigVecPrev = eigVec_tmp / eigVec_norm ; /// eigVec_norm
        //cout << 1 - pow(eigVecPrev.dot(eigVec),2) / pow(eigVec.dot(eigVec),2) << endl;
        // FIXME: this
        double delta = measureEigenvectors(eigVec_tmp,eigVec);
        //cout << delta << endl;
        if(abs(delta) < _epsilon){
            break;
        }
    }
    double rayleigh =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
    double delta = 1 - pow(eigVec_tmp.dot(eigVec),2) / pow(eigVec.dot(eigVec),2);
    finished = abs(delta) < _epsilon;
    updatePlot(delta,_epsilon);
    return rayleigh;
}

// use best heavy ball (tuning momentum each S steps by comparing which momentum gives the largest eigenvalue)
ProgressivePowerMomentum::run_step_return
ProgressivePowerMomentum::progressive_power_M(Matrix data, Vector& eigVec, Vector& eigVecPrev, double& eigVal, double& beta, steps_t steps)
{
    Vector eigvec_tmp, eigvecPrev_tmp;
    Vector eigvec_tmp_best, eigvecPrev_tmp_best;
    double rayleigh_biggest = 0, rayleigh_tmp = 0;
    bool finished = false;
    for(int x=0;x<5;x++){
        eigvec_tmp = eigVec.clone();
        eigvecPrev_tmp = eigVecPrev.clone();
        rayleigh_tmp = power(data, eigvec_tmp, eigvecPrev_tmp, beta * _beta_range[x], steps, finished);

        if(rayleigh_biggest < rayleigh_tmp){
            rayleigh_biggest = rayleigh_tmp;
            eigvec_tmp_best = eigvec_tmp.clone();
            eigvecPrev_tmp_best = eigvecPrev_tmp.clone();
        }

        if(finished){
            eigVec = eigvec_tmp_best.clone();
            eigVecPrev = eigvecPrev_tmp_best.clone();
            eigVal = rayleigh_biggest;
            beta = pow(eigVal,2) / 4;
            return run_step_return(state_terminated, steps);
        }
    }
    //cout << delta << endl;
    eigVec = eigvec_tmp_best.clone();
    eigVecPrev = eigvecPrev_tmp_best.clone();
    eigVal = rayleigh_biggest;

    beta = pow(eigVal,2) / 4;
    return run_step_return(state_ready, steps);
}

// use eig2 as momentum
ProgressivePowerMomentum::run_step_return
ProgressivePowerMomentum::progressive_power_M2(Matrix data, Vector& eigVec, Vector& eigVecPrev, double& eigVal, double& beta, steps_t steps)
{

    bool finished = false;
    //cout << _beta2 << endl;
    eigVal = power(data, eigVec, eigVecPrev, beta , steps, finished);
    //cout << eigVal << endl;

    beta = pow(eigVal,2)/4;

    if(finished){
        return run_step_return(state_terminated, steps);
    }

    return run_step_return(state_ready, steps);
}

// use traditional power method to calculate
ProgressivePowerMomentum::run_step_return
ProgressivePowerMomentum::progressive_power_M3(Matrix data, Vector& eigVec, Vector& eigVecPrev, double& eigVal, double& beta, steps_t steps)
{
    eigVec = eigVec / norm(eigVec);
    double delta;
    for(int i=0;i<steps;i++){
        Vector xPre = eigVec.clone();
        eigVec = data * eigVec;
        eigVec = eigVec / norm(eigVec);

        delta = measureEigenvectors(xPre,eigVec);
        if(abs(delta) < _epsilon){
            eigVal = (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
            beta = pow(eigVal,2)/4;
            return run_step_return(state_terminated, i);
        }
    }
    eigVal = (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
    beta = pow(eigVal,2)/4;
    return run_step_return(state_ready, steps);
}

ProgressivePowerMomentum::run_step_return
ProgressivePowerMomentum::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second + _state2.second); // already done

    if(_state1.first != state_terminated) {
        progress_update_first = true;
        progress_update_second = false;
        auto t1 = std::chrono::high_resolution_clock::now();
        run_step_return ret1 = progressive_power_M2(_data_covar, _eigenvectors1, _eigenvectors1Prev, _eigenvalues[0], _beta2, steps/3);
        auto t2 = std::chrono::high_resolution_clock::now();
        _state1.first = ret1.first;
        _state1.second += ret1.second;
        _time1 += std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;
        _data2 = _data_covar - _eigenvectors1 * _eigenvalues[0] / _eigenvectors1.dot(_eigenvectors1) * _eigenvectors1.t();
        //if (!_beta2) {
        //    _eigenvalues[1] = (_data2 * _eigenvectors2).dot(_eigenvectors2) / _eigenvectors2.dot(_eigenvectors2);
        //    _beta2 = pow(_eigenvalues[1], 2) / 4;
        //}
    }
    //if(_state2.first != state_terminated) {
        progress_update_first = false;
        progress_update_second = true;
        auto t3 = std::chrono::high_resolution_clock::now();
        run_step_return ret2 = progressive_power_M2(_data2, _eigenvectors2, _eigenvectors2Prev, _eigenvalues[1], _beta3, steps/3);
        //run_step_return ret2 = progressive_power_M3(_data2, _eigenvectors2, _eigenvectors2Prev, _eigenvalues[1], _beta2, steps/3);
        _beta2 = pow(_eigenvalues[1],2)/4;
        auto t4 = std::chrono::high_resolution_clock::now();
        //cout << "2 run " << ret2.second << " steps" << endl;
        _state2.first = ret2.first;
        _state2.second += ret2.second;
        _time2 += std::chrono::duration_cast<std::chrono::milliseconds>(t4-t3).count()/1000.0;
    //}

    if(_state1.first != state_terminated) {
        _data3 = _data2 - _eigenvectors2 * _eigenvalues[1] / _eigenvectors2.dot(_eigenvectors2) * _eigenvectors2.t();
        progressive_power_M3(_data3, _eigenvectors3, _eigenvectors3, _eigenvalue3, _beta3, steps / 3);
    }

    //cout << _eigenvalue3 << endl;

    if (_state1.first == state_terminated && _state2.first == state_terminated){
        _state = state_terminated;
    }
    else
        _state = state_ready;

    double time1_average = _state1.second == 0 ? 0 : _time1 / _state1.second;
    double time2_average = _state2.second == 0 ? 0 : _time2 / _state2.second;
    partialResults(_epsilon, _state1.second,_state2.second, time1_average, time2_average);

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);

    return run_step_return(_state, _state1.second + _state2.second);
}

void ProgressivePowerMomentum::_init(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);

    _beta3 = 0;
    _beta2 = 0;
    ///_eigenvectors1Prev =  Matrix::zeros( _data_covar.rows, 1, _data_covar.type() );
    ///_eigenvectors2Prev = Matrix::zeros( _data_covar.rows, 1, _data_covar.type() );
    _eigenvectors3 = Matrix::zeros( _data_covar.rows, 1, _data_covar.type() );
    for(int i=0;i<_data_covar.rows;i++){
        _eigenvectors3.at<double>(i,0) =  randDouble(-1,1);
    }
    _eigenvectors3 = _eigenvectors3 / norm(_eigenvectors3);
}