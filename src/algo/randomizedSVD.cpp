#include <Eigen/Eigenvalues>
#include "randomizedSVD.h"

RandomizedSVD::RandomizedSVD(const std::string& name, Matrix data, int maxIter)
        : PCA(name, data, maxIter)
{
    _nb_iters = 7;
    _aux_matrix_size = 12;
}

Matrix RandomizedSVD::construct_approximate_matrix(){

    Matrix _aux_matrix = Matrix::zeros( _data.cols, _aux_matrix_size, _data.type() );

    // random matrix with normal distribution
    double mean = 0.0;
    double stddev = 1.0;
    randn(_aux_matrix, cv::Scalar(mean), cv::Scalar(stddev));

/*
    double low = -1.0;
    double high = +1.0;
    randu(_aux_matrix, cv::Scalar(low), cv::Scalar(high));
*/

/*
    // With power method
    for(int c=0; c<_nb_iters;c++){
        _aux_matrix = _data_meanSub * _aux_matrix;
        _aux_matrix = _data_meanSub.t() * _aux_matrix;
    }
*/


    // With QR
    for(int c=0; c<_nb_iters;c++){
        Matrix AQ = _data_meanSub * _aux_matrix;
        _aux_matrix = myQR_complet(AQ, _aux_matrix_size);
        Matrix ATQ = _data_meanSub.t() * _aux_matrix;
        _aux_matrix = myQR_complet(ATQ, _aux_matrix_size);
    }



/*
    // With pivoted LU

    //Eigen::MatrixXd _data_mean;//(_data_meanSub.rows,_data_meanSub.cols);
    //Eigen::MatrixXd _aux;//(_aux_matrix.rows,_aux_matrix.cols);
    Eigen::MatrixXd _data_mean(_data_meanSub.rows,_data_meanSub.cols);
    Eigen::MatrixXd _aux(_aux_matrix.rows,_aux_matrix.cols);

    //cv::cv2eigen(_data_meanSub, _data_mean);
    //cv::cv2eigen(_aux_matrix, _aux);

    for(int x=0;x<_data_mean.rows();x++)
        for(int y=0;y<_data_mean.cols();y++)
            _data_mean(x,y) = _data_meanSub.at<double>(x,y);

    for(int x=0;x<_aux.rows();x++)
        for(int y=0;y<_aux.cols();y++)
            _aux(x,y) = _aux_matrix.at<double>(x,y);

    Eigen::FullPivLU<Eigen::MatrixXd> lu;

    for(int c=0; c<_nb_iters;c++){
        clock_t tStart1 = clock();

        lu = Eigen::FullPivLU<Eigen::MatrixXd>(_data_mean * _aux);
        _aux = lu.matrixLU().triangularView<Eigen::UpLoType::UnitLower>();
        lu = Eigen::FullPivLU<Eigen::MatrixXd>(_data_mean.transpose() * _aux);
        _aux = lu.matrixLU().triangularView<Eigen::UpLoType::UnitLower>();

        clock_t tStart2 = clock();
        double _timett = (double) (tStart2 - tStart1) / CLOCKS_PER_SEC;
        cout << "LU: " << _timett << " s." << endl;
    }

    for( int i=0; i<_aux.rows(); ++i )
        for( int j=0; j<_aux.cols(); ++j )
            _aux_matrix.at<double>(i, j) = _aux(i, j);

    //cv::eigen2cv(_aux,_aux_matrix);
*/

    Matrix AQ = _data_meanSub * _aux_matrix;
    _aux_matrix = myQR_complet(AQ, _aux_matrix_size);
    // new data matrix with a small rank
    return _aux_matrix.t() * _data_meanSub;
}

void RandomizedSVD::fit(){
    ///auto t1 = std::chrono::high_resolution_clock::now();
    _data_meanSub = meanSubtraction(_data);
    ///auto t2 = std::chrono::high_resolution_clock::now();

    ///auto t3 = std::chrono::high_resolution_clock::now();
    _approximate_matrix = construct_approximate_matrix();
    ///auto t4 = std::chrono::high_resolution_clock::now();

    ///double _timett = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;
    ///double _timett2 = std::chrono::duration_cast<std::chrono::milliseconds>(t4-t3).count()/1000.0;
    ///cout << "Mean Sub: " << _timett << " s." << endl;
    ///cout << "Construct approximate range: " << _timett2 << " s." << endl;

    Matrix D0,U0,V0;
    cv::SVD::compute(_approximate_matrix,D0,U0,V0,0);
    _components = U0(cv::Rect(0, 0, 2, U0.rows));

    //_components = _components * diagonalize(D0(cv::Rect(0, 0, 2, 1)));
    //cout << _components.size << " == " << D0(cv::Rect(0, 0, 2, 1)).size << endl;
}

Matrix RandomizedSVD::transform(){
    _sign_flip();
    Matrix _result = _components.t()*_approximate_matrix;
    return _result;
}