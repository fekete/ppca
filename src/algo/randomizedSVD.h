#ifndef PPCA_RANDOMIZEDSVD_H
#define PPCA_RANDOMIZEDSVD_H

#include "pca.hpp"

class RandomizedSVD : public PCA {
public:
    RandomizedSVD(const std::string& name, Matrix data, int maxIter);
    void fit() override;
    Matrix transform() override;

protected:
    Matrix construct_approximate_matrix();

private:
    int _nb_iters;
    int _aux_matrix_size;
    Matrix _approximate_matrix;
};

#endif //PPCA_RANDOMIZEDSVD_H
