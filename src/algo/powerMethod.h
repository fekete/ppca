#ifndef PPCA_POWERMETHOD_H
#define PPCA_POWERMETHOD_H

#include "pca.hpp"

class PowerMethod : public PCA {
public:
    PowerMethod(const std::string& name, Matrix data, int maxIter);
    void fit() override;

protected:
    Matrix power(Matrix data, Vector init, int epoch);
    Matrix power2(Matrix data, Vector init, int& nbIters, int maxIter);
    Matrix power3(Matrix data, Vector init, int& nbIters, int maxIter);

};


#endif //PPCA_POWERMETHOD_H
