#include "prog_randomizedSVD.h"

// stops if the angle of two eigenvectors is small enough
Matrix ProgressiveRandomizedSVD::block_power(Matrix data, Matrix eigBasis){
    Matrix eigBasisPrev,z;
    double delta = 0;
    for(int i=0;i<_max_iter;i++){
        eigBasisPrev = eigBasis.clone();
        z = data * eigBasis;

        eigBasis = myQR_complet(z,2);

        delta = measureEigenvectors(eigBasisPrev,eigBasis);
        if(abs(delta) < 1e-7){
            break;
        }
    }

    Matrix ev = eigBasis.t() * data * eigBasis;
    _eigenvalues[0] = ev.at<double>(0,0);
    _eigenvalues[1] = ev.at<double>(1,1);
    return eigBasis;
}

ProgressiveRandomizedSVD::run_step_return
ProgressiveRandomizedSVD::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second); // already done

    // first call
    if(_state1.second == 0){
        double mean = 0.0;
        double stddev = 1.0;
        randn(_aux_matrix, cv::Scalar(mean), cv::Scalar(stddev));
        _state1.second++;
    }
    else{
        for(int c=0; c<steps;c++){
            AQ = _data_meanSub * _aux_matrix;
            _aux_matrix = myQR_complet(AQ, _aux_matrix_size);
            ATQ = _data_meanSub.t() * _aux_matrix;
            _aux_matrix = myQR_complet(ATQ, _aux_matrix_size);

            AQ.release();ATQ.release();
            _state1.second++;
        }
    }

    ///auto t1 = std::chrono::high_resolution_clock::now();
    AQ = _data_meanSub * _aux_matrix;
    AQO = myQR_complet(AQ, _aux_matrix_size);
    AQ.release();
    ///auto t2 = std::chrono::high_resolution_clock::now();
    ///adjust_step(std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0);


    // new data matrix with a small rank
    _reduced_data = AQO.t() * _data_meanSub;

    _data_covar = covMat(_reduced_data, _transpose);

    _components_tmp = block_power(_data_covar, _components_tmp);


    _components = (_components_tmp.t() * AQO.t()).t();

    _eigenvectors1 = _components(cv::Rect(0,0,1,_components.rows));
    _eigenvectors2 = _components(cv::Rect(1,0,1,_components.rows));

    double delta = measureEigenvectors(_components_prev,_components);
    if(delta < _epsilon)
        _state = state_terminated;

    _components_prev = _components.clone();
    progress_update_first = true;
    progress_update_second = true;
    updatePlot(delta,_epsilon);
    double time1_average = _state1.second == 0 ? 0 : _time1 / _state1.second;
    double time2_average = time1_average;
    partialResults(_epsilon, _state1.second,_state2.second, time1_average, time2_average);

    return run_step_return(_state, _state1.second);
}

void ProgressiveRandomizedSVD::_init(){
    _transpose = false;
    _aux_matrix_size = max(_data.rows > 12?12:_data.rows-1,2);
    _max_step = 1000;
    _step_per_iteration = 1;
    _epsilon = 5e-4;

    _aux_matrix = Matrix::zeros( _data.cols, _aux_matrix_size, _data.type() );
    _data_meanSub = meanSubtraction(_data);


    _eigenvectors1 = generateRandomVector(_aux_matrix_size);
    _eigenvectors2 = generateRandomVector(_aux_matrix_size);
    _components_tmp = concatTwoCols(_eigenvectors1,_eigenvectors2);
    _components_prev = Matrix::zeros( _data.rows, 2, _data.type() );
}

void ProgressiveRandomizedSVD::_partial_fit(){
    auto t1 = std::chrono::high_resolution_clock::now();
    Progressive::run_step_return ret = run_step(_step_per_iteration);
    auto t2 = std::chrono::high_resolution_clock::now();
    _time1 += std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;

    if(ret.first == Progressive::state_terminated){
        _progressiveStateSingleton->getState()->_start_progressive = false;
    }

    if(ret.second >= maxStep()){
        _progressiveStateSingleton->getState()->_abort_calculation = true;
    }

    double time1_average = _state1.second == 0 ? 0 : _time1 / _state1.second;
    double time2_average = 0;
    partialResults(_epsilon, _state1.second,_state2.second, time1_average, time2_average);
}
/*
Matrix ProgressiveRandomizedSVD::transform(){
    _sign_flip();
    _result = _components.t()*_reduced_data;
    ///cout << _components.size << " == " << _reduced_data.size << " == " << _data_meanSub.size << endl;
    return _result;
}


void ProgressiveRandomizedSVD::adjust_step(double exec_time){
    double m = _time_constant_second / exec_time;
    if(m < 1) _step_per_iteration = 1;

    int _step_per_second_expected = (m-1) / 2 +1;
    double multiplier = _step_per_iteration / _step_per_second_expected;

    if(multiplier < 1)
        _step_per_iteration = _step_per_iteration * sqrt(m);
    else
        _step_per_iteration = _step_per_iteration * pow(m,2);

    _step_per_iteration = (m-1) / 2 +1;

    if(_step_per_iteration < 1) _step_per_iteration = 1;
    if(_step_per_iteration > 1000) _step_per_iteration = 1000;
}

double ProgressiveRandomizedSVD::calculate_error(Matrix result){
    cout << _data_meanSub.size << " >>>> " << inv_project(result).size << endl;
    cv::Scalar neam=cv::mean(cv::abs(_data_meanSub - inv_project(result)));
    return neam.val[0];
}
*/
Matrix ProgressiveRandomizedSVD::inv_project(Matrix _result){
    Matrix inv_result;
    Matrix eigenbasis =  AQO * _components;
    //_sign_flip(_eigenbasis);
    if(_transpose) //FIXME: this branch
        inv_result = (eigenbasis * _data_meanSub.t()).t() * _result;
    else
        inv_result = eigenbasis * _result;
    return inv_result;
}