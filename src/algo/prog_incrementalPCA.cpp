#include "prog_incrementalPCA.h"

// return [U, D, mu, n]
void ProgressiveIncrementalPCA::sklm(Matrix data, Matrix& U0, Matrix& D0, Matrix& mu0, int& n0, double FF, int K){
    // first iteration
    if(n0 == 0) {
        data = meanSubtraction(data, mu0);
        Matrix V0;
        cv::SVD::compute(data,D0,U0,V0,0);
        n0 = data.cols;

        D0 = D0(cv::Rect(0,0,1,2));
        U0 = U0(cv::Rect(0,0,2,U0.rows));

    }
    else{
        Matrix mu1;
        data = meanSubtraction(data, mu1);       ///   cout<< "Data: " << data.size << endl;
        int n1 = data.cols;
        data  = concatTwoCols(data,sqrt(n0*n1/(double)(n0+n1))*(mu0-mu1));   ///    cout<< "Data After concat: " << data.size << endl;
        mu0 = (FF*n0*mu0 + n1*mu1)/(n1+FF*n0);       ///    cout<< "Mean: " << mu0.size << endl;
        n0 = n1+ FF*n0;
      ///  cout<< "U0 Size: " << U0.size << endl;
      ///  cout<< "D0 Size: " << D0.size << endl;
        Matrix data_proj = U0.t() * data;       ///     cout<< "Data project: " << data_proj.size << endl;
        Matrix data_res = data - U0*data_proj;   ///    cout<< "Data proj res: " << data_res.size << endl;

        Matrix _q1,_r1;

        ///clock_t tStart1 = clock();

        //Matrix data_res_tmp = data_res.clone();
        //qr_thin( data_res_tmp, _q1 );
        _q1 = myQR_complet(data_res,data_res.cols);

        ///clock_t tStart2 = clock();
        ///cout << "  >>>>>>>>> QR  Time: " << (double) (tStart2 - tStart1) / CLOCKS_PER_SEC << endl;
/*
        HouseHolderQR(data_res,_q1,_r1);
        if(data_res.rows > data_res.cols){
            _q1 = _q1(cv::Rect(0,0,data_res.cols,data_res.rows));
            // throw _r1
            //_r1 = _r1(cv::Rect(0,0,data_res.cols,data_res.cols));
        }
*/

        Matrix _Q1,_R1;
        Matrix _R1_up,_R1_down;
        _Q1 = concatTwoCols(U0,_q1);

        _R1_up = concatTwoCols(FF*diagonalize(D0),data_proj);
        _R1_down = concatTwoCols(Matrix::zeros(data.cols,D0.rows,CV_64F),(_q1.t()*data_res));
        _R1 = concatTwoRows(_R1_up,_R1_down);
     ///   cout<< "R Size: " << _R1.size << endl;
        Matrix V2;
        // update D0,U0
        ///clock_t tStart3 = clock();
        cv::SVD::compute(_R1,D0,U0,V2,0);

        ///clock_t tStart4 = clock();
        ///cout << "  >>>>>>>>> SVD Time: " << (double) (tStart4 - tStart3) / CLOCKS_PER_SEC << endl;

        D0 = D0(cv::Rect(0,0,1,2));
        U0 = _Q1 * U0(cv::Rect(0,0,2,U0.rows));cout << endl << endl;
    }
}


void ProgressiveIncrementalPCA::calculation_eigen(Matrix data)
{

    sklm(data, U0, D0, mu0, n0, FF, K);
    _index += _batch_size;  _nb_points_using = _index;

    Matrix _D = diagonalize(D0);
    Matrix _eigenVal = _D.t()*_D/n0;
    _eigenvalues[0] = _eigenVal.at<double>(0,0);
    _eigenvalues[1] = _eigenVal.at<double>(1,1);

    _components = U0;
    //Matrix _eigenVec = U0.t();
    //Matrix projection_res = _eigenVec * _data(cv::Rect(0,0,_index,_data.rows));

}

ProgressiveIncrementalPCA::run_step_return
ProgressiveIncrementalPCA::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second); // already done
    if(_index + _batch_size > _data.cols) _batch_size = _data.cols - _index;
    _data_batch = _data(cv::Rect(_index,0,_batch_size,_data.rows));

    calculation_eigen(_data_batch);

    _state1.second++;
    if((double)_index / _data.cols >= 1 ) _state = state_terminated;
    cout << (double)_index / _data.cols*100 << "% done." << endl;
    return run_step_return(_state, _state1.second);
}


void ProgressiveIncrementalPCA::_init(){
    _result = Matrix::zeros(2,_data.cols,CV_64F) + 1e4;
    //_batch_size = min(max(_data.rows,_data.cols/20),_data.rows);
}

void ProgressiveIncrementalPCA::_partial_fit(){
    Progressive::run_step_return ret = run_step(-9999);
    if(ret.first == Progressive::state_terminated){
        _progressiveStateSingleton->getState()->_start_progressive = false;
    }

    if(ret.second >= maxStep()){
        _progressiveStateSingleton->getState()->_abort_calculation = true;
    }
}

Matrix ProgressiveIncrementalPCA::transform(){
    _sign_flip();
    Matrix _eigenVec = U0.t() * _data(cv::Rect(0,0,_index,_data.rows));

    for(int i=0;i<_eigenVec.cols;i++){
        _result.at<double>(0,i) = _eigenVec.at<double>(0,i);
        _result.at<double>(1,i) = _eigenVec.at<double>(1,i);
    }

    return _result;
}

void ProgressiveIncrementalPCA::adjust_step(double exec_time){
    double m = _time_constant_second / exec_time;
    if(exec_time < _time_constant_second)
        _batch_size = _batch_size * sqrt(m);
    else
        _batch_size = _batch_size * pow(m,2);

    if(_batch_size < 1) _batch_size = 1;
    if(_batch_size > 1000) _batch_size = 1000;
}