#ifndef PPCA_PCA_H
#define PPCA_PCA_H

#include "progressive.hpp"
#include "ppca_core.h"

using namespace std;

class myPCA{
public:
    enum Algo {
        ALGO_OPENCV,
        ALGO_SVD,
        ALGO_EIGEN,
        ALGO_POWER,
        ALGO_SPECTRA,
        ALGO_PROG_POWER,
        ALGO_PROG_POWER_MOMENTUM,
        ALGO_MINIBATCH,
        ALGO_PROG_POWER_MINIBATCH,
        ALGO_LANCZOS,
        ALGO_PROG_LANCZOS,
        ALGO_HAAR,
        ALGO_MY_INCREMENTAL,
        ALGO_PROG_POWER_BLOCK,
        ALGO_PROG_POWER_BLOCK_MOMENTUM,
        ALGO_COV_FREE,
        ALGO_RANDOMIZED_SVD,
        ALGO_PROG_RANDOMIZED_SVD,
        ALGO_INCREMENTAL,
        ALGO_LAST
    };
    static const char* algorithm_names[ALGO_LAST];
    static int algo_chosen;
    Matrix calculate();
    myPCA(Matrix data);
    ~myPCA();
    void setPrintTimeRes(bool b);
    string getMethod();
    int number_points_calculated();

private:
    Matrix _data;
    Matrix _data_meanSub;
    Matrix _data_covar;
    int _R;
    int _C;
    int _max_iter;
    string method;
    PCA *pca;

    Matrix _result;
    Matrix _result_prev;
    Matrix _baseline;

    double _timer;

    void _callAlgo();
    Progressive *progressive = nullptr;
    ProgressiveStateSingleton* _progressiveStateSingleton;
};

#endif
