#include "libraryMethods.h"

#include <Eigen/Eigenvalues>
#include "../Spectra/SymEigsSolver.h"

LibraryMethod::LibraryMethod(const std::string& name, Matrix data, int maxIter)
        : PCA(name, data, maxIter)
{}

void LibraryMethod::fit(){}
Matrix LibraryMethod::fit_transform(){
    if(_name.compare("OpenCV PCA") == 0)
        return calculate_OpenCV_PCA();
    else if(_name.compare("OpenCV SVD") == 0)
        return calculate_OpenCV_SVD();
    else if(_name.compare("Eigensolver") == 0)
        return calculate_Eigensolver();
    else if(_name.compare("Spectra") == 0)
        return calculate_Spectra();
    else
        return calculate_Eigen();
}

Matrix LibraryMethod::calculate_OpenCV_PCA(){
    cv::PCA pca(_data, cv::noArray(), cv::PCA::DATA_AS_COL,2);
    return pca.project(_data);
}

Matrix LibraryMethod::calculate_OpenCV_SVD(){
    _data_meanSub = meanSubtraction(_data);
    _transpose = false;
    Matrix D0,U0,V0;
    cv::SVD::compute(_data_meanSub,D0,U0,V0,0);
    Matrix P = U0(cv::Rect(0, 0, 2, U0.rows));
    ///cout << D0.at<double>(0,0) << " = " << D0.at<double>(1,0) << endl;
    _components = P;
    return transform();
}

Matrix LibraryMethod::calculate_Eigensolver(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);
    int _R = _data_covar.rows;
    Eigen::MatrixXd covM(_R,_R);
    for(int x=0;x<_R;x++)
        for(int y=0;y<_R;y++)
            covM(x,y) = _data_covar.at<double>(x,y);

    Eigen::EigenSolver<Eigen::MatrixXd> s;
    s.compute(covM);
    Matrix surfaceP =  Matrix::zeros( 2, _R, _data.type() );
    Eigen::VectorXcd v1 = s.eigenvectors().col(0);
    Eigen::VectorXcd v2 = s.eigenvectors().col(1);

    for(int y=0;y<_R;y++)
        surfaceP.at<double>(0,y) = real(v1(y,0));
    for(int y=0;y<_R;y++)
        surfaceP.at<double>(1,y) = real(v2(y,0));
    //_sign_flip(surfaceP);
    _components = surfaceP.t();
    return transform();
}

Matrix LibraryMethod::calculate_Spectra(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);
    int _R = _data_covar.rows;
    Eigen::MatrixXd covM(_R,_R);
    for(int x=0;x<_R;x++)
        for(int y=0;y<_R;y++)
            covM(x,y) = _data_covar.at<double>(x,y);

    Matrix surfaceP =  Matrix::zeros( 2, _R, _data.type() );
    Spectra::DenseSymMatProd<double> op(covM);

    static int ncv_chosen = 6;
    if(_R < ncv_chosen)  ncv_chosen = _R;
    Spectra::SymEigsSolver< double, Spectra::LARGEST_ALGE, Spectra::DenseSymMatProd<double> > eigs(&op, 2, ncv_chosen);

    eigs.init();
    eigs.compute();

    Eigen::VectorXd evalues;
    Eigen::MatrixXd evecs;
    if(eigs.info() == Spectra::SUCCESSFUL) {
        evalues = eigs.eigenvalues();
        evecs = eigs.eigenvectors().transpose();
    }

    //int num_iter = eigs.num_iterations();

    for(int x=0;x<2;x++)
        for(int y=0;y<_R;y++)
            surfaceP.at<double>(x,y) = real(evecs(x,y));

    _components = surfaceP.t();
    return transform();
}

Matrix LibraryMethod::calculate_Eigen(){
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);

    Matrix eigenvalues,eigenvectors;
    cv::eigen(_data_covar, eigenvalues, eigenvectors);

    Matrix P =  Matrix::zeros( 2, _data_covar.rows, _data.type() );
    for ( int i=0; i<2; i++ )
        for ( int j=0; j<_data_covar.rows; j++ )
            P.at<double>(i,j) = eigenvectors.at<double>(i,j);

    _components = P.t();
    return transform();
}