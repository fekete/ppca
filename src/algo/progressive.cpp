#include "progressive.hpp"//xtensor

Progressive::Progressive(const std::string& name, const Matrix data, int maxIter)
  : PCA(name, data, maxIter), _state(state_created), _max_step(maxIter)
{
  _state1 = run_step_return(state_created, 0);
  _state2 = run_step_return(state_created, 0);
  _step_per_iteration = 1;
  _time_constant_second = 1;
  _nb_components = 2;
}

//Progressive::~Progressive()
//{
//}

void Progressive::fit(){
    while(true){
        Progressive::run_step_return ret = run_step(_step_per_iteration);
        if(ret.first == Progressive::state_terminated){
            _progressiveStateSingleton->getState()->_start_progressive = false;
            break;
        }

        if(ret.second >= maxStep()){
            _progressiveStateSingleton->getState()->_abort_calculation = true;
            break;
        }
    }

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);
}

void Progressive::partial_fit(bool auto_adjust_step, double *time){
    if(auto_adjust_step || time != nullptr) {
        auto t1 = std::chrono::high_resolution_clock::now();
        _partial_fit();
        auto t2 = std::chrono::high_resolution_clock::now();
        double exec_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()/1e6;;
        if (time != nullptr)
            *time = exec_time;
        if (auto_adjust_step)
            adjust_step(exec_time);
    }
    else {
        _partial_fit();
    }
}

void Progressive::_partial_fit() {
    Progressive::run_step_return ret = run_step(_step_per_iteration);
    if (ret.first == Progressive::state_terminated) {
        _progressiveStateSingleton->getState()->_start_progressive = false;
    }

    if (ret.second >= maxStep()) {
        _progressiveStateSingleton->getState()->_abort_calculation = true;
    }

    _components = concatTwoCols(_eigenvectors1, _eigenvectors2);
}

bool Progressive::is_terminated() const{
    return _state == state_terminated;
}

void Progressive::adjust_step(double exec_time){
    double m = _time_constant_second / exec_time;
    if(exec_time < _time_constant_second)
        _step_per_iteration = _step_per_iteration * sqrt(m);
    else
        _step_per_iteration = _step_per_iteration * pow(m,2);

    //_step_per_iteration = 1;
    if(_step_per_iteration < 1) _step_per_iteration = 1;
    if(_step_per_iteration > 10000) _step_per_iteration = 10000;
}

void Progressive::set_step(int batch_size){
    if(batch_size >= 1 && batch_size <= 10000)
        _step_per_iteration = batch_size;
}

double Progressive::reconstruction_error(Matrix result){
    ///cout << _data_meanSub.size << " >>>> " << inv_project(result).size << endl;
    cv::Scalar neam=cv::mean(cv::abs(_data_meanSub - inv_project(result)));
    return neam.val[0];
}

void Progressive::test_partial_result_converge(Matrix result_prev, Matrix result){
    Matrix _delta = abs(result - result_prev);
    //cout << cv::norm(_delta) << endl;
    if (cv::norm(_delta) < 1e-7)
        _progressiveStateSingleton->getState()->_abort_calculation = true;
}
