#ifndef PPCA_PROG_MINIBATCH_H
#define PPCA_PROG_MINIBATCH_H

#include "progressive.hpp"

class ProgressiveMiniBatch : public Progressive {
public:
    ProgressiveMiniBatch(const std::string& name, const Matrix data, int maxIter, int batch_size):
            Progressive(name,data,maxIter),_batch_size(batch_size>0 ? batch_size:1){_init();};
    virtual run_step_return run_step(steps_t steps) override;

protected:
    run_step_return progressive_power_minibatch(Matrix data, Vector& eigVec, double& eigVal, steps_t steps_to_do, bool secondEigen);
    void _partial_fit() override;

private:
    int _batch_size;
    Matrix generateMiniBatch(Matrix data);
    void _init();
    Matrix _origin_dataMeanSub;
    bool _transpose_batch;
};

#endif //PPCA_PROG_MINIBATCH_H
