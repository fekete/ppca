#include "prog_blockPowerMomentum.h"

ProgressivePowerMethodBlockMomentum::run_step_return
ProgressivePowerMethodBlockMomentum::progressive_power(Matrix data, Matrix& eigVec, steps_t steps_to_do)
{
    Matrix eigVecPrev,z;
    double delta = 0;
    for(int i=0;i<steps_to_do;i++,update_momentum_count++){
        eigVecPrev = eigVec.clone();
        //eigVecPrev = myQR_complet(eigVec,_nb_components);

        Matrix qq = data * eigVec;
        if(!update_momentum) qq -= _beta * _eigVecPrevPrev;

        Matrix eigVec_tmp = concatTwoRows(qq,eigVec);
        Matrix eigVec_stack = myQR_complet(eigVec_tmp,_nb_components);

        eigVec = eigVec_stack(cv::Rect(0,0,_nb_components,eigVec.rows));
        if(!update_momentum)
            _eigVecPrevPrev = eigVec_stack(cv::Rect(0,eigVec.rows,_nb_components,eigVec.rows));

        // normalization
        eigVec =  myQR_complet(eigVec,_nb_components);

        qq.release(); eigVec_tmp.release(); eigVec_stack.release();

        delta = measureEigenvectors(eigVecPrev,eigVec);

        if(update_momentum){
            if(abs(delta) < _epsilon_momentum){
                update_momentum = false;
                _nb_components = 2;
                eigVec = eigVec(cv::Rect(0,0,_nb_components,eigVec.rows));
            }
        }
        else if(abs(delta) < _epsilon){
            //eigVal =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
            updatePlot(delta,_epsilon);
            return run_step_return(state_terminated, i);
        }

    }
    //eigVal =  (data * eigVec).dot(eigVec) / eigVec.dot(eigVec);
    updatePlot(delta,_epsilon);

    return run_step_return(state_ready, steps_to_do);
    //return eigVec;
}

ProgressivePowerMethodBlockMomentum::run_step_return
ProgressivePowerMethodBlockMomentum::run_step(steps_t steps)
{
    if (_state == state_terminated) return run_step_return(_state, _state1.second + _state2.second); // already done

    progress_update_first = true;
    progress_update_second = true;

    auto t1 = std::chrono::high_resolution_clock::now();
    Progressive::run_step_return ret = progressive_power(_data_covar, _eigenbasis, steps);
    auto t2 = std::chrono::high_resolution_clock::now();
    _state1.first = ret.first;
    _state1.second += ret.second;
    _time1 += std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()/1000.0;

    Matrix ev = _eigenbasis.t() * _data_covar * _eigenbasis;

    _eigenvalues[0] = ev.at<double>(0,0);
    _eigenvalues[1] = ev.at<double>(1,1);

    if(update_momentum){_beta = pow(ev.at<double>(2,2),2) / 4;}

    _state = _state1.first;
    if(_state1.second >= _max_iter) _state = state_terminated;

    double time1_average = _state1.second == 0 ? 0 : _time1 / _state1.second;
    double time2_average = time1_average;
    partialResults(_epsilon, _state1.second,_state1.second, time1_average, time2_average);

    return run_step_return(_state, _state1.second + _state2.second);
}

void ProgressivePowerMethodBlockMomentum::_init(){
    update_momentum = true;
    _nb_components = 3;

    update_momentum_count = 0;
    _data_meanSub = meanSubtraction(_data);
    _data_covar = covMat(_data_meanSub, _transpose);

    _components = concatTwoCols(_eigenvectors1,_eigenvectors2);
    _eigenbasis = concatTwoCols(_components,initRandomRowVector(_eigenvectors1.rows));

    ///_eigenvalues[0] = (_data_covar * _eigenvectors1).dot(_eigenvectors1) / _eigenvectors1.dot(_eigenvectors1);
    _beta = 0;//pow(_eigenvalues[0],2) / 4;
    _eigVecPrevPrev = Matrix::zeros( _data_covar.rows, 2, _data_covar.type() );
    //_eigVecPrevPrev = eigVecBlock.clone();
    //_epsilon = 1e-10;
}

void ProgressivePowerMethodBlockMomentum::_partial_fit() {
    Progressive::run_step_return ret = run_step(_step_per_iteration);
    if (ret.first == Progressive::state_terminated) {
        _progressiveStateSingleton->getState()->_start_progressive = false;
    }

    if (ret.second >= maxStep()) {
        _progressiveStateSingleton->getState()->_abort_calculation = true;
    }

    _components = _eigenbasis(cv::Rect(0,0,2,_eigenbasis.rows));
}