#include "incrementalPCA.h"
/* Inspired and ported to OpenCV from
References
----------
T. Fujiwara, J.-K. Chou, Shilpika, P. Xu, L. Ren, K.-L. Ma. An Incremental
Dimensionality Reduction Method for Visualizing Streaming Multidimensional
Data
Code at: https://github.com/takanori-fujiwara/streaming-vis-pca
*/

IncrementalPCA::run_step_return
IncrementalPCA::run_step(steps_t steps)
{
    if(_index >= _data_t.rows) _state = state_terminated;
    if (_state == state_terminated) return run_step_return(_state, _state1.second); // already done

    ///cout << "Batch size : " << _batch_size << " - Index : " << _index  << " ==== _data_t : " << _data_t.size << endl;
    auto end = std::min(_index + _batch_size, _data.cols);

    inc->partialFit(_data_t(cv::Rect(0, _index, _data_t.cols, end - _index)));
    _index = end;
    _nb_points_using = _index;

    _state1.second++;
    return run_step_return(_state, _state1.second);
}


void IncrementalPCA::_init(){
    _result = Matrix::zeros(2,_data.cols,CV_64F) + 1e4;
    //_batch_size = min(max(_data.rows,_data.cols/20),_data.rows);
    inc = new IncPCA(2,1.0);
    // their implementation: each row = one data; our system: each col = one data
    _data_t = _data.t();
    _first_iter = true;
}

IncrementalPCA::~IncrementalPCA(){
    _data.release();
    _data_meanSub.release();
    _data_covar.release();
    delete inc;
}

void IncrementalPCA::_partial_fit(){
    Progressive::run_step_return ret = run_step(-9999);
    if(ret.first == Progressive::state_terminated){
        _progressiveStateSingleton->getState()->_start_progressive = false;
    }

    if(ret.second >= maxStep()){
        _progressiveStateSingleton->getState()->_abort_calculation = true;
    }

    _eigenvalues[0] = inc->eigenvalues().at<double>(0,0);
    _eigenvalues[1] = inc->eigenvalues().at<double>(1,0);

}

Matrix IncrementalPCA::transform(){
    if (_state == state_terminated) {
        _components = inc->eigenvectors().t();
        _eigenvectors1 = _components.col(0); // (cv::Rect(0,0,1,_components.rows));
        _eigenvectors2 = _components.col(1); //(cv::Rect(1,0,1,_components.rows));
        res = inc->transform(_data_t); //(cv::Rect(0,0,_data_t.cols,_index)));
        return res.t();
    }
    if(!_first_iter){
        res_prev = res.clone();
    }

    res = inc->transform(_data_t(cv::Rect(0,0,_data_t.cols,_index)));

///    if(!_first_iter)
///        res = inc->geomTrans(res_prev, res);

    _result(cv::Rect(0,0,_index,2)) = res.t();

    _first_iter = false;
    return _result(cv::Rect(0,0,_index,2));
}

void IncrementalPCA::adjust_step(double exec_time){
    //_batch_size = 2;
    double m = exec_time == 0 ? 100 : _time_constant_second / exec_time;
    if(exec_time < _time_constant_second)
        _batch_size = _batch_size * sqrt(m);
    else if (exec_time > _time_constant_second)
        _batch_size = _batch_size * pow(m,2);
    ///cout << _batch_size << endl;

    if(_batch_size < 2) _batch_size = 2;
    if(_batch_size > 10000) _batch_size = 10000;

}

Matrix IncrementalPCA::inv_project(Matrix __result){
    return inc->inverse_transform(_result.t());
}

double IncrementalPCA::reconstruction_error(Matrix result){
    ///cout << _data.size << " >>>> " << inv_project(result).size << endl;
    cv::Scalar neam=cv::mean(cv::abs(_data - inv_project(result).t()));
    return neam.val[0];
}


IncPCA::IncPCA(int const nComponents, double const forgettingFactor)
        : nComponents(nComponents), forgettingFactor(forgettingFactor) {
    initialize();
}

void IncPCA::initialize() {
    components_.resize(0, 0);
    nSamplesSeen_ = 0;
    mean_ = 0.0;
    var_ = 0.0;
    singularValues_.resize(0, 0);
    explainedVariance_.resize(0, 0);
    explainedVarianceRatio_.resize(0, 0);
}

//FIXME: calculate variance numerical error

void IncPCA::partialFit(Matrix X) {
    auto nSamples = X.rows;
    auto nFeatures = X.cols;

    // sanity check
    if (nComponents < 1) {
        nComponents_ = components_.rows < 1 ? std::min(nSamples, nFeatures)
                                            : components_.rows;
    } else if (nComponents > nFeatures) {
        std::cerr << "nComponents must be smaller than or equal to nFeatures"
                  << std::endl;
        return;
    } else if (nComponents > nSamples) {
        std::cerr << "nComponents must be smaller than or equal to nSamples"
                  << std::endl;
    } else {
        nComponents_ = nComponents;
    }

    if (components_.rows >= 1 && components_.rows != nComponents_) {
        std::cerr << "size error";
    }

    // Initialize mean_ and var_
    if (nSamplesSeen_ == 0) {
        mean_ = cv::Mat::zeros(1,nFeatures,CV_64F);
        var_ = cv::Mat::zeros(1,nFeatures,CV_64F);
    }

    // Update stats
    auto colMean_colVar_nTotalSamples =
            incrementalMeanAndVar(X, forgettingFactor, mean_, var_, nSamplesSeen_);
    Vector colMean = std::get<0>(colMean_colVar_nTotalSamples);
    Vector colVar = std::get<1>(colMean_colVar_nTotalSamples);
    auto nTotalSamples = std::get<2>(colMean_colVar_nTotalSamples);
    // Whitening
    if (nSamplesSeen_ == 0) {
        // If it is the first step, simply whiten X
        for(int i=0;i<X.rows;i++){
            for(int j=0;j<X.cols;j++){
                X.at<double>(i,j) -= colMean.at<double>(0,j);
            }
        }
        ///X = X.rowwise() - colMean;
    } else {
        Vector colBatchMean;
        cv::reduce(X, colBatchMean, 0, 1, CV_64F);
        // If X has one row, we should not subtract mean (everything col becomes 0)
        if (X.rows > 1) {
            ///X = X.rowwise() - colBatchMean;
            for(int i=0;i<X.rows;i++){
                for(int j=0;j<X.cols;j++){
                    X.at<double>(i,j) -= colBatchMean.at<double>(0,j);
                }
            }
        }
        Vector castedMean_ = mean_;

        // Build matrix of combined previous basis and new data
        Vector meanCorrection =
                // avoid int overflow
                std::sqrt((nSamplesSeen_ * (long)nSamples) / (double)nTotalSamples) *
                (castedMean_ - colBatchMean);
        singularValues_ = singularValues_ * forgettingFactor;
        Vector rvecSingularValues_ = singularValues_.t();
        Matrix sc = Matrix::zeros(components_.rows,components_.cols,components_.type());
        for(int x=0;x<components_.rows;x++)
            for(int y=0;y<components_.cols;y++)
                sc.at<double>(x,y) = components_.at<double>(x,y) * rvecSingularValues_.at<double>(0,x);

        // vertical stack of result
        Matrix tmpX = cv::Mat::zeros(sc.rows + X.rows + meanCorrection.rows,sc.cols,CV_64F);

        for(int i=0;i<sc.rows;i++)
            for(int j=0;j<sc.cols;j++)
                tmpX.at<double>(i,j) = sc.at<double>(i,j);

        for(int i=0;i<X.rows;i++)
            for(int j=0;j<sc.cols;j++)
                tmpX.at<double>(i+sc.rows,j) = X.at<double>(i,j);

        for(int i=0;i<meanCorrection.rows;i++)
            for(int j=0;j<sc.cols;j++)
                tmpX.at<double>(i+sc.rows+X.rows,j) = meanCorrection.at<double>(i,j);

        X = tmpX;
        //tmpX.release();
    }

    Matrix S,U,V,S2;
    cv::SVD::compute(X,S,U,V,0);
    svdFlip(U, V);

    cv::pow(S,2,S2);
    Vector explainedVariance = S2 / double(nTotalSamples - 1);
    Vector explainedVarianceRatio = S2 / cv::sum( colVar * double(nTotalSamples) )[0];
    nSamplesSeen_ = forgettingFactor * nSamplesSeen_ + (nTotalSamples - nSamplesSeen_);

    auto nRowsToTake = std::min(nComponents_, V.rows);
    components_ = V(cv::Rect(0, 0, V.cols,nRowsToTake));
    singularValues_ = S(cv::Rect(0, 0, S.cols,nRowsToTake));

    mean_ = colMean;
    var_ = colVar;
    explainedVariance_ =
            explainedVariance(cv::Rect(0, 0, explainedVariance.cols,nRowsToTake));
    explainedVarianceRatio_ = explainedVarianceRatio(cv::Rect(
            0, 0, explainedVarianceRatio.cols,nRowsToTake));

}

// 0: colwise   1:rowwise
// CV_REDUCE_SUM = 0 CV_REDUCE_AVG = 1 CV_REDUCE_MAX = 2 CV_REDUCE_MIN = 3
std::tuple<Vector, Vector, int>
IncPCA::incrementalMeanAndVar(Matrix const &X, double forgettingFactor,
                              Vector const &lastMean,
                              Vector const &lastVariance,
                              int const lastSampleCount) {
    /* References
    ----------
    T. Chan, G. Golub, R. LeVeque. Algorithms for computing the sample
        variance: recommendations, The American Statistician, Vol. 37, No. 3,
        pp. 242-247
    */

    Matrix lastSum =
            forgettingFactor * lastMean * double(lastSampleCount);
    Matrix newSum;
    cv::reduce(X, newSum, 0, 0, CV_64F);
    auto newSampleCount = X.rows;
    auto updatedSampleCount =
            forgettingFactor * double(lastSampleCount) + double(newSampleCount);
    Vector updatedMean = (newSum + lastSum) / double(updatedSampleCount);

    Matrix updatedVariance;
    Matrix updatedUnnormalizedVariance;
    if (lastVariance.rows != 0) {
        // colwise variace * newSampleCount
        Matrix mean1,mean2,pow,pow2;
        cv::pow(X,2,pow);
        cv::reduce(pow, mean1, 0, 1, CV_64F);

        cv::reduce(X, mean2, 0, 1, CV_64F);
        cv::pow(mean2,2,pow2);

        Matrix newUnnormalizedVariance = mean1 - pow2 * double(newSampleCount);

        if (lastSampleCount == 0) { // Avoid division by 0
            updatedUnnormalizedVariance = newUnnormalizedVariance;
        } else {
            auto lastOverNewCount = double(lastSampleCount) / double(newSampleCount);
            Matrix lastUnnormalizedVariance =
                    lastVariance * double(lastSampleCount);

            Matrix pow3;
            cv::pow((lastSum / lastOverNewCount - newSum),2,pow3);

            updatedUnnormalizedVariance =
                    lastUnnormalizedVariance + newUnnormalizedVariance +
                    lastOverNewCount / double(updatedSampleCount) * pow3;

            pow3.release();
        }
        updatedVariance = updatedUnnormalizedVariance / double(updatedSampleCount);

        newUnnormalizedVariance.release();
        mean1.release();mean2.release();pow.release();pow2.release();
    }

    return std::make_tuple(updatedMean, updatedVariance,
                           lastSampleCount + newSampleCount);
}

void IncPCA::svdFlip(Matrix &U, Matrix &V) {
    auto n = U.cols;
    for (int i = 0, m = V.rows; i < n; ++i) {
        // Eigen maxCoeff doesn't work well for single cell matrix
        int maxAbsPos = 0;
        if( m != 1){
            double minVal,maxVal;
            cv::Point minLoc,maxLoc;
            cv::minMaxLoc(abs(V.row(i)),  &minVal, &maxVal, &minLoc, &maxLoc);
            maxAbsPos = maxLoc.x;
        }


        auto value = V.at<double>(i, maxAbsPos);
        auto sign = (value > 0.0) - (value < 0.0);
        if (n > i)
            U.col(i) *= sign;
        V.row(i) *= sign;
    }
}

Matrix IncPCA::transform(Matrix const &X) {
    // if (components_.rows == 0 || X.cols != components_.cols) {
    //     std::cerr << "Matrix cols and components cols have different sizes."
    //               << std::endl;
    //     return result;
    // }
    //Matrix result = cv::Mat::zeros(X.rows,X.cols,CV_64F);
    // for(int i=0;i<X.rows;i++) {
    //     for(int j=0;j<X.cols;j++){
    //         result.at<double>(i,j) = X.at<double>(i,j) - mean_.at<double>(0,j);
    //     }
    // }
    Matrix result = X.clone();
    for (int r = 0; r < X.rows; r++)
        result.row(r) = result.row(r) - mean_;

    result *= components_.t();

    return result;
    ///return X * components_.t();
}

Matrix IncPCA::geomTrans(Matrix const &pointsFrom,
                         Matrix const &pointsTo) {
    auto n = std::min(pointsFrom.rows, pointsTo.rows);
    auto m = pointsTo.rows - pointsFrom.rows;

    if (m < 0) {
        std::cerr << "geomTrans: does not support the case that rows of pointsTo "
                     "is smaller than rows of pointsFrom"
                  << std::endl;
    }

    Matrix processedPointsFrom = pointsFrom(cv::Rect(0,0,pointsFrom.cols,n));
    Matrix processedPointsTo = pointsTo(cv::Rect(0,0,pointsTo.cols,n));

    // translation
    Matrix meanPointsFrom, meanPointsTo;
    cv::reduce(processedPointsFrom, meanPointsFrom, 0, 1, CV_64F);
    cv::reduce(processedPointsTo, meanPointsTo, 0, 1, CV_64F);

    ///processedPointsFrom = processedPointsFrom.rowwise() - meanPointsFrom;
    ///processedPointsTo = processedPointsTo.rowwise() - meanPointsTo;
    for(int i=0;i<processedPointsFrom.rows;i++){
        for(int j=0;j<processedPointsFrom.cols;j++){
            processedPointsFrom.at<double>(i,j) -= meanPointsFrom.at<double>(0,j);
        }
    }

    for(int i=0;i<processedPointsTo.rows;i++){
        for(int j=0;j<processedPointsTo.cols;j++){
            processedPointsTo.at<double>(i,j) -= meanPointsTo.at<double>(0,j);
        }
    }

    // uniform scaling
    double scalePointsFrom=0,scalePointsTo=0;
    for(int i=0;i<processedPointsFrom.cols;i++){
        scalePointsFrom += cv::norm(processedPointsFrom(cv::Rect(i,0,1,processedPointsFrom.rows)));
    }
    for(int i=0;i<processedPointsTo.cols;i++){
        scalePointsTo += cv::norm(processedPointsTo(cv::Rect(i,0,1,processedPointsTo.rows)));
    }

    scalePointsFrom /= double(n);
    scalePointsTo /= double(n);
    scalePointsFrom = std::sqrt(scalePointsFrom);
    scalePointsTo = std::sqrt(scalePointsTo);
    processedPointsFrom /= scalePointsFrom;
    processedPointsTo /= scalePointsTo;

    // rotation
    Matrix S,U,V,R;
    cv::SVD::compute(processedPointsFrom.t() * processedPointsTo,S,U,V,0);
    V = V.t();
    R = V*U.t();

    // apply tranformation
    Matrix transformedPointsTo = cv::Mat::zeros(pointsTo.rows,2,CV_64F);
    transformedPointsTo(cv::Rect(0,0,transformedPointsTo.cols,n)) = scalePointsFrom * processedPointsTo * R;

    // for new points
    Matrix processedNewPointsTo = pointsTo(cv::Rect(0,pointsTo.rows-m,pointsTo.cols,m));
    ///processedNewPointsTo = processedNewPointsTo.rowwise() - meanPointsTo;
    for(int i=0;i<processedNewPointsTo.rows;i++){
        for(int j=0;j<processedNewPointsTo.cols;j++){
            processedNewPointsTo.at<double>(i,j) -= meanPointsTo.at<double>(0,j);
        }
    }
    processedNewPointsTo /= scalePointsTo;
    processedNewPointsTo = scalePointsFrom * processedNewPointsTo * R;

    // aggregate result and move center
    // don't remove + 0
    transformedPointsTo(cv::Rect(0,transformedPointsTo.rows-m,transformedPointsTo.cols,m)) = processedNewPointsTo + 0;

    ///transformedPointsTo = transformedPointsTo.rowwise() + meanPointsFrom;
    for(int i=0;i<transformedPointsTo.rows;i++){
        for(int j=0;j<transformedPointsTo.cols;j++){
            transformedPointsTo.at<double>(i,j) += meanPointsFrom.at<double>(0,j);
        }
    }

    S.release();U.release();V.release();R.release();processedNewPointsTo.release();processedPointsFrom.release();
    //cout << processedNewPointsTo.size << endl;
    //cout << transformedPointsTo.size << endl;
    //printMat(processedNewPointsTo);
    return transformedPointsTo;
}

Matrix IncPCA::inverse_transform(Matrix const &X){
    return X * components_ ;
}

Matrix IncPCA::eigenvalues() {
    Matrix _eigenvalues = Matrix::zeros(singularValues_.rows,1,singularValues_.type());
    for(int i=0;i<singularValues_.rows;i++)
        _eigenvalues.at<double>(i,0) = pow(singularValues_.at<double>(i,0),2) / nSamplesSeen_;
    return _eigenvalues;
}

Matrix IncPCA::eigenvectors() {
    return components_;
}
