#ifndef PPCA_INCREMENTALPCA_H
#define PPCA_INCREMENTALPCA_H

#include "progressive.hpp"

class IncPCA {
public:
    IncPCA(int const nComponents = 2,
           double const forgettingFactor = 1.0);

    void initialize();
    void partialFit(Matrix X);
    Matrix transform(Matrix const &X);
    Matrix inverse_transform(Matrix const &X);
    Matrix static geomTrans(Matrix const &pointsFrom,
                            Matrix const &pointsTo);
    Matrix eigenvectors();
    Matrix eigenvalues();

private:
    int nComponents;
    double forgettingFactor;

    Matrix components_;
    int nComponents_;
    int nSamplesSeen_;
    Vector mean_;
    Vector var_;
    Matrix singularValues_;
    Matrix explainedVariance_;
    Matrix explainedVarianceRatio_;

    std::tuple<Vector, Vector,int> static incrementalMeanAndVar(Matrix const &X,
                                                                double forgettingFactor,
                                                                Vector const &lastMean = {},
                                                                Vector const &lastVariance = {},
                                                                int const lastSampleCount = 0);
    void static svdFlip(Matrix &U, Matrix &V);
};

class IncrementalPCA : public Progressive {
public:
    IncrementalPCA(const std::string& name, const Matrix data, int maxIter, int batch_size):
            Progressive(name,data,maxIter), _batch_size(batch_size>0 ? batch_size:1){_init();};
    ~IncrementalPCA() override;

    virtual run_step_return run_step(steps_t steps) override;
    double reconstruction_error(Matrix result) override;
    void adjust_step(double exec_time) override;
    int current_index(){return _index;}
    Matrix inv_project(Matrix __result) override;
    Matrix transform() override;

protected:
    void _partial_fit() override;

private:
    int _index = 0;
    int _batch_size;
    Matrix _result;
    void _init();

    Matrix _data_t;
    Matrix res_prev;
    Matrix res;
    bool _first_iter;
    IncPCA *inc;
};

#endif //PPCA_INCREMENTALPCA_H

