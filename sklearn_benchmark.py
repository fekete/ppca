import sys
from time import perf_counter
import csv

import numpy as np
from sklearn.decomposition import IncrementalPCA, PCA
from sklearn.utils import gen_batches


def batch_pca(data, baseline, out):
    algo = "ScikitLearnPCA"

    print("Computing ", algo, "...", end="")
    n_components = 2
    start = perf_counter()
    pca = PCA(copy=False,
              svd_solver='full')
    # pca = PCA(n_components=n_components,
    #           copy=False,
    #           svd_solver='auto')
    init_time = perf_counter() - start
    out.writerow([algo, 0, 0, init_time, None, None])

    starti = perf_counter()
    pca.fit(data)
    fit_time = perf_counter() - starti
    x_pca = pca.transform(data)
    error = np.abs(np.abs(baseline) - np.abs(x_pca[:,:2])).mean()
    out.writerow([algo, -1, 0, fit_time,
                  pca.explained_variance_[0],
                  pca.explained_variance_[1],
                  error])
    print("done: ", init_time+fit_time, "s", sep="")


def incremental_pca(data, baseline, out):
    algo = "ScikitLearnIncrementalPCA"

    print("Computing ", algo, "...", end="")
    n_components = 2
    n_samples, n_features = data.shape
    batch_size = 5*n_features
    start = perf_counter()
    ipca = IncrementalPCA(n_components=n_components,
                          copy=False,
                          batch_size=batch_size)
    total_time = perf_counter() - start
    out.writerow([algo, 0, batch_size, total_time, None, None])
    i = 1
    for batch in gen_batches(n_samples, ipca.batch_size,
                             min_batch_size=n_components):
        starti = perf_counter()
        x_batch = data[batch]
        ipca.partial_fit(x_batch, check_input=False)
        duration = perf_counter() - starti
        x_ipca = ipca.transform(data[0:batch.stop])
        error = np.abs(np.abs(baseline[0:batch.stop]) - np.abs(x_ipca)).mean()
        out.writerow([algo, i, batch_size, duration,
                      ipca.explained_variance_[0],
                      ipca.explained_variance_[1],
                      error])
        total_time += duration
        i += 1
    print("done:  ", total_time, "s", sep="")

if __name__ == "__main__":
    data = np.loadtxt(sys.argv[1], delimiter=',', dtype=np.float64)
    baseline = np.loadtxt(sys.argv[2], delimiter=',', dtype=np.float64)
    with open('out.csv', 'w') as csvfile:
        out = csv.writer(csvfile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
        out.writerow(["Algorithm", "Stage", "BatchSize", "Duration", "eigenvalue1", "eigenvalue2", "error"])

        incremental_pca(data, baseline, out)
        batch_pca(data, baseline, out)
