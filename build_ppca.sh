#!/bin/bash
set -eu -o pipefail
if [ $# -eq 0 ] ; then
    echo "Usage: $0 <build-dir>"
    exit 1
fi


CONDA=$(which conda)||true
if [[ -x "$CONDA" ]] ; then
    echo "conda executable found: $CONDA"
else
    echo "conda executable NOT found"
    echo "- download and install miniconda (see https://docs.conda.io/en/latest/miniconda.html)"
    echo "- do what is needed to include the conda executable in your PATH environment variable"
    echo "(the default path to conda executable is $HOME/miniconda3/bin/conda)"
    echo "Then run ./install_ppca.sh"
    exit 1
fi

CONDA_ROOT=$(dirname $(dirname $CONDA))
build_dir="$1"
mkdir -p "${build_dir}"
cd "${build_dir}" || exit
cmake -DCMAKE_INSTALL_PREFIX=${CONDA_ROOT}  ..
# Use all the CPUs minus one
CPUS=`nproc --all`
make -j`expr $CPUS - 1`
