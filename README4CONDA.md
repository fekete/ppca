* download and install miniconda (see https://docs.conda.io/en/latest/miniconda.html)
* do what is needed to include the conda executable in your **PATH** environment variable (usually, the default path to conda executable is $HOME/miniconda3/bin/conda)
* change directory to ppca
* Execute ``./build_ppca.sh <build-dir>``
* Executables are generate in <build-dir> their names are : ppca_benchmark and ppca_momentum

NB: You dont have to create <build-dir>! Indeed, build_ppca.sh will create it foy you as a subdirectory of the ppca dir.
