#!/bin/bash
set -eu -o pipefail
if [ $# -eq 0 ] ; then
    echo "Usage: $0 <build-dir> [\"blank_conda_required\"]"
    exit 1
fi

if [  $# -eq 2 ] ; then
    if [ $2 == "blank_conda_required" ] ; then
	if [ ! -d "$HOME/blank_miniconda3" ]; then
	    echo "$HOME/blank_miniconda3 dir does not exist, abort!"
	    exit 1
	fi
	rm -rf "$HOME/miniconda3"
	cp -rp  "$HOME/blank_miniconda3"  "$HOME/miniconda3"
    else
	echo "Usage: $0 <build-dir> [\"blank_conda_required\"]"
    fi
fi

CONDA=$(which conda)||true
if [[ -x "$CONDA" ]] ; then
    echo "conda executable found: $CONDA"
else
    echo "conda executable NOT found"
    echo "- download and install miniconda (see https://docs.conda.io/en/latest/miniconda.html)"
    echo "- do what is needed to include the conda executable in your PATH environment variable"
    echo "(the default path to conda executable is $HOME/miniconda3/bin/conda)"
    exit 1
fi
CONDA_ROOT=$(dirname $(dirname $CONDA))
if [ Linux == `uname` ]
then
    conda env create -f ppca-linux.yml
else
    conda env create -f ppca-osx.yml
fi       

if $?; then
    echo "Installation failed, sorry!"
else
    echo "Installation succeeded. Type:"
    echo "conda activate ppca"
    echo "Then, run compile the program by typing:"
    echo "./build_ppca.sh build"
    echo "Then, run the benchmark by typing:"
    echo "./run_benchmark.sh build"
fi
