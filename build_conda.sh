#!/usr/bin/env bash

set -eu -o pipefail

if [ $# -eq 0 ] ; then
    echo "Usage: $0 <build-dir> [\"blank_conda_required\"]"
    exit 1
fi

if [  $# -eq 2 ] ; then
    if [ $2 == "blank_conda_required" ] ; then
	if [ ! -d "$HOME/blank_miniconda3" ]; then
	    echo "$HOME/blank_miniconda3 dir does not exist, abort!"
	    exit 1
	fi
	rm -rf "$HOME/miniconda3"
	cp -rp  "$HOME/blank_miniconda3"  "$HOME/miniconda3"
    else
	echo "Usage: $0 <build-dir> [\"blank_conda_required\"]"
    fi
fi

build_dir="$1"
mkdir -p "${build_dir}"
cd "${build_dir}" || exit

cp -r ../conda-recipe .
cp ../CMakeLists.txt .
cp -r ../src .

CONDA="conda"
if [[ "$OSTYPE" == "msys" ]]; then
    CONDA="conda.exe"
fi
$CONDA build --no-anaconda-upload -c conda-forge conda-recipe
$CONDA install --no-update-deps --use-local --yes -c conda-forge conda-ppca 
$CONDA clean --all --yes
exit $?
