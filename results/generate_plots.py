#!/bin/env python3

import sys
import shutil

import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc
#from matplotlib.lines import Line2D

LABELS = [
    "PIM",
    "RPCA",
    "IPCA sk",
    "IPCA 2*d",
    "IPCA 5*d",
    "IPCA 1k"
]

def hi_plot(ax, x, y, label, **style):
    p = ax.plot(x, y, label=label, **style)
    # ax.plot(x.iloc[-1],
    #         y.iloc[-1],
    #         label=label,
    #         marker='|',
    #         markeredgecolor=p[-1].get_color())
    ax.axvline(x.iloc[-1],
               lw=0.5,
               color=p[-1].get_color())

def generate_plot(dataset,only_legend = False):
    style = dict(marker=',',
                 markerfacecolor="b",
                 markevery=10)
    algorithms = []
    batch_sizes = []
    iterations = []
    durations = []
    inits = []
    df = pd.read_csv(dataset+".txt", sep=' ', index_col=False)
    first = df.iloc[0]
    title = (first['Dataset'] +
             " (" + str(first['Rows']) + "x" + str(first['Cols']) + ")")
    baseline = df[(df.Algorithm=='Baseline') & (df.Stage==-1)]
    baseline_ev1 = baseline.eigenvalue1.iloc[0]
    baseline_ev2 = baseline.eigenvalue2.iloc[0]
    algorithms.append("Baseline")
    batch_sizes.append(0)
    iterations.append(0)
    durations.append(baseline.Duration.iloc[0])
    inits.append(baseline.Duration.iloc[0])
    opencv = df[(df.Algorithm=='OpenCVAll') & (df.Stage!=0)]
    algorithms.append("OpenCV")
    batch_sizes.append(0)
    iterations.append(0)
    durations.append(opencv.Duration.iloc[0])
    inits.append(opencv.Duration.iloc[0])

    momentum = df[(df.Algorithm=='ProgressiveBlockPowerMethodWithMomentum')]
    randomized = df[(df.Algorithm=='ProgressiveRandomizedSVD')]
    incrementalP = df[(df.Algorithm=='ScikitLearnPCA')]
    incremental = df[(df.Algorithm=='ProgressiveIncrementalPCA')]
    # incremental has multiple runs with different increments
    pca_sizes = incremental.BatchSize.unique()
    incrementals = [incremental[incremental.BatchSize == pca_size]
                    for pca_size in pca_sizes]

    plt.yscale("log")
    fig, axes = plt.subplots(ncols=1,
                             nrows=3,
                             figsize=(5, 3), # tuned for LaTeX VIS style
                             sharex=True)
    #fig, axes = plt.subplots(ncols=2, nrows=2)
    #[[ax1, ax3], [ax2, ax4]] = axes
    ax1, ax3, ax2 = axes

    batch_sizes.append(1)
    label = LABELS[0]
    algorithms.append(label)
    momentum_time = momentum.Duration.cumsum().iloc[1:]
    iterations.append(momentum.Duration.iloc[1:].median())
    durations.append(momentum.Duration.sum())
    inits.append(momentum.Duration.iloc[0])
    # ax1.plot(
    hi_plot(
        ax1, 
        momentum_time,
        abs((momentum.eigenvalue1.iloc[1:]-baseline_ev1)
            /baseline_ev1),
        label=label,
        **style)

    # ax3.plot(
    hi_plot(
        ax3,
        momentum_time,
        abs((momentum.eigenvalue2.iloc[1:]-baseline_ev2)
            /baseline_ev2),
        label=label,
        **style)

    label = LABELS[1]
    algorithms.append(label)
    batch_sizes.append(1)
    randomized_time = randomized.Duration.cumsum().iloc[1:]
    iterations.append(randomized.Duration.iloc[1:].median())
    durations.append(randomized.Duration.sum())
    inits.append(randomized.Duration.iloc[0])
    # ax1.plot(
    hi_plot(
        ax1,
        randomized_time,
        abs((randomized.eigenvalue1.iloc[1:]-baseline_ev1)
            /baseline_ev1),
        label=label,
        **style)
    # ax3.plot(
    hi_plot(
        ax3,
        randomized_time,
        abs((randomized.eigenvalue2.iloc[1:]-baseline_ev2)
            /baseline_ev2),
        label=label,
        **style)

    incrementalP_batch_size = incrementalP.BatchSize.iloc[0]
    label = LABELS[2]
    algorithms.append(label)
    batch_sizes.append(incrementalP_batch_size)
    incrementalP_time = incrementalP.Duration.cumsum().iloc[1:]
    iterations.append(incrementalP.Duration.iloc[1:].median())
    durations.append(incrementalP.Duration.sum())
    inits.append(incrementalP.Duration.iloc[0])
    # ax1.plot(
    hi_plot(
        ax1,
        incrementalP_time,
        abs((incrementalP.eigenvalue1.iloc[1:]-baseline_ev1)
            /baseline_ev1),
        label=label,
        **style)
    # ax3.plot(
    hi_plot(
        ax3,
        incrementalP_time,
        abs((incrementalP.eigenvalue2.iloc[1:]-baseline_ev2)
            /baseline_ev2),
        label=label,
        **style)

    incremental_times = []
    for i, inc in enumerate(incrementals):
        batch_size = inc.BatchSize.iloc[0]
        label = LABELS[3+i]
        algorithms.append(label)
        batch_sizes.append(batch_size)
        incremental_time = inc.Duration.cumsum().iloc[1:]
        iterations.append(inc.Duration.iloc[1:].median())
        durations.append(inc.Duration.sum())
        inits.append(inc.Duration.iloc[0])
        incremental_times.append(incremental_time)
        # ax1.plot(
        hi_plot(
            ax1,
            incremental_time,
            abs((inc.eigenvalue1.iloc[1:]-baseline_ev1)
                /baseline_ev1),
            label=label,
            **style)
        # style2['color'] = p[-1].get_color()
        # ax3.plot(
        hi_plot(
            ax3,
            incremental_time,
            abs((inc.eigenvalue2.iloc[1:]-baseline_ev2)
                /baseline_ev2),
            label=label,
            **style)

    ax1.set_xlim(left=0)
    ax1.set_yscale("log")
    ax1.set_ylabel('$\\log |\\frac{{\\mu_1}-\\lambda_1}{\\lambda_1}|$',
                   labelpad=0)
    ax1.grid(True)
    ax3.set_xlim(left=0)
    ax3.set_yscale("log")
    ax3.set_ylabel('$\\log |\\frac{{\\mu_2}-\\lambda_2}{\\lambda_2}|$',
                   labelpad=0)
    ax3.grid(True)
    #ax1.set_title("Time of progressive algorithms")  # Add a title to the axes.
    # ax1.legend()  # Add a legend.
    #ax1.add_artist(ax1.legend(
    #bbox_to_anchor=(1.04, 0.5),
    #loc='lower right',
    #ncol=2))

    # legend_elements = \
    #     [Line2D([0], [0], color='black', lw=1, linestyle='-', label='First Eigenvalue'),
    #     Line2D([0], [0], color='black', lw=1, linestyle='--',label='Second Eigenvalue')]
    # legend1 = ax1.legend(handles=legend_elements)
    # ax1.legend(handles=legend_elements,
    #            bbox_to_anchor=(0.5, -0.1),
    #            loc='center',
    #            ncol=2)

    hi_plot(
        ax2,
        momentum_time,
        momentum.error.iloc[1:],
        label=algorithms[0],
        **style)
    hi_plot(
        ax2,
        randomized_time,
        randomized.error.iloc[1:],
        label=algorithms[1],
        **style)
    hi_plot(
        ax2,
        incrementalP_time,
        incrementalP.error.iloc[1:],
        label=algorithms[2],
        **style)

    incremental_times.reverse()
    for i, incremental in enumerate(incrementals):
        incremental_time = incremental_times.pop()
        hi_plot(
            ax2,
            incremental_time,
            incremental.error.iloc[1:],
            label=algorithms[3+i],
            **style)
    # ax2.set_xlabel('Time (s)')  # Add an x-label to the axes.
    ax2.set_ylabel('Error (log)')  # Add a y-label to the axes.
    ax2.set_yscale("log")
    ax2.grid(True)
    #ax2.legend(loc='lower right')

    # output_boxplots(ax3, momentum, randomized, incrementals, incrementalP)
    fig.suptitle("Time (s) and Error for Dataset "+title, y=0.93)
    plt.subplots_adjust(hspace=0.05)
    count = len(algorithms)
    data = {'Dataset':  [dataset]*count,
            'Algorithm': algorithms,
            'Dims': [str(df.Cols.iloc[0])]+([""]*(count-1)),
            'BatchSize': batch_sizes,
            'Init': inits,
            'Iteration': iterations,
            'Duration': durations}
            
    if only_legend:
        handles,labels = ax2.get_legend_handles_labels()
        fig, axe = plt.subplots(ncols=1,
                    nrows=1,
                    figsize=(5/2.0, 3/2.0), # tuned for LaTeX VIS style
                    sharex=True)
        axe.legend(handles, labels,loc = "center")
        axe.xaxis.set_visible(False)
        axe.yaxis.set_visible(False)
        for v in axe.spines.values():
            v.set_visible(False)
        
    #import pdb; pdb.set_trace()
    return pd.DataFrame(data=data, index=range(len(algorithms))), df

def generate_boxplots(ax3, momentum, randomized, incrementals, incrementalP):
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    ax3.set_title('Durations')
    data = [momentum.Duration.iloc[1:].values,
            randomized.Duration.iloc[1:].values,
            incrementals[0].Duration.iloc[1:].values,
            incrementals[1].Duration.iloc[1:].values,
            incrementals[2].Duration.iloc[1:].values,
            incrementalP.Duration.iloc[1:].values]
    bplot = ax3.boxplot(data, vert=False,
                        patch_artist=True,
                        showfliers=False,
                        manage_ticks=True,
                        labels=LABELS)
    print(bplot.keys())
    for patch, color in zip(bplot['boxes'], colors):
        patch.set_facecolor(color)
    for patch, color in zip(bplot['fliers'], colors):
        patch.set_markeredgecolor(color)
    for patch, color in zip(bplot['caps'], colors):
        patch.set_color(color)
    for patch, color in zip(bplot['whiskers'], colors):
        patch.set_color(color)
    for patch, color in zip(bplot['medians'], colors):
        patch.set_color(color)

    for i, text in enumerate(ax3.get_yticklabels()):
        text.set_color(colors[i])
        
    # ax3.get_xaxis().tick_bottom()
    ax3.set_xscale("log")
    ax3.tick_params(axis='y',
                    which='both',
                    top=False,
                    bottom=True,
                    labelbottom=False)
    ax3.set_xlabel('duration (s)')
    ax3.grid(True)

if __name__ == "__main__":
    matplotlib.use("pdf")
    # In case latex or gs are not installed
    if shutil.which("latex") and shutil.which("gs"):
        rc('text', usetex=True)
        # rc('font', **{'family':'sans-serif','sans-serif':['Helvetica']})
    DATASETS = ["MNIST", "SINEWAVE", "NORMAL", "LFW", "CIFAR10", "NORB", "IRIS"]
    if len(sys.argv) > 1:
        DATASETS = sys.argv[1:]

    STATS = None
    ALL = None
    for ds in DATASETS:
        plt.figure(frameon=False,
                   clear=True)
        stat, df = generate_plot(ds,False)
        if STATS is None:
            STATS = stat
            ALL = df
        else:
            STATS = STATS.append(stat, ignore_index=True)
            ALL = ALL.append(df, ignore_index=True)
        plt.savefig(ds)
        plt.clf()
    
    # Draw legend in a separate file
    plt.figure(frameon=False,clear=True)
    generate_plot("IRIS",True)
    plt.savefig("LEGEND")
    plt.clf()
    
    GROUPED = STATS.groupby(['Dataset'])
    STATS['sum'] = GROUPED[['Duration']].transform(sum)
    STATS.sort_values(by='sum', kind='mergesort', inplace=True)
    STATS.drop('sum', axis=1, inplace=True)
    STATS.set_index(['Dataset', 'Algorithm'], inplace=True)
    with open('stats.tex', 'w') as texfile:
        STATS.to_latex(texfile, sparsify=True, multirow=True)
    ALL.to_csv('ALL.txt', index=False, sep=' ')
