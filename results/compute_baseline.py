import pandas as pd

DF = pd.read_csv('ALL.txt', sep=' ', index_col=False)

DATASETS = ["MNIST", "SINEWAVE", "NORMAL", "LFW", "CIFAR10", "NORB", "IRIS"]

for dataset in DATASETS:
    batch = DF[(DF.Stage==-1) & (DF.Dataset==dataset)]
    e1 = batch.eigenvalue1.iloc[0]
    e2 = batch.eigenvalue2.iloc[0]
    for row in batch.itertuples():
        print(dataset, row.Algorithm, "e1", (row.eigenvalue1/e1)-1)
        print(dataset, row.Algorithm, "e2", (row.eigenvalue2/e2)-1)
    
