#!/bin/env python3
import urllib.request
import ssl
import os
import sys
import bisect
from shutil import move

print('Downloading `ppca_input.zip`', file=sys.stderr)


def feedback(cnt, blksize, totalsize):
    if cnt == 0:
        return
    percents = [totalsize*pc/100 for pc in range(5, 100, 5)]
    prev = (cnt-1)*blksize
    cur = cnt*blksize
    if (cnt % 128) == 0:
        print('.', file=sys.stderr, end='')
    if bisect.bisect(percents, prev) != bisect.bisect(percents, cur):
        print(f"{cur:,}/{totalsize:,}", file=sys.stderr)


if os.path.isfile('ppca_input.zip'):
    print('`ppca_input.zip` is already downloaded', file=sys.stderr)
    sys.exit(0)

ssl._create_default_https_context = ssl._create_unverified_context
local_filename, headers = urllib.request.urlretrieve(
    'https://aviz.fr/data/ppca_input.zip',
    None,
    reporthook=feedback
)
print('done.', file=sys.stderr)
move(local_filename, 'ppca_input.zip')
